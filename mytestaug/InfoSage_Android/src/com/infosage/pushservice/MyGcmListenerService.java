/**
 * Copyright 2015 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infosage.pushservice;

import java.util.Date;

import org.infosagehealth.app.AppController;
import org.infosagehealth.app.MainActivity;
import org.infosagehealth.app.R;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * 
 * @author Govinda P.
 *
 */
public class MyGcmListenerService extends GcmListenerService {
	 int count1=0,count2=0;
    private static final String TAG = "MyGcmListenerService";
    String msg="";
    
    
    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) 
    {
    	   Log.e(TAG, "From: " + from + data.toString());
      
        try {
        	  String message = data.getString("body");
              
              Log.e(TAG, "From: " + "INFOSAGE");
              Log.e(TAG, "Message: " + message);

              /**
               * Json Parsing 
               */
             msg=message;
             
        //JSONObject mainObject = new JSONObject(message);
        
        if (!msg.toString().trim().equals("")) 
        {
        	// New Order = status 1
        	
        	
 			message=msg;
		}
        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */        
       // if(AppController.session.isPushRegister())
      //  {
        	if(AppController.isActivityVisible())
        	{
        		Intent registrationComplete = new Intent(QuickstartPreferences.PUSH_RECIVER);
        		LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
        	}else
        	{
        		sendNotification(msg);
        	}
     	//}
        
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) 
    {
    	/**
    	 * Sent number for notification
    	 */
    	 
    	if( android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.KITKAT )
        {
       	 sendNotificationBelow(message);
        } 
        else {
        	sendNotificationAbove(message);
        } 
    }
    
    public void sendNotificationAbove(String message)
	 {
		 long time = new Date().getTime();
	        String tmpStr = String.valueOf(time);
	        String last4Str = tmpStr.substring(tmpStr.length() -5);
	        int notificationId = Integer.valueOf(last4Str);
	        
	      Intent intent = new Intent(this, MainActivity.class);
	       
	        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent,
	                PendingIntent.FLAG_ONE_SHOT);
	        Bitmap icon = BitmapFactory.decodeResource(getResources(),
	                R.drawable.myicon);
	        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	        Builder notificationBuilder = new NotificationCompat.Builder(this)
	                .setSmallIcon(getNotificationIcon())
	                .setLargeIcon(icon)
	                .setContentTitle("InfoSAGE")
	                .setContentText(message)
	                .setAutoCancel(true)
	                .setSound(defaultSoundUri)
	                .setContentIntent(pendingIntent);
	        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
	        	notificationBuilder.setColor(Color.parseColor("#cbdb2a"));
			}else
			{
	        	notificationBuilder.setColor(Color.parseColor("#cbdb2a"));
			}
	        notificationBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
	        
	        notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
	        NotificationManager notificationManager =
	                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

	        notificationManager.notify(notificationId, notificationBuilder.build());
	 }
	 public void sendNotificationBelow(String message)
	 {
		 long time = new Date().getTime();
	        String tmpStr = String.valueOf(time);
	        String last4Str = tmpStr.substring(tmpStr.length() -5);
	        int notificationId = Integer.valueOf(last4Str);
	        
	      Intent intent = new Intent(this, MainActivity.class);
	       
	        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent,
	                PendingIntent.FLAG_ONE_SHOT);
	        Bitmap icon = BitmapFactory.decodeResource(getResources(),
	                R.drawable.myicon);
	        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	        Builder notificationBuilder = new NotificationCompat.Builder(this)
	                .setSmallIcon(R.drawable.myicon)
	               // .setLargeIcon(icon)
	                .setContentTitle("InfoSAGE")
	                .setContentText(message)
	                .setAutoCancel(true)
	                .setSound(defaultSoundUri)
	                .setContentIntent(pendingIntent);
	       
	        notificationBuilder.setDefaults(NotificationCompat.DEFAULT_VIBRATE);
	        
	        notificationBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
	        NotificationManager notificationManager =
	                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	        notificationManager.notify(notificationId, notificationBuilder.build());
	 }
    
    private int getNotificationIcon() {
    	 boolean whiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
         if( android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP )
         {
        	 return R.drawable.myicon_loli;
         } 
         else {
        	 return R.drawable.myicon;
         }
    }
   
}
