/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.infosage.pushservice;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.infosage.util.Constants;
import com.infosage.util.MyFunction;

import java.io.IOException;
import java.util.Map;

import org.infosagehealth.app.AppController;
import org.json.JSONObject;

/**
 * 
 * @author Govinda P.
 *
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};
    JSONObject jsonBody=new JSONObject();
    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            // In the (unlikely) event that multiple refresh operations occur simultaneously,
            // ensure that they are processed sequentially.
            synchronized (TAG) {
                // [START get_token]
                // Initially this call goes out to the network to retrieve the token, subsequent calls
                // are local.
                InstanceID instanceID = InstanceID.getInstance(this);
                // ginfo app : 939360870074 	
               // 69758586256
                String token = instanceID.getToken("69758586256",
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                Log.e(TAG, "GCM Registration Token: " + token);

                // TODO: Implement this method to send any registration to your app's servers.
               String DEVICE_ID = Secure.getString(getApplicationContext().getContentResolver(),Secure.ANDROID_ID);
            	if (DEVICE_ID.equals("") || DEVICE_ID==null) {
            		DEVICE_ID="null";
				}
            	
         sendRegistrationToServer(token,DEVICE_ID);

                // Subscribe to topic channels
              //  subscribeTopics(token);

                // You should store a boolean that indicates whether the generated token has been
                // sent to your server. If the boolean is false, send the token to your server,
                // otherwise your server should have already received the token.
                sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
                // [END get_token]
            }
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            Toast.makeText(getApplicationContext(), "Push Notification not supported",2000).show();
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
     
    }

    /**
     * Persist registration to third-party servers.
     *
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token,final String DEVICE_ID) 
    {
        jsonBody=MyFunction.getJsonBody(new String[]{"string"},new String[]{ token});
		// Add custom implementation, as needed.
    	StringRequest req = new StringRequest(Method.POST,
    			Constants.getURL_PushRegistration(""+AppController.getMainUser().getId()),
                new Response.Listener<String>() {
 
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG," "+response.toString());
                        Log.e("push reposne","Push complete");
							// startService(new Intent(getApplicationContext(), LogoutService.class));
                        AppController.session.SetPushToken(token);
						AppController.session.SetPushRegister(true);
                    }
                },new Response.ErrorListener()  {

					@Override
					public void onErrorResponse(VolleyError volleyError) {
						// TODO Auto-generated method stub
						Log.e("push Error","Push error");
						MyFunction.CheckError(getApplicationContext(), volleyError);
					//	Toast.makeText(getApplicationContext(),""+VolleyErrorHandler.getMessage(volleyError, getApplicationContext()),2000).show();
						AppController.session.SetPushToken("");
						AppController.session.SetPushRegister(false);
								  
					}
				
				}){
		           @Override
		        protected VolleyError parseNetworkError(
		        		VolleyError volleyError) {
		        	// TODO Auto-generated method stub
		        	 
		        	   return MyFunction.parseNetworkErrorMy(volleyError);
		        	       }
		           @Override
		        	protected Response<String> parseNetworkResponse(
		        			NetworkResponse response) {
		        		// TODO Auto-generated method stub
		        	   //Log.e("headers: "," "+ response.headers.toString());
		        	 /*Map<String, String> headers = response.headers;
		        	   String cookie = headers.get("Set-Cookie");
		        	   Log.e("Set-Cookie: "," "+ cookie);
		        	   AppController.getInstance().saveCookie(cookie);*/
		        		return super.parseNetworkResponse(response);
		        	}
		            /**
		             * Passing some request headers
		             * */
		           @Override
			        public Map<String, String> getHeaders() throws AuthFailureError {
			        	// TODO Auto-generated method stub
		        	   
			        	return AppController.createBasicAuthHeader();
		           }
		           
		           @Override
		           public byte[] getBody() throws AuthFailureError {
		               return jsonBody.toString().getBytes();
		           }

		           @Override
		           public String getBodyContentType() {
		               return "application/json";
		           }
		           
		        };
	
    	
		req.setRetryPolicy(new DefaultRetryPolicy(10000,0,1));
		int socketTimeout = 90000;//30 seconds - change to what you want
		RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		req.setRetryPolicy(policy);
		AppController.getInstance().addToRequestQueue(req);
    	
    	
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(String token) throws IOException {
        for (String topic : TOPICS) {
            GcmPubSub pubSub = GcmPubSub.getInstance(this);
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]

}
