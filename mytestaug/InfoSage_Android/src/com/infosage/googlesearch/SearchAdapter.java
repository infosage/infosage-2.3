/**
*@authour user
*Apr 7, 2016
* @update date Apr 7, 2016
 */
package com.infosage.googlesearch;

import java.util.ArrayList;

import org.infosagehealth.app.R;

import com.infosage.adapter.OnListItemClickListner;
import com.infosage.adapter.AdapterMeditation.ViewHolder;
import com.infosage.item.ItemMeditation;
import com.infosage.util.Constants;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification.Action;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author user
 *
 */
public class SearchAdapter extends BaseAdapter{

	Context context;

	Example list;
	Item item;
	public SearchAdapter(Context context1, Example list)
	{
		context = context1;
		this.list=list;
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.getItems().size();
	}

	@Override
	public Item getItem(int position) {
		// TODO Auto-generated method stub
		return list.getItems().get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder
	{
		
		TextView txttitle,txturl,txtmatter;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewHolder holder;
		item=getItem(position);
		try
		{
			
			if(convertView == null){
				v = inflater.inflate(R.layout.row_search, null);				
				
				holder = new ViewHolder();
				holder.txttitle = (TextView)v.findViewById(R.id.textView1);
				holder.txturl = (TextView)v.findViewById(R.id.textView2);
				holder.txtmatter = (TextView)v.findViewById(R.id.textView3);
				
				v.setTag(holder);
			}
			else
			{
				holder = (ViewHolder)v.getTag();
			}
			
//			holder.txttitle.setText(Html.fromHtml("<a href="+item.getLink()+"'>"+item.getTitle()+"</a>"));
//			holder.txturl.setText(Html.fromHtml("<a href="+item.getLink()+"'>"+item.getDisplayLink()+"</a>"));
			holder.txttitle.setText(item.getTitle());
			holder.txturl.setText(item.getDisplayLink());
			
			Log.e("LINK", "<a href='"+item.getLink()+"'>"+item.getDisplayLink()+"</a>");
			holder.txtmatter.setText(Html.fromHtml(""+item.getHtmlSnippet()));			
			
			holder.txttitle.setOnClickListener(new clicklistener(position));
		}
		catch(Exception e)
		{
		e.printStackTrace();	
		}
		
		return v;
	}

	
	private class clicklistener implements View.OnClickListener
	{

		/* (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		int pos;
		
		private clicklistener(int pos){
			this.pos = pos;
		}
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			item=getItem(pos);
			
			String url = item.getLink();
			
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(url));
			context.startActivity(i);
		}
		
	}
	
	
}