package com.infosage.adapter;

import java.util.List;

import org.infosagehealth.app.MainActivity;
import org.infosagehealth.app.R;

import com.infosage.item.Notification;
import com.infosage.util.Constants;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author Govinda P
 *
 */
public class NotificationAdapter extends BaseAdapter{

	Context context;
	List<Notification> list=null;
	Notification item=null;
	private int lastPosition = -1;
	public NotificationAdapter(Context context1,List<Notification> list2)
	{
		this.list=list2;
		context = context1;
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Notification getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder
	{
		Button btnusr, btncal;
		private ImageView profimg;
		private TextView txttitle;
		private TextView txtdesc;
		private TextView txtdate;		
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewHolder holder;
		item=getItem(position);
		try
		{
			if(convertView == null){
				v = inflater.inflate(R.layout.notifchild_new, null);
				v.setBackgroundDrawable((context.getResources().getDrawable(R.drawable.noti_selector)));
				holder = new ViewHolder();
				holder.profimg = (ImageView)v.findViewById(R.id.profimg);
				holder.txttitle = (TextView)v.findViewById(R.id.txttitle);
				holder.txtdesc = (TextView)v.findViewById(R.id.txtdesc);
				holder.txtdate = (TextView)v.findViewById(R.id.txtdate);
				holder.txtdesc.setEllipsize(TruncateAt.END);
				holder.txtdesc.setMaxLines(2);
				holder.txttitle.setMaxLines(2);
				holder.txttitle.setEllipsize(TruncateAt.END);
				
				v.setTag(holder);
			}
			else
			{
				holder = (ViewHolder)v.getTag();
			}
			
			
			if (item.isResolved())
			{
				v.setBackgroundColor(Color.WHITE);
				v.setBackgroundDrawable((context.getResources().getDrawable(R.drawable.noti_selector)));
			}
			else
			{ 
				v.setBackgroundColor(Color.parseColor("#DFF0EE")); // DFF0EEsite color for unread msg = #D9EDF7
			}
			
			holder.txttitle.setText(""+item.getName());
			holder.txtdate.setText(""+Constants.getDate(item.getDate()));
			holder.txtdesc.setText(""+item.getText());
			try {
				if (item.getImage()!=null) {
					Picasso.with(context).load(MainActivity.imgbaseurl+"/cache/w100xh100-2/images/"+item.getImage().getPath())
					.placeholder(R.drawable.default_profile)
					.into(holder.profimg);
				}else
				{
					holder.profimg.setImageResource(R.drawable.default_profile);
				}
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("Update Adapter", " "+e.toString());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return v;
	}

	
}
