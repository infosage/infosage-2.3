package com.infosage.adapter;

import java.util.ArrayList;

import org.infosagehealth.app.AppController;
import org.infosagehealth.app.MainActivity;
import org.infosagehealth.app.R;
import org.infosagehealth.app.UserPermissionsSetup;
import org.infosagehealth.app.UserTasks;
import org.json.JSONObject;

import com.android.volley.Response.Listener;
import com.infosage.item.ItemKeystone;
import com.infosage.item.ItemPermissions;
import com.infosage.item.User;
import com.infosage.util.SessionManager;
import com.infosage.view.CustomAnimatedProgress;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author Govinda P
 *
 */
public class ImageAdapter extends BaseAdapter {

	private Context mContext;
	private Activity activity;
	
	
	ArrayList<ItemKeystone> userobjlist = new ArrayList<ItemKeystone>();
	
	public ImageAdapter(Context c, Activity act, ArrayList<ItemKeystone>list){
		mContext = c;
		userobjlist = list;
		activity = act;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userobjlist.size();
	}

	@Override
	public ItemKeystone getItem(int arg0) {
		// TODO Auto-generated method stub
		return userobjlist.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		
		
		
		return 0;
	}

	public static class ViewHolder
	{
		TextView tv;
		ImageView img;
	}
	
	
	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		ItemKeystone item = getItem(arg0);
		LayoutInflater inflater = LayoutInflater.from(mContext);
		
		ViewHolder holder=null;

		View childview=arg1;
		if (childview==null) {
			childview = inflater.inflate(R.layout.gridchild, null);
			holder = new ViewHolder();
			 holder.tv = (TextView)childview.findViewById(R.id.txtname);
			 holder.tv.setMaxLines(1);
			 holder.tv.setLines(1);
			 holder.tv.setEllipsize(TruncateAt.END);
			 holder.img = (ImageView)childview.findViewById(R.id.img);
			 
			 childview.setTag(holder);			 
		}
		else
		{
			holder=(ViewHolder)childview.getTag();
		}
		
		
		holder.tv.setText(item.getTarget().getFirstName()+" "+item.getTarget().getLastName());
		
		holder.img= (ImageView)childview.findViewById(R.id.img);
		
		holder.img.setImageResource(R.drawable.default_profile);
		
		if(item.getTarget().getProfileImage() != null){
			
			String path = MainActivity.imgbaseurl+"/cache/w150xh150-2/images/";
			Picasso.with(mContext).load(path+item.getTarget().getProfileImage().getImage().getPath())
			.placeholder(R.drawable.default_profile)
			.into(holder.img);
		}else{
			holder.img.setImageResource(R.drawable.default_profile);
		}
		childview.setOnClickListener(new clicklistner(arg0));
		return childview;
	}
	
	
	private class clicklistner implements View.OnClickListener
	{

		int id;
		
		public clicklistner(int id1)
		{
			id = id1;
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			
			SessionManager sesobj = new SessionManager(mContext);
			sesobj.setMyUserid(""+userobjlist.get(id).getTarget().getId());
			sesobj.setIsProxy(false);
			
			final Intent intent = new Intent(mContext, UserTasks.class);
			final Bundle b = new Bundle();
			b.putString("target",AppController.gson.toJson(userobjlist.get(id).getTarget()));
		
			try {
				
				String user=AppController.gson.toJson(userobjlist.get(id).getTarget());
				final User selectiuser = AppController.gson.fromJson(user,User.class);
				AppController.setViewUser(selectiuser);
				Listener<JSONObject> resp = null;
				final CustomAnimatedProgress progress=new CustomAnimatedProgress(activity);
				UserPermissionsSetup.getPermissionByResponse(progress,""+selectiuser.getId(),""+AppController.getMainUser().getId(), new Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject arg0) {  
						// TODO Auto-generated method stub
						AppController.permissions=AppController.gson.fromJson(arg0.toString(), ItemPermissions.class);
						AppController.permissions.setLoguserid(""+AppController.getMainUser().getId());
						AppController.permissions.setSelectid(""+selectiuser.getId());
						
						Log.e("ImageAdapter","Loguserid: "+AppController.permissions.getLoguserid());
						Log.e("ImageAdapter","Selectid: "+AppController.permissions.getSelectid());
						Log.e("ImageAdapter","MainUserid: "+AppController.getMainUser().getId());
						
						intent.putExtras(b);
						progress.dismiss();
						activity.startActivity(intent);
						activity.overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
						activity.finish();
					}
				});
				
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			
			
		}
		
	}

}
