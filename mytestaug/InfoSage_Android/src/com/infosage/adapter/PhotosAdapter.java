/**
*@authour sanjay
*12-May-2016
* @update date 12-May-2016
 */
package com.infosage.adapter;

import java.util.ArrayList;

import org.infosagehealth.app.MainActivity;
import org.infosagehealth.app.PhotoObject;
import org.infosagehealth.app.R;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * @author sanjay
 *
 */
public class PhotosAdapter extends BaseAdapter {
    private Context mContext;
    String[] mThumbIds;

    ArrayList<PhotoObject> arrPhoto = new ArrayList<PhotoObject>();
    
    public PhotosAdapter(Context c, String[] mThumbIds2, ArrayList<PhotoObject> arrPhoto) {
        mContext = c;
        mThumbIds = mThumbIds2;
        this.arrPhoto = arrPhoto;
    }


	public int getCount() {
        return arrPhoto.size();
    }

    public Object getItem(int position) {
    	return arrPhoto.get(position);//mThumbIds[position];
    }

    public long getItemId(int position) {
        return 0;
    } 

    // Create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        final ImageView imageView;// = null;
        
        if (convertView == null) {  // If it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            Resources r = Resources.getSystem();
            float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 90, r.getDisplayMetrics());

           // imageView.setBackgroundResource(R.drawable.round_corner);
           imageView.setLayoutParams(new GridView.LayoutParams((int)px, (int)px));
           imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
           imageView.setPadding(0, 2, 2, 10);
           //imageView.setPadding(8, 8, 8, 8);
           // imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            
        } else {
            imageView = (ImageView) convertView;
        }

        //imageView.setImageResource(mThumbIds[position]);
        
        
        String path = MainActivity.imgbaseurl+"/cache/w1000/images/"+arrPhoto.get(position).getImagePath();
       // Picasso.with(mContext).load(path).into(imageView,new P);
        
       // Picasso.with(this).load(url).networkPolicy(NetworkPolicy.OFFLINE) .into(imageView);
        //String path = MainActivity.imgbaseurl+"/cache/w1000/images/"+arrPhoto.get(i).getImagePath();
       Picasso.with(mContext).load(path).into(imageView, new com.squareup.picasso.Callback(){

       // Picasso.with(mContext).load(path).into(imageView);
              // Picasso.with(mContext).load(path).into(imageView, new com.squareup.picasso.Callback(){
        
        	
			@Override
			public void onError() {
				// TODO Auto-generated method stub
				
				
			}

			@Override
			public void onSuccess() {
				// TODO Auto-generated method stub
				Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
		        
		        RoundedBitmapDrawable RBD = RoundedBitmapDrawableFactory.create(mContext.getResources(),bitmap);

		        RBD.setCornerRadius(10);
		        
		        RBD.setAntiAlias(true);
		        
		        imageView.setImageDrawable(RBD);
			}
        	
        });
        
        
        
        return imageView;
//return convertView;
        
        
        
    }
}
