package com.infosage.adapter;

import org.infosagehealth.app.TaskObject;

import android.view.View;

public interface OnListItemClickListner {
	public void showlist(View v, int pos);
	public void showlist(View v, int pos,boolean isMe);
	public void showdate(View v, int pos);
	public void showclearbtn(View v, int flag);
	public void showdialog(View v, int pos);
	public void update(TaskObject taskobj, int flag, String taskid);
}
