package com.infosage.adapter;

import java.util.ArrayList;

import org.infosagehealth.app.R;
import com.infosage.item.NavDrawerItem;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

/**
 * 
 * @author Govinda P
 *
 */
public class MenuAdapter extends BaseAdapter {
	LayoutInflater inflater = null;
	Activity activity;
	Context context;
	String[] menuItemsIcon;
	ArrayList<NavDrawerItem> mPlanetTitles = new ArrayList<NavDrawerItem>();

	public MenuAdapter(Context con, Activity act, ArrayList<NavDrawerItem> mPlanetTitles) {
		this.context = con;
		activity = act;
		this.mPlanetTitles = mPlanetTitles;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mPlanetTitles.size();
	}

	@Override
	public NavDrawerItem getItem(int position) {
		// TODO Auto-generated method stub
		return mPlanetTitles.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder {

		ImageView imageHolder;
		TextView text;

	}

	public void refreshMenu() {
		this.notifyDataSetChanged();
	}

	@SuppressLint("NewApi")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View v = convertView;

		NavDrawerItem item = getItem(position);
		try {
			ViewHolder holder;
			if (convertView == null) {

				v = inflater.inflate(R.layout.element, null);

				v.setMinimumHeight(10);

				holder = new ViewHolder();
				holder.text = (TextView) v.findViewById(R.id.element_text);
				holder.imageHolder = (ImageView) v.findViewById(R.id.icon);
				int resId = 0;
				holder.imageHolder.setImageResource(resId);
				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			
			
			if(mPlanetTitles.get(position).getTitle().equals("Change Account")){
				RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				lp.setMargins(0, 40, 0, 5);				
				holder.text.setLayoutParams(lp);
				holder.imageHolder.setVisibility(View.GONE);
				holder.text.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM);
				holder.text.setTextSize(12);
				holder.text.setPadding(0, 40, 0, 40);
				holder.text.setText(item.getTitle());
				holder.text.setPaintFlags(holder.text.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
				holder.text.setTextColor(Color.parseColor("#0000ff"));
			}else {

				holder.text.setText(item.getTitle());
				holder.imageHolder.setImageResource(item.getIcon());
			}
			
		
		} catch (Exception e) {

		}

		return v;

	}

}
