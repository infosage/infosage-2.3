package com.infosage.adapter;



import java.util.ArrayList;

import com.infosage.item.ItemSearchMedication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * @author Govinda P
 **/
public class AdapterSearchDrop extends ArrayAdapter<ItemSearchMedication>{

	Context context;
	
ArrayList<ItemSearchMedication> listmedi=new ArrayList<ItemSearchMedication>();
	ItemSearchMedication item=null;
	public AdapterSearchDrop(Context context1,ArrayList<ItemSearchMedication> listMedi)
	{
		super(context1, android.R.layout.simple_dropdown_item_1line, listMedi);
		context = context1;
		
		this.listmedi=listMedi;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listmedi.size();
	}

	@Override
	public ItemSearchMedication getItem(int position) {
		// TODO Auto-generated method stub
		return listmedi.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder
	{
		
		TextView txtmed;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewHolder holder;
		item=getItem(position);
		try
		{
			
			if(convertView == null){
				v = inflater.inflate(android.R.layout.simple_dropdown_item_1line, null);				
				
				holder = new ViewHolder();
			
				holder.txtmed = (TextView)v.findViewById(android.R.id.text1);
				
				v.setTag(holder);
			}
			else
			{
				holder = (ViewHolder)v.getTag();
			}
			
			holder.txtmed.setText(""+item.getTitle());
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("v===="+v);
		return v;
	}

	
	
	
}
