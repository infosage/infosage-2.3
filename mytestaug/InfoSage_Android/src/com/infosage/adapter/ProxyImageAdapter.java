
package com.infosage.adapter;

import java.util.ArrayList;

import org.infosagehealth.app.AppController;
import org.infosagehealth.app.MainActivity;
import org.infosagehealth.app.R;
import org.infosagehealth.app.UserPermissionsSetup;
import org.infosagehealth.app.UserTasks;
import org.json.JSONObject;

import com.android.volley.Response.Listener;
import com.infosage.item.ItemPermissions;
import com.infosage.item.ItemPoxy;
import com.infosage.item.User;
import com.infosage.util.SessionManager;
import com.infosage.view.CustomAnimatedProgress;
import com.squareup.picasso.Picasso;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author Govinda P.
 *
 */
public class ProxyImageAdapter extends BaseAdapter {

	private Context mContext;
	private Activity activity;
	
	ArrayList<ItemPoxy> userobjlist = new ArrayList<ItemPoxy>();
	
	public ProxyImageAdapter(Context c, Activity act, ArrayList<ItemPoxy>list){
		mContext = c;
		userobjlist = list;
		activity = act;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return userobjlist.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return userobjlist.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		ViewHolder holder=null;
		LayoutInflater inflater = LayoutInflater.from(mContext);
		View childview=arg1;
		if (childview==null) {
			childview = inflater.inflate(R.layout.gridchild, null);
			holder = new ViewHolder();
			 holder.tv = (TextView)childview.findViewById(R.id.txtname);
			 holder.tv.setMaxLines(1);
			 holder.tv.setEllipsize(TruncateAt.END);
			 holder.img = (ImageView)childview.findViewById(R.id.img);
			 
			 childview.setTag(holder);
			 
		}
		else
		{
			holder=(ViewHolder)childview.getTag();
		}
		
		 holder.tv.setText(userobjlist.get(arg0).getFirstName()+" "+userobjlist.get(arg0).getLastName());
		if(userobjlist.get(arg0).getProfileImage() != null){
			String path = MainActivity.imgbaseurl+"/cache/w150xh150-2/images/";
			Picasso.with(mContext)
			.load(path+userobjlist.get(arg0).getProfileImage().getImage().getPath())
			.placeholder(R.drawable.default_profile)
			.into(holder.img);
		}else{
			holder.img.setImageResource(R.drawable.default_profile);
		}
		childview.setOnClickListener(new clicklistner(arg0));
		
		return childview;
	}
	public static class ViewHolder
	{
		TextView tv;
		ImageView img;
	}
	
	private class clicklistner implements View.OnClickListener
	{

		int id;
		
		public clicklistner(int id1)
		{
			id = id1;
		}
		 
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			
			SessionManager sessobj = new SessionManager(mContext);
			sessobj.setIsProxy(true);
			sessobj.setLogUserId(""+userobjlist.get(id).getId());
			
			final Intent intent = new Intent(mContext, UserTasks.class);
			Bundle b = new Bundle();
			b.putString("target",AppController.gson.toJson(userobjlist.get(id)));
			
			intent.putExtras(b);
			

			try {
			/**
			 * User change Proxy set
			 */
				String user=AppController.gson.toJson(userobjlist.get(id));
				final User selectiuser = AppController.gson.fromJson(user,User.class);
				AppController.setUser(selectiuser);
				AppController.setViewUser(selectiuser);
			
				final CustomAnimatedProgress progress=new CustomAnimatedProgress(activity);
				UserPermissionsSetup.getPermissionByResponse(progress,""+selectiuser.getId(),""+AppController.getMainUser().getId(), new Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject arg0) {
						// TODO Auto-generated method stub
						if (progress!=null) {
							progress.dismiss();
						}
						
						AppController.permissions=AppController.gson.fromJson(arg0.toString(), ItemPermissions.class);
						AppController.permissions.setLoguserid(""+AppController.getMainUser().getId());
						AppController.permissions.setSelectid(""+selectiuser.getId());
						activity.startActivity(intent);
						activity.finish();
					}
				});
				
			} catch (Exception e) {
				// TODO: handle exception
				
			}
			
			
		}
		
	}

}
