/**
*@authour sanjay
*23-May-2016
* @update date 23-May-2016
 */
package com.infosage.adapter;

import org.infosagehealth.app.TaskObject;

/**
 * @author sanjay
 *
 */
public interface PhotoUpdateListener {

	public void updatePhoto(String picurl, int flag);
}
