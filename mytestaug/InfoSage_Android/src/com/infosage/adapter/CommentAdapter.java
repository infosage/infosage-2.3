package com.infosage.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.infosagehealth.app.AppController;
import org.infosagehealth.app.CommentClickListener;
import org.infosagehealth.app.MainActivity;
import org.infosagehealth.app.R;

import com.infosage.item.Comments;
import com.infosage.util.Constants;
import com.squareup.picasso.Picasso;

/**
 * 
 * @author Govinda P
 *
 */
public class CommentAdapter  extends BaseAdapter{

	Context context;
	CommentClickListener mlistener;
	
	List<Comments> list=new ArrayList<Comments>();
	public int postid=0,post_position=0;
	public CommentAdapter(Context context1,CommentClickListener mlistener1,List<Comments> list,int postid,int post_position)
	{
		context = context1;
		mlistener = mlistener1;
		this.postid=postid;
		this.post_position=post_position;
		this.list=list;
	}
	
	public int getPostid() {
		return this.postid;
	} 
	public int getPostPosition() {
		return this.post_position;
	} 
	
	 @Override
	    public int getViewTypeCount() {
	        // menu type count
	        return 2;
	    }

	    @Override
	    public int getItemViewType(int position) {
	        // current menu type
	    	int type=0;
	    	if (String.valueOf(AppController.getUser().getId()).equals(""+list.get(position).getUser().getId())) {
	    		type=0;
	    	}else
	    	{
	    		type=1;
	    	}
	        return type;
	    }
	
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Comments getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder
	{
		/**********DECLARES*************/
		private ImageView profimg,btncomm,btnedit;
		private TextView txtname;
	
		private TextView txtdate;
		Button btnusr, btncal;
		RelativeLayout relcomm;
		TextView txtcomm,txtdesc;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewHolder holder;
		
		try
		{
			Comments item=getItem(position);
			if(convertView == null){
				v = inflater.inflate(R.layout.updatelistchild, null);
				holder = new ViewHolder();
				
				/**********INITIALIZES*************/
				holder.	profimg = (ImageView)v.findViewById(R.id.profimg);
				holder.txtname = (TextView)v.findViewById(R.id.txtname);
				
				holder.txtdate = (TextView)v.findViewById(R.id.txtdate);
			
				holder.txtcomm = (TextView)v.findViewById(R.id.txtcomm);
				holder.btncomm = (ImageView)v.findViewById(R.id.btncomm);
				holder.btncomm.setVisibility(View.GONE);
				holder.txtcomm = (TextView)v.findViewById(R.id.txtcomm);
				holder.txtcomm.setVisibility(View.GONE);
				holder.txtdesc = (TextView)v.findViewById(R.id.txtdesc);
				holder.btnedit=(ImageView)v.findViewById(R.id.img_edit);
				holder.btnedit.setVisibility(View.INVISIBLE);
				v.setTag(holder);
			}
			else
			{
				holder = (ViewHolder)v.getTag();
			}
			
			try {
				if (item.getUser().getProfileImage()!=null) {
					Picasso.with(context).load(MainActivity.imgbaseurl+"/cache/w100xh100-2/images/"+item.getUser().getProfileImage().getImage().getPath())
					.placeholder(R.drawable.default_profile)
					.into(holder.profimg);
				}
				else
				{
					holder.profimg.setImageResource(R.drawable.default_profile);
				}
				
			} catch (Exception e) {
				// TODO: handle exception
				
			}	
			if (String.valueOf(AppController.getUser().getId()).equals(""+item.getUser().getId())) 
			{
				holder.btnedit.setVisibility(View.VISIBLE);
				holder.btnedit.setOnClickListener(new editlisner(position,""+item.getCommentText()));
			}
			else
			{
				holder.btnedit.setVisibility(View.GONE);
			}
				
				holder.txtdesc.setText(""+item.getCommentText());
				holder.txtdesc.setOnClickListener(new listener(null, position));
				holder.txtname.setText(""+item.getUser().getFirstName()+" "+item.getUser().getLastName());
				holder.txtdate.setText(""+Constants.getDate(item.getCommentTimeEdited()));
				
		}
		catch(Exception e)
		{
			
		}
		
		return v;
	}
	
	private class listener implements View.OnClickListener
	{
		RelativeLayout rel;
		int pos;
		int type=0;
		private listener(RelativeLayout rel1, int pos1)
		{
			rel = rel1;
			pos = pos1;
			this.type=getItemViewType(pos);
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			mlistener.OnCommentClickListener(rel, pos,type);
			System.out.println("Edit lisner");
		}
		
	}
	
	private class editlisner implements View.OnClickListener
	{		
		int pos;
		String comm="";
		int type=0;
		private editlisner(int pos1,String comm)
		{			
			pos = pos1;
			this.comm=comm;
			this.type=getItemViewType(pos);
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			mlistener.OnCommentEditListener(null, pos, comm,true,type);
			
		}
	}

}
