package com.infosage.adapter;



import java.util.ArrayList;

import org.infosagehealth.app.R;

import com.infosage.item.ItemMeditation;
import com.infosage.util.Constants;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @use use in medication page. This is notmal adapter class. bind data with list view.
 * @author Govinda P
 */
public class AdapterMeditation extends BaseAdapter{

	Context context;
	Activity activity;
	OnListItemClickListner mlistener;
	int flag;
	ArrayList<ItemMeditation> listmedi=new java.util.ArrayList<ItemMeditation>();
	ItemMeditation item=null;
	//Dialog
	Dialog dialog=null; 
	
	
	public AdapterMeditation(Context context1, Activity act, OnListItemClickListner listener, ArrayList<ItemMeditation> listMedi)
	{
		context = context1;
		activity = act;
		mlistener = listener;
		this.listmedi=listMedi;
		dialog= new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listmedi.size();
	}

	@Override
	public ItemMeditation getItem(int position) {
		// TODO Auto-generated method stub
		return listmedi.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder
	{
		ImageView btninfo;
		TextView txtmed;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewHolder holder;
		item=getItem(position);
		try
		{
			
			if(convertView == null){
				v = inflater.inflate(R.layout.medchild, null);				
				
				holder = new ViewHolder();
				holder.btninfo = (ImageView)v.findViewById(R.id.btninfo);
				holder.txtmed = (TextView)v.findViewById(R.id.txtmed);
				
				v.setTag(holder);
			}
			else
			{
				holder = (ViewHolder)v.getTag();
			}
			
			holder.txtmed.setText(""+item.getName());
			
			
			holder.btninfo.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					item=getItem(position);
					LayoutInflater inflater = LayoutInflater.from(context);
					View details = inflater.inflate(R.layout.medicationdetails, null);
					
					TextView txtdose = (TextView)details.findViewById(R.id.txtdose);
					String dosestr = "<b>Dosage : </b>"+item.getDosage();
					txtdose.setText(Html.fromHtml(dosestr));

					TextView txtfreq = (TextView)details.findViewById(R.id.txtfreq);
					String freqstr = "<b>Frequency : </b>"+item.getDrugFrequency().getName();
					txtfreq.setText(Html.fromHtml(freqstr));
					
					TextView txtsymp = (TextView)details.findViewById(R.id.txtsymp);
					if (item.getDrugSymptom().getName().toString().toLowerCase().equals("n/a")) {
						txtsymp.setVisibility(View.GONE);
					}else
					{
						txtsymp.setVisibility(View.VISIBLE);
						String sympstr = "<b>Symptom : </b>"+item.getDrugSymptom().getName();
						txtsymp.setText(Html.fromHtml(sympstr));
					}
					
					TextView txtrout = (TextView)details.findViewById(R.id.txtrout);
					String routstr = "<b>Route : </b> "+item.getRoute();
					txtrout.setText(Html.fromHtml(routstr));
					
					TextView txttype = (TextView)details.findViewById(R.id.txttype);
					String typestr = "<b>Type : </b>"+item.getType();
					txttype.setText(Html.fromHtml(typestr));
					
					TextView txtnote = (TextView)details.findViewById(R.id.txtnote);
					
					if (item.getOtherNotes().trim().toString().equals("")) {
						String notestr = "<b>Notes : </b>None";
						txtnote.setText(Html.fromHtml(notestr));
					}else{
					String notestr = "<b>Notes : </b>"+item.getOtherNotes();
					txtnote.setText(Html.fromHtml(notestr));
					}
					
					
					try {
						TextView txtcreate = (TextView)details.findViewById(R.id.txtcreate);
						String createstr = "<b>Created on : </b>"+Constants.getDate(item.getCreationDate());
						txtcreate.setText(Html.fromHtml(createstr));
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				
					
					try {
						TextView txtedit = (TextView)details.findViewById(R.id.txtedited);
						String editstr = "<font color=#000000><b>Edited on : </b>"+Constants.getDate(item.getEditDate())+"</font>";
						txtedit.setText(Html.fromHtml(editstr));
						
						if (item.getCreationDate()==item.getEditDate()) {
							txtedit.setVisibility(View.GONE);
				}else
				{
					txtedit.setVisibility(View.VISIBLE);						
				}
						
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				
					
					TextView txtheader = (TextView)details.findViewById(R.id.txttitle);
					String header =item.getName()+"(Active) ";
					if (!item.isIsActive()) {
						header =item.getName()+"(Inactive)";
						if (!item.isIsPublic()) {
							header =item.getName()+"(Private)(Inactive)";
						}
					}else
					{
						if (!item.isIsPublic()) {
							header =item.getName()+"(Private)(Active)";
						}
					}		
					
					txtheader.setText(Html.fromHtml("<b>"+header+"</b>"));
					Button btn=(Button)details.findViewById(R.id.btn_dialog_close);
					btn.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});
					dialog.setContentView(details);
					dialog.show();
				}
			});
			
		}
		catch(Exception e)
		{
			
		}
		
		return v;
	}

	
	
	
}
