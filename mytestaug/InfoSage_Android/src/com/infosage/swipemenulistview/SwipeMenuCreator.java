package com.infosage.swipemenulistview;


/**
 * 
 * @author Govinda P.
 *
 */
public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
