package com.infosage.util;

/**
 * 
 * @author Govinda P.
 *
 */
public class FindRedirectPage 
{
	static String[] method=new String[3];
	static String link="";
	static String user_id="";
	
	
	public static String[] getLink(String key_text,String key_forwordlink){
		
		
		if (key_text.contains("medication")) {
						
			method[0]="medication";
			method[1]=stripNonDigits(key_forwordlink);;
			method[2]="";
		}else if(key_text.contains("task")) {
			
			method[0]="todo";
					if (key_forwordlink.contains("profile")) 
					{
						method[1]=stripNonDigits(key_forwordlink);
						method[2]="profile";
					}else{
						method[1]=stripNonDigits(key_forwordlink);
						method[2]="";
					}
				
		}else if(key_text.contains("post")) {
			
			method[0]="post";
			method[1]=stripNonDigits(key_forwordlink);
		}
		
		if (key_forwordlink.contains("communication")) {
			method[0]="update";
			method[1]=stripNonDigits(key_forwordlink);
			method[2]="4";
		}
		
		return method;
	}
	
	public static String stripNonDigits(
            final CharSequence input /* inspired by seh's comment */){
    final StringBuilder sb = new StringBuilder(
            input.length() /* also inspired by seh's comment */);
    for(int i = 0; i < input.length(); i++){
        final char c = input.charAt(i);
        if(c > 47 && c < 58){
            sb.append(c);
        }
    }
    return sb.toString();
}
}
