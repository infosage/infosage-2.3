package com.infosage.util;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * 
 * @author Govinda P.
 *
 */
public class Constants {
	
	public static String vendorID = "0S7ZXK2WXU5RECYIYXPJ-YTZZQOJYHLCG23HYC9SJ";
	
	//URLS
	//public static String HOST_URL="http://infosage-stage-apps.hsl.harvard.edu:80/api";
	//public static String URL_img="http://infosage-stage-apps.hsl.harvard.edu:80";
	//public static String URL_SIGNUP = "http://infosage-stage-apps.hsl.harvard.edu/#/access/signup/reg";
	
	//public static String HOST_URL="https://www.infosagehealth.org";
	public static String HOST_URL="http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com/api";
	public static String URL_img="http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com";
	public static String URL_SIGNUP ="https://www.infosagehealth.org";
			
	
	/*public static String URL_SIGNUP = "https://www.infosagehealth.org/#/access/signup/reg";	
	public static String URL_img="https://www.infosagehealth.org";
	public static String HOST_URL="https://www.infosagehealth.org/api";*/
	
	
	public static String getUserProfile(String userid)
	{
		return HOST_URL+"/users/"+userid;				
	}
	
	public static String getURL_PushRegistration(String userid)
	{
		return HOST_URL+"/users/"+userid+"/push-notifications/apns/register/android";				
	}	
	public static String getURL_PushUnRegistration(String userid)
	{
		return HOST_URL+"/users/"+userid+"/push-notifications/apns/unregister/android";				
	}	
				// User Privileges
	public static String getURL_Privileges(String sourceUserid,String targetUserId)
	{
		return HOST_URL+"/users/"+sourceUserid+"/privileges/users/"+targetUserId;				
	}			
		
		// Update Post
		public static String getURL_Post_POST(String userid)
		{
			return HOST_URL+"/users/"+userid+"/posts";
		}
		
		public static String getURL_Post_DELETE(String userid,String postid)
		{
			return HOST_URL+"/users/"+userid+"/posts/"+postid+"";
		}
		
		
		// Update Comments
		public static String getURL_Comment_POST(String userid,int postid)
		{
			return HOST_URL+"/users/"+userid+"/posts/"+postid+"/comments";
		}
		public static String getURL_Comment_DELETE(String userid,String postid,String cmtid)
		{
			//HOST_URL/users/:userId/posts/:postId/comments/:commentId
			return HOST_URL+"/users/"+userid+"/posts/"+postid+"/comments/"+cmtid;
		}
		
		// Notification
		public static String getURL_Notification_Fetch(String userid,String count)
		{
			if (count.equals("")) {
				return HOST_URL+"/users/"+userid+"/notifications";
			}else{
				return HOST_URL+"/users/"+userid+"/notifications?count="+count;}
			}
		public static String getURL_UnresolveNotification(String userid)
		{
			return HOST_URL+"/users/"+userid+"/notifications/unresolved";
		}
		
		public static String getURL_Notification_Read(String userid,String id)
		{
			return HOST_URL+"/users/"+userid+"/notifications/"+id+"";
		}
						
		// Medications section
		public static String getURL_MedicationList(String userid)
		{
			return HOST_URL+"/users/"+userid+"/drugImo";
		}
		public static String getURL_DeleteMedicationDrug(String userid,String drugid)
		{
			return HOST_URL+"/users/"+userid+"/drugImo/"+drugid;
		}
						
		public static String getURL_Medi_search(String str)
		{
			return HOST_URL+"/drugImo/search/"+str;
		}	
		
		public static String getURL_Medi_drugopt(String drugcode)
		{
			return HOST_URL+"/drugImo/details/"+drugcode;
		}
						
		public static String getURL_Medi_Public(String userid,String drugid)
		{
			return HOST_URL+"/users/"+userid+"/drugImo/"+drugid+"/public";
		}
		
		public static String getURL_Medi_Active(String userid,String drugid)
		{
			return HOST_URL+"/users/"+userid+"/drugImo/"+drugid+"/active";
		}
		
		public static String getURL_Medi_Check(String userid,String drugid)
		{
			return HOST_URL+"/users/"+userid+"/drugImo/drugFormExists/"+drugid;
			///users/:userId/drugImo/drugFormExists/:userDrugId
		}
						
		// Social Media Login
		public static String getURL_SocialLogin(String userid)
		{
			return HOST_URL+"/login/oauth";				
		}	
	
		// Logout
		public static String getURL_logout(String userid)
		{
			if (userid.equals("")) {
					userid="-1";
			}
			return HOST_URL+"/logout/"+userid;
		}
						
							
		/**
		 * @use convert into EEE, dd MMM yyyy, hh:mm a format. 
		 * @param str datetime in string format
		 * @return
		 */
		public static String getDate(String str)
		{
			Date dt = new Date(str);
			//Sat, 04 Apr 2015 05:15:50 EDT
			SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
			sdf.setTimeZone(TimeZone.getDefault());
			String time1 = sdf.format(dt);
			return time1;
		}
		/**
		 * @use convert into EEE, dd MMM yyyy, hh:mm a format. 
		 * @param str datetime in long format (milisec)
		 * @return
		 */
		public static String getDate(long str)
		{
			try {
				Date dt = new Date(str);
				//Sat, 04 Apr 2015 05:15:50 EDT
				SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy, hh:mm a");
				sdf.setTimeZone(TimeZone.getDefault());
				String time1 = sdf.format(dt);
				return time1;
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return "";
			}
		}
	
		/**
		 * @return Frequency array list. Used in add/edit medications page.  
		 */
		public static List<String> getFrequencyArray() 
		{	
			List<String> temp=Arrays.asList(new String[]{"Unknown",
                    "As needed",
                    "One time",
                    "Once daily",
                    "Twice daily",
                    "Three times daily",
                    "Four times daily",
                    "At bed time",
                    "Every other day",
                    "Once a week",
                    "Every four (4) hours",
                    "Every six (6) hours",
                    "Every eight (8) hours",
                    "Every twelve (12) hours",
                    "Every month"});
			return temp;
		}
		
		/**
		 * @return Symptoms array list. Used in add/edit medications page.  
		 */
	public static List<String> getSymptomsArray() 
	{
		List<String> temp=Arrays.asList(new String[]{"Unknown",
			        "Chest Pain",
			        "Constipation",
			        "Cough",
			        "Fever",
			        "Headache",
			        "Heartburn",
			        "Indigestion",
			        "Insomnia",
			        "Nausea",
			        "Pain",
			        "Shortness of Breath",
			        "Other"});
			return temp;
		}
		
}
