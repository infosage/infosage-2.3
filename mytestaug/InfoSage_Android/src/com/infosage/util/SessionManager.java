package com.infosage.util;
import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
/*
 * Modify Date: 21/08/2015
 */
public class SessionManager {
	// Shared Preferences
	SharedPreferences pref;
	
	// Editor for Shared preferences
	Editor editor;
	
	// Context
	Context _context;
	
	// Shared pref mode
	int PRIVATE_MODE = 0;
	
private static final String PREF_NAME = "infosage";
	
	// All Shared Preferences Keys
	private static final String IS_LOGIN = "IsLoggedIn";
	
	
public static String SELECTED_USERID = "0";

	// All Shared Preferences Keys
	public static String MY_USERID = "0";
	
	
	// User name (make variable public to access from outside)
	public static final String KEY_USERNAME = "username";
	
	public static final String KEY_FULLNAME = "fullname";
	
	public static final String KEY_MyUserID = "myuserid";
	public static final String KEY_LogUserID = "luserid";
	
	public static final String KEY_isProxy = "isProxy";
	
	public static final String KEY_FB_ID = "fbid";
	
	public static final String KEY_EMAILID = "email";
	
	// DEVICE ID (make variable public to access from outside)
	public static final String KEY_DEVICE_ID = "device_id";
	
	public static final String KEY_token_ID = "token_id";	
	
	public static final String KEY_phone = "phone";	
	
	public static final String KEY_PUSH_FLAG = "push";
	
	public static final String KEY_IsKeystone = "false";
	public static final String KEY_proxycnt = "proxy";
	
	/**
	 * @use create session manager class with current page and access session information.
	 * @param context activity context
	 */
	public SessionManager(Context context){
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}
	public void clearSession()
	{
		editor.clear();
		editor.apply();
		 /*anu........*/
		 String helpstatus = pref.getString("helpstatus", "");
		 Log.d("helpstatus", ""+helpstatus);
		 
		 editor = pref.edit();
		 editor.putString("helpstatus", helpstatus);
		 Log.d("helpstatus", ""+helpstatus);
		 editor.commit();
		 /*anu.......*/
	}
	
	
	/**
	 * Create login session
	 * */
	public void createLoginSession(String myuserid,String luserid,String name){
		// Storing login value as TRUE
		
		// Storing name in pref
	
		editor.putString(KEY_MyUserID, myuserid);
		editor.putString(KEY_LogUserID,luserid);
		editor.putBoolean(IS_LOGIN, true);
				
		// commit changes
		editor.apply();
	}	
	
	/**
	 * Get stored session data 
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String, String> user = new HashMap<String, String>();
		// user name
		
		user.put(KEY_MyUserID, pref.getString(KEY_MyUserID, "0"));
		//user.put(KEY_FB_ID, pref.getString(KEY_token_ID, null));
		// user Device id
		user.put(KEY_LogUserID, pref.getString(KEY_LogUserID, "0"));
		
		return user;
	}
	
	public String getMyUserId()
	{
		return pref.getString(KEY_MyUserID, "0");
	}
	public void setMyUserId(String myuserid)
	{
		editor.putString(KEY_MyUserID,myuserid);
		editor.apply();
	}
	
	/**
	 * @use fetch login user id from local session
	 * @return login user id: this is change if user change account and access as a proxy
	 */
	public String getLogUserId()
	{
		return pref.getString(KEY_LogUserID, "0");
	}
	
	/**
	 * @use : save login user id. this is change if user work as a proxy
	 * @param loguserid
	 */
	public void setLogUserId(String loguserid)
	{
		editor.putString(KEY_LogUserID,loguserid);
		editor.apply();
	}
	
	/**
	 * @use to check user is normal or proxy
	 * @return TRUE if user is login as proxy.   
	 * FALSE if user login as Main login user
	 */
	public Boolean getIsProxy()
	{
		return pref.getBoolean(KEY_isProxy,false);
	}
	
	/**
	 * @use set user is proxy user or not. this is true and false from change account page.
	 * @param proxy
	 */
	public void setIsProxy(Boolean proxy)
	{
		editor.putBoolean(KEY_isProxy,proxy);
		editor.apply();
	}
	
	public String getMyUserid()
	{
		return pref.getString(KEY_MyUserID,"0");
	}
	public void setMyUserid(String myuid)
	{
		editor.putString(KEY_MyUserID,myuid);
		editor.apply();
		//editor.commit();
		
	}
	
	/**
	 * 
	 * @return is Keystone or not
	 */
	public String getIsKeystone()
	{
		return pref.getString(KEY_IsKeystone, "0");
	}
	/**
	 * @use set is keystone or not (isElder or Not)
	 * @param keystone
	 */
	public void setIsKeystone(String keystone)
	{
		editor.putString(KEY_IsKeystone,keystone);
		editor.apply();
	}
	
	public int getProxycnt()
	{
		return pref.getInt(KEY_proxycnt,0);
	}
	public void setProxycnt(int proxy)
	{
		editor.putInt(KEY_proxycnt,proxy);
		editor.apply();
	}
	
	
	public String getDeviceid()
	{
		return pref.getString(KEY_DEVICE_ID,"");
	}
	public void setDeviceid(String deviceid)
	{
		editor.putString(KEY_DEVICE_ID,deviceid);
		editor.apply();
	}
	
	public String getFacebookId()
	{
		return pref.getString(KEY_FB_ID,"");
	}
	public void setFacebookID(String fbid)
	{
		editor.putString(KEY_FB_ID,fbid);
		editor.apply();
	}
	
	/**
	 * @use to check user is successful register with push notification or not
	 * This is very imp. If user is register TRUE then first call un-register call at Logout process.
	 * if return false then direct call Logout process.
	 * @return is register or not
	 */
	public boolean isPushRegister()
	{
		return pref.getBoolean(KEY_PUSH_FLAG,false);
	}
	/**
	 * @use set user push notification register or not with server.
	 * @param isRegister is true if register with server. False if not register
	 */
	public void SetPushRegister(boolean isRegister)
	{
		editor.putBoolean(KEY_PUSH_FLAG,isRegister);
		editor.apply();
	}

	/**
	 * use to get push notification token for unregister or register.
	 * @return registered push notification token
	 * 
	 */
	public String getPushToken()
	{
		return pref.getString("ptoken","");
		
	}
	/**
	 * @use set push notification token for unregister or register.
	 */
	public void SetPushToken(String token)
	{
		editor.putString("ptoken",token);
		editor.apply();
	}
	
	/**
	 * getToken From Local
	 */
	public String getTokenAuthorization()
	{
		return "Bearer "+pref.getString(KEY_token_ID,"");
	}
	
	public String getFacebookProfilePicture(String userID)
	{
		String res="https://graph.facebook.com/" + userID + "/picture?type=large";
		return res;
	}
	
}