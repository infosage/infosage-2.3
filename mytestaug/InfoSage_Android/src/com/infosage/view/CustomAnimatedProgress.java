/**
 * @author Govinda A. Paliwal
 * @date 10 Sept 2015
 */

package com.infosage.view;

import org.infosagehealth.app.R;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.WindowManager;
import android.widget.TextView;
public class CustomAnimatedProgress extends AlertDialog 
{
Context context;
TextView txt_title;
public String title="";
    public CustomAnimatedProgress(Context context) 
    {
		super(context,R.style.CustomAlertDialogStyle);
		
		// TODO Auto-generated constructor stub
		this.context=context;
		
	     //this.getWindow().setAttributes(lp);
	}
	@Override
    public void show()
	{
		
		try {
		if (this.context!=null) {
			 WindowManager.LayoutParams lp = this.getWindow().getAttributes();
		     this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		 lp.dimAmount =0;
	     super.show();
	    
	     setContentView(R.layout.progressbar);
	     txt_title=(TextView)findViewById(R.id.textView1);
	     if (!title.equals("")) {
	    	  txt_title.setText(title);
		}
		}	
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
   
		//remove the dim effect
  
  // this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
  // this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);  
    }
	public void show(String title)
	{
		this.title=title;
	   this.show();
	}
	
}