
package com.infosage.item;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;


public class ItemPertaining {

    @Expose
    private String name;
    @Expose
    private String code;
    
    @Expose
    private List<String> genericNames = new ArrayList<String>();
   
    @Expose
    private List<String> brandNames = new ArrayList<String>();
   
    @Expose
    private List<Form> forms = new ArrayList<Form>();

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public ItemPertaining withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 
     * @return
     *     The code
     */
    public String getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    public ItemPertaining withCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * 
     * @return
     *     The genericNames
     */
    public List<String> getGenericNames() {
        return genericNames;
    }

    /**
     * 
     * @param genericNames
     *     The genericNames
     */
    public void setGenericNames(List<String> genericNames) {
        this.genericNames = genericNames;
    }

    public ItemPertaining withGenericNames(List<String> genericNames) {
        this.genericNames = genericNames;
        return this;
    }

    /**
     * 
     * @return
     *     The brandNames
     */
    public List<String> getBrandNames() {
        return brandNames;
    }

    /**
     * 
     * @param brandNames
     *     The brandNames
     */
    public void setBrandNames(List<String> brandNames) {
        this.brandNames = brandNames;
    }

    public ItemPertaining withBrandNames(List<String> brandNames) {
        this.brandNames = brandNames;
        return this;
    }

    /**
     * 
     * @return
     *     The forms
     */
    public List<Form> getForms() {
        return forms;
    }

    /**
     * 
     * @param forms
     *     The forms
     */
    public void setForms(List<Form> forms) {
        this.forms = forms;
    }

    public ItemPertaining withForms(List<Form> forms) {
        this.forms = forms;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).append(code).append(genericNames).append(brandNames).append(forms).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ItemPertaining) == false) {
            return false;
        }
        ItemPertaining rhs = ((ItemPertaining) other);
        return new EqualsBuilder().append(name, rhs.name).append(code, rhs.code).append(genericNames, rhs.genericNames).append(brandNames, rhs.brandNames).append(forms, rhs.forms).isEquals();
    }

}
