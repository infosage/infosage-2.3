
package com.infosage.item;

import java.util.ArrayList;
import java.util.List;


import com.google.gson.annotations.Expose;

import org.apache.commons.lang.builder.ToStringBuilder;


public class ItemUpdate {

    @Expose
    private int postId;
    @Expose
    private User user;
    @Expose
    private LoggedInUser loggedInUser;
    @Expose
    private WallUser wallUser;
    @Expose
    private String postText;
    @Expose
    private boolean isDeleted;
    @Expose
    private List<Comments> comments = new ArrayList<Comments>();
    @Expose
    private String postTime;
    @Expose
    private String postTimeEdited;

    
    
    /**
     * 
     * @return
     *     get active count
     */
   
    /**
     * 
     * @return
     *     The postId
     */
    public int getPostId() {
        return postId;
    }

    /**
     * 
     * @param postId
     *     The postId
     */
    public void setPostId(int postId) {
        this.postId = postId;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * 
     * @return
     *     The loggedInUser
     */
    public LoggedInUser getLoggedInUser() {
        return loggedInUser;
    }

    /**
     * 
     * @param loggedInUser
     *     The loggedInUser
     */
    public void setLoggedInUser(LoggedInUser loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    /**
     * 
     * @return
     *     The wallUser
     */
    public WallUser getWallUser() {
        return wallUser;
    }

    /**
     * 
     * @param wallUser
     *     The wallUser
     */
    public void setWallUser(WallUser wallUser) {
        this.wallUser = wallUser;
    }

    /**
     * 
     * @return
     *     The postText
     */
    public String getPostText() {
        return postText;
    }

    /**
     * 
     * @param postText
     *     The postText
     */
    public void setPostText(String postText) {
        this.postText = postText;
    }

    /**
     * 
     * @return
     *     The isDeleted
     */
    public boolean getIsDeleted() {
        return isDeleted;
    }

    /**
     * 
     * @param isDeleted
     *     The isDeleted
     */
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    /**
     * 
     * @return
     *     The comments
     */
    public List<Comments> getComments() {
        return comments;
    }

    /**
     * 
     * @param comments
     *     The comments
     */
    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    /**
     * 
     * @return
     *     The postTime
     */
    public String getPostTime() {
        return postTime;
    }

    /**
     * 
     * @param postTime
     *     The postTime
     */
    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    /**
     * 
     * @return
     *     The postTimeEdited
     */
    public String getPostTimeEdited() {
        return postTimeEdited;
    }

    /**
     * 
     * @param postTimeEdited
     *     The postTimeEdited
     */
    public void setPostTimeEdited(String postTimeEdited) {
        this.postTimeEdited = postTimeEdited;
    }
    
    
    /**
     * @return count of undeleted comments from comments array
     *   
     */
    public int getCommentsSize()
    {
    	int count=0;
        for (Comments comments2 : comments) {
			if (!comments2.isIsDeleted()) {
				count++;
			}
		}
        return count;
    }
    
   
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
