
package com.infosage.item;

import com.google.gson.annotations.Expose;
import org.apache.commons.lang.builder.ToStringBuilder;


public class Image {

    @Expose
    private int id;
    @Expose
    private String path;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The path
     */
    public String getPath() 
    {	if(path!=null)
        return path;
    	else
    		return "";
    }

    /**
     * 
     * @param path
     *     The path
     */
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
