
package com.infosage.item;

import com.google.gson.annotations.Expose;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


public class Owner {

    @Expose
    private long id;
    @Expose
    private String firstName;
    @Expose
    private String middleName;
    @Expose
    private String lastName;
    @Expose
    private String title;
    @Expose
    private String emailAddress;
    @Expose
    
    private ProfileImage profileImage;
    @Expose
    private boolean enabled;
    @Expose
    private boolean isElder;
    @Expose
    private boolean isTest;
    @Expose
    private boolean isAdministrator;
    @Expose
    private boolean sendEmails;
    @Expose
    private String creationDate;

    /**
     * 
     * @return
     *     The id
     */
    public long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(long id) {
        this.id = id;
    }

    public Owner withId(long id) {
        this.id = id;
        return this;
    }

    /**
     * 
     * @return
     *     The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * 
     * @param firstName
     *     The firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Owner withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * 
     * @return
     *     The middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * 
     * @param middleName
     *     The middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Owner withMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    /**
     * 
     * @return
     *     The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * 
     * @param lastName
     *     The lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Owner withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public Owner withTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * 
     * @return
     *     The emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * 
     * @param emailAddress
     *     The emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Owner withEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    /**
     * 
     * @return
     *     The profileImage
     */
    public ProfileImage getProfileImage() {
        return profileImage;
    }

    /**
     * 
     * @param profileImage
     *     The profileImage
     */
    public void setProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
    }

    public Owner withProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
        return this;
    }

    /**
     * 
     * @return
     *     The enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * 
     * @param enabled
     *     The enabled
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Owner withEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * 
     * @return
     *     The isElder
     */
    public boolean isIsElder() {
        return isElder;
    }

    /**
     * 
     * @param isElder
     *     The isElder
     */
    public void setIsElder(boolean isElder) {
        this.isElder = isElder;
    }

    public Owner withIsElder(boolean isElder) {
        this.isElder = isElder;
        return this;
    }

    /**
     * 
     * @return
     *     The isTest
     */
    public boolean isIsTest() {
        return isTest;
    }

    /**
     * 
     * @param isTest
     *     The isTest
     */
    public void setIsTest(boolean isTest) {
        this.isTest = isTest;
    }

    public Owner withIsTest(boolean isTest) {
        this.isTest = isTest;
        return this;
    }

    /**
     * 
     * @return
     *     The isAdministrator
     */
    public boolean isIsAdministrator() {
        return isAdministrator;
    }

    /**
     * 
     * @param isAdministrator
     *     The isAdministrator
     */
    public void setIsAdministrator(boolean isAdministrator) {
        this.isAdministrator = isAdministrator;
    }

    public Owner withIsAdministrator(boolean isAdministrator) {
        this.isAdministrator = isAdministrator;
        return this;
    }

    /**
     * 
     * @return
     *     The sendEmails
     */
    public boolean isSendEmails() {
        return sendEmails;
    }

    /**
     * 
     * @param sendEmails
     *     The sendEmails
     */
    public void setSendEmails(boolean sendEmails) {
        this.sendEmails = sendEmails;
    }

    public Owner withSendEmails(boolean sendEmails) {
        this.sendEmails = sendEmails;
        return this;
    }

    /**
     * 
     * @return
     *     The creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * 
     * @param creationDate
     *     The creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Owner withCreationDate(String creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(firstName).append(middleName).append(lastName).append(title).append(emailAddress).append(profileImage).append(enabled).append(isElder).append(isTest).append(isAdministrator).append(sendEmails).append(creationDate).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Owner) == false) {
            return false;
        }
        Owner rhs = ((Owner) other);
        return new EqualsBuilder().append(id, rhs.id).append(firstName, rhs.firstName).append(middleName, rhs.middleName).append(lastName, rhs.lastName).append(title, rhs.title).append(emailAddress, rhs.emailAddress).append(profileImage, rhs.profileImage).append(enabled, rhs.enabled).append(isElder, rhs.isElder).append(isTest, rhs.isTest).append(isAdministrator, rhs.isAdministrator).append(sendEmails, rhs.sendEmails).append(creationDate, rhs.creationDate).isEquals();
    }

}
