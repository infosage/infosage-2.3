package com.infosage.item;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;


public class Comments 
{

@Expose
private long commentId;
@Expose
private User user;
@Expose
private LoggedInUser loggedInUser;
@Expose
private Object replyUser;
@Expose
private String commentText;
@Expose
private boolean isDeleted;
@Expose
private String commentTime;
@Expose
private String commentTimeEdited;

/**
*
* @return
* The commentId
*/
public long getCommentId() {
return commentId;
}

/**
*
* @param commentId
* The commentId
*/
public void setCommentId(long commentId) {
this.commentId = commentId;
}

public Comments withCommentId(long commentId) {
this.commentId = commentId;
return this;
}

/**
*
* @return
* The user
*/
public User getUser() {
return user;
}

/**
*
* @param user
* The user
*/
public void setUser(User user) {
this.user = user;
}

public Comments withUser(User user) {
this.user = user;
return this;
}

/**
*
* @return
* The loggedInUser
*/
public LoggedInUser getLoggedInUser() {
return loggedInUser;
}

/**
*
* @param loggedInUser
* The loggedInUser
*/
public void setLoggedInUser(LoggedInUser loggedInUser) {
this.loggedInUser = loggedInUser;
}

public Comments withLoggedInUser(LoggedInUser loggedInUser) {
this.loggedInUser = loggedInUser;
return this;
}

/**
*
* @return
* The replyUser
*/
public Object getReplyUser() {
return replyUser;
}

/**
*
* @param replyUser
* The replyUser
*/
public void setReplyUser(Object replyUser) {
this.replyUser = replyUser;
}

public Comments withReplyUser(Object replyUser) {
this.replyUser = replyUser;
return this;
}

/**
*
* @return
* The commentText
*/
public String getCommentText() {
return commentText;
}

/**
*
* @param commentText
* The commentText
*/
public void setCommentText(String commentText) {
this.commentText = commentText;
}

public Comments withCommentText(String commentText) {
this.commentText = commentText;
return this;
}

/**
*
* @return
* The isDeleted
*/
public boolean isIsDeleted() {
return isDeleted;
}

/**
*
* @param isDeleted
* The isDeleted
*/
public void setIsDeleted(boolean isDeleted) {
this.isDeleted = isDeleted;
}

public Comments withIsDeleted(boolean isDeleted) {
this.isDeleted = isDeleted;
return this;
}

/**
*
* @return
* The commentTime
*/
public String getCommentTime() {
return commentTime;
}

/**
*
* @param commentTime
* The commentTime
*/
public void setCommentTime(String commentTime) {
this.commentTime = commentTime;
}

public Comments withCommentTime(String commentTime) {
this.commentTime = commentTime;
return this;
}

/**
*
* @return
* The commentTimeEdited
*/
public String getCommentTimeEdited() {
return commentTimeEdited;
}

/**
*
* @param commentTimeEdited
* The commentTimeEdited
*/
public void setCommentTimeEdited(String commentTimeEdited) {
this.commentTimeEdited = commentTimeEdited;
}

public Comments withCommentTimeEdited(String commentTimeEdited) {
this.commentTimeEdited = commentTimeEdited;
return this;
}

@Override
public String toString() {
return ToStringBuilder.reflectionToString(this);
}

}