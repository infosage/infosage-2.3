
package com.infosage.item;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;

public class ItemKeystone {

    @Expose
    private long id;
    @Expose
  
    private Owner owner;
    @Expose
  
    private Target target;
    @Expose
    private boolean subscribed;
    @Expose
    private Object groups;

    
    public ItemKeystone(Owner owner,Target ownuser) {
		// TODO Auto-generated constructor stub
    	this.id=0;
    	this.target=ownuser;
    	this.owner=owner;
    	this.subscribed=true;
	}
    
    /**
     * 
     * @return
     *     The id
     */
    public long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(long id) {
        this.id = id;
    }

    public ItemKeystone withId(long id) {
        this.id = id;
        return this;
    }

    /**
     * 
     * @return
     *     The owner
     */
    public Owner getOwner() {
        return owner;
    }

    /**
     * 
     * @param owner
     *     The owner
     */
    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public ItemKeystone withOwner(Owner owner) {
        this.owner = owner;
        return this;
    }

    /**
     * 
     * @return
     *     The target
     */
    public Target getTarget() {
        return target;
    }

    /**
     * 
     * @param target
     *     The target
     */
    public void setTarget(Target target) {
        this.target = target;
    }

    public ItemKeystone withTarget(Target target) {
        this.target = target;
        return this;
    }

    /**
     * 
     * @return
     *     The subscribed
     */
    public boolean isSubscribed() {
        return subscribed;
    }

    /**
     * 
     * @param subscribed
     *     The subscribed
     */
    public void setSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
    }

    public ItemKeystone withSubscribed(boolean subscribed) {
        this.subscribed = subscribed;
        return this;
    }

    /**
     * 
     * @return
     *     The groups
     */
    public Object getGroups() {
        return groups;
    }

    /**
     * 
     * @param groups
     *     The groups
     */
    public void setGroups(Object groups) {
        this.groups = groups;
    }

    public ItemKeystone withGroups(Object groups) {
        this.groups = groups;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(owner).append(target).append(subscribed).append(groups).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ItemKeystone) == false) {
            return false;
        }
        ItemKeystone rhs = ((ItemKeystone) other);
        return new EqualsBuilder().append(id, rhs.id).append(owner, rhs.owner).append(target, rhs.target).append(subscribed, rhs.subscribed).append(groups, rhs.groups).isEquals();
    }

}
