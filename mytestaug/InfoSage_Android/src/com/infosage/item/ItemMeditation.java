
package com.infosage.item;

import com.google.gson.annotations.Expose;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ItemMeditation{

    @Expose
    private long id;
    @Expose
    private User user;
    @Expose
    private String name;
    @Expose
    private String drugImoCode;
    @Expose
    private String dosage;
    @Expose
    private String route;
    @Expose
    private String type;
    @Expose
    private DrugFrequency drugFrequency;
    @Expose
    private DrugSymptom drugSymptom;
    @Expose
    private String otherNotes;
    @Expose
    private boolean isPublic;
    @Expose
    private boolean isActive;
    @Expose
    private long creationDate;
    @Expose
    private long editDate;
    @Expose
    private LoggedInCreator loggedInCreator;
    @Expose
    private LoggedInEditor loggedInEditor;

    /**
     * 
     * @return
     *     The id
     */
    public long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(long id) {
        this.id = id;
    }

    public ItemMeditation withId(long id) {
        this.id = id;
        return this;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    public ItemMeditation withUser(User user) {
        this.user = user;
        return this;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public ItemMeditation withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 
     * @return
     *     The drugImoCode
     */
    public String getDrugImoCode() {
        return drugImoCode;
    }

    /**
     * 
     * @param drugImoCode
     *     The drugImoCode
     */
    public void setDrugImoCode(String drugImoCode) {
        this.drugImoCode = drugImoCode;
    }

    public ItemMeditation withDrugImoCode(String drugImoCode) {
        this.drugImoCode = drugImoCode;
        return this;
    }

    /**
     * 
     * @return
     *     The dosage
     */
    public String getDosage() {
        return dosage;
    }

    /**
     * 
     * @param dosage
     *     The dosage
     */
    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public ItemMeditation withDosage(String dosage) {
        this.dosage = dosage;
        return this;
    }

    /**
     * 
     * @return
     *     The route
     */
    public String getRoute() {
        return route;
    }

    /**
     * 
     * @param route
     *     The route
     */
    public void setRoute(String route) {
        this.route = route;
    }

    public ItemMeditation withRoute(String route) {
        this.route = route;
        return this;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    public ItemMeditation withType(String type) {
        this.type = type;
        return this;
    }

    /**
     * 
     * @return
     *     The drugFrequency
     */
    public DrugFrequency getDrugFrequency() {
        return drugFrequency;
    }

    /**
     * 
     * @param drugFrequency
     *     The drugFrequency
     */
    public void setDrugFrequency(DrugFrequency drugFrequency) {
        this.drugFrequency = drugFrequency;
    }

    public ItemMeditation withDrugFrequency(DrugFrequency drugFrequency) {
        this.drugFrequency = drugFrequency;
        return this;
    }

    /**
     * 
     * @return
     *     The drugSymptom
     */
    public DrugSymptom getDrugSymptom() {
        return drugSymptom;
    }

    /**
     * 
     * @param drugSymptom
     *     The drugSymptom
     */
    public void setDrugSymptom(DrugSymptom drugSymptom) {
        this.drugSymptom = drugSymptom;
    }

    public ItemMeditation withDrugSymptom(DrugSymptom drugSymptom) {
        this.drugSymptom = drugSymptom;
        return this;
    }

    /**
     * 
     * @return
     *     The otherNotes
     */
    public String getOtherNotes() {
        return otherNotes;
    }

    /**
     * 
     * @param otherNotes
     *     The otherNotes
     */
    public void setOtherNotes(String otherNotes) {
        this.otherNotes = otherNotes;
    }

    public ItemMeditation withOtherNotes(String otherNotes) {
        this.otherNotes = otherNotes;
        return this;
    }

    /**
     * 
     * @return
     *     The isPublic
     */
    public boolean isIsPublic() {
        return isPublic;
    }

    /**
     * 
     * @param isPublic
     *     The isPublic
     */
    public void setIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
    }

    public ItemMeditation withIsPublic(boolean isPublic) {
        this.isPublic = isPublic;
        return this;
    }

    /**
     * 
     * @return
     *     The isActive
     */
    public boolean isIsActive() {
        return isActive;
    }

    /**
     * 
     * @param isActive
     *     The isActive
     */
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public ItemMeditation withIsActive(boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    /**
     * 
     * @return
     *     The creationDate
     */
    public long getCreationDate() {
        return creationDate;
    }

    /**
     * 
     * @param creationDate
     *     The creationDate
     */
    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public ItemMeditation withCreationDate(long creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    /**
     * 
     * @return
     *     The editDate
     */
    public long getEditDate() {
        return editDate;
    }

    /**
     * 
     * @param editDate
     *     The editDate
     */
    public void setEditDate(long editDate) {
        this.editDate = editDate;
    }

    public ItemMeditation withEditDate(long editDate) {
        this.editDate = editDate;
        return this;
    }

    /**
     * 
     * @return
     *     The loggedInCreator
     */
    public LoggedInCreator getLoggedInCreator() {
        return loggedInCreator;
    }

    /**
     * 
     * @param loggedInCreator
     *     The loggedInCreator
     */
    public void setLoggedInCreator(LoggedInCreator loggedInCreator) {
        this.loggedInCreator = loggedInCreator;
    }

    public ItemMeditation withLoggedInCreator(LoggedInCreator loggedInCreator) {
        this.loggedInCreator = loggedInCreator;
        return this;
    }

    /**
     * 
     * @return
     *     The loggedInEditor
     */
    public LoggedInEditor getLoggedInEditor() {
        return loggedInEditor;
    }

    /**
     * 
     * @param loggedInEditor
     *     The loggedInEditor
     */
    public void setLoggedInEditor(LoggedInEditor loggedInEditor) {
        this.loggedInEditor = loggedInEditor;
    }

    public ItemMeditation withLoggedInEditor(LoggedInEditor loggedInEditor) {
        this.loggedInEditor = loggedInEditor;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(user).append(name).append(drugImoCode).append(dosage).append(route).append(type).append(drugFrequency).append(drugSymptom).append(otherNotes).append(isPublic).append(isActive).append(creationDate).append(editDate).append(loggedInCreator).append(loggedInEditor).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ItemMeditation) == false) {
            return false;
        }
        ItemMeditation rhs = ((ItemMeditation) other);
        return new EqualsBuilder().append(id, rhs.id).append(user, rhs.user).append(name, rhs.name).append(drugImoCode, rhs.drugImoCode).append(dosage, rhs.dosage).append(route, rhs.route).append(type, rhs.type).append(drugFrequency, rhs.drugFrequency).append(drugSymptom, rhs.drugSymptom).append(otherNotes, rhs.otherNotes).append(isPublic, rhs.isPublic).append(isActive, rhs.isActive).append(creationDate, rhs.creationDate).append(editDate, rhs.editDate).append(loggedInCreator, rhs.loggedInCreator).append(loggedInEditor, rhs.loggedInEditor).isEquals();
    }

}
