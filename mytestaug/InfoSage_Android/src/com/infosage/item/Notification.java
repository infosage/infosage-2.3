
package com.infosage.item;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;


public class Notification {

    @Expose
    private long id;
    @Expose
    private String text;
    @Expose
    private String forwardLink;
    @Expose
    private String name;
    @Expose
    private boolean resolved;
    @Expose
    private long date;
    @Expose
    private Image image;

    /**
     * 
     * @return
     *     The id
     */
    public long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(long id) {
        this.id = id;
    }

    public Notification withId(long id) {
        this.id = id;
        return this;
    }

    /**
     * 
     * @return
     *     The text
     */
    public String getText() {
        return text;
    }

    /**
     * 
     * @param text
     *     The text
     */
    public void setText(String text) {
        this.text = text;
    }

    public Notification withText(String text) {
        this.text = text;
        return this;
    }

    /**
     * 
     * @return
     *     The forwardLink
     */
    public String getForwardLink() {
        return forwardLink;
    }

    /**
     * 
     * @param forwardLink
     *     The forwardLink
     */
    public void setForwardLink(String forwardLink) {
        this.forwardLink = forwardLink;
    }

    public Notification withForwardLink(String forwardLink) {
        this.forwardLink = forwardLink;
        return this;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public Notification withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 
     * @return
     *     The resolved
     */
    public boolean isResolved() {
        return resolved;
    }

    /**
     * 
     * @param resolved
     *     The resolved
     */
    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public Notification withResolved(boolean resolved) {
        this.resolved = resolved;
        return this;
    }

    /**
     * 
     * @return
     *     The date
     */
    public long getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(long date) {
        this.date = date;
    }

    public Notification withDate(long date) {
        this.date = date;
        return this;
    }

    /**
     * 
     * @return
     *     The image
     */
    public Image getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(Image image) {
        this.image = image;
    }

    public Notification withImage(Image image) {
        this.image = image;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(text).append(forwardLink).append(name).append(resolved).append(date).append(image).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Notification) == false) {
            return false;
        }
        Notification rhs = ((Notification) other);
        return new EqualsBuilder().append(id, rhs.id).append(text, rhs.text).append(forwardLink, rhs.forwardLink).append(name, rhs.name).append(resolved, rhs.resolved).append(date, rhs.date).append(image, rhs.image).isEquals();
    }

}
