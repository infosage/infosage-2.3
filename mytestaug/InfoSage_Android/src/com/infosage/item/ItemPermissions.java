
package com.infosage.item;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ItemPermissions {

    @SerializedName("UPLOAD_PHOTOS")
    @Expose
    private boolean UPLOADPHOTOS;
    
    @SerializedName("VIEW_PROFILE")
    @Expose
    private boolean VIEWPROFILE;
    
    @Expose
    private boolean CONNECTED;
    @SerializedName("VIEW_CALENDAR")
    @Expose
    private boolean VIEWCALENDAR;
    @SerializedName("EDIT_PHOTOS")
    @Expose
    private boolean EDITPHOTOS;
    @SerializedName("EDIT_PROFILE")
    @Expose
    private boolean EDITPROFILE;
    @SerializedName("VIEW_TASKS")
    @Expose
    private boolean VIEWTASKS;
    @SerializedName("EDIT_TASKS")
    @Expose
    private boolean EDITTASKS;
    @SerializedName("VIEW_UPDATES")
    @Expose
    private boolean VIEWUPDATES;
    @SerializedName("VIEW_MEDICATIONS")
    @Expose
    private boolean VIEWMEDICATIONS;
    @SerializedName("EDIT_CALENDAR")
    @Expose
    private boolean EDITCALENDAR;
    @Expose
    private boolean PROXY;
    
    @SerializedName("VIEW_NETWORK")
    @Expose
    private boolean VIEWNETWORK;
    @SerializedName("VIEW_PHOTOS")
    @Expose
    private boolean VIEWPHOTOS;
    @SerializedName("WRITE_UPDATES")
    @Expose
    private boolean WRITEUPDATES;

    public String loguserid="";
    
    public String selectid="";
    
    
    public ItemPermissions() {
		// TODO Auto-generated constructor stub
    	this.UPLOADPHOTOS=false;
    	this.VIEWPROFILE=false;
    	this.CONNECTED=false;
    	this.VIEWCALENDAR=false;
    	this.EDITPHOTOS=false;
    	this.EDITPROFILE=false;
    	this.VIEWTASKS=false;
    	this.EDITTASKS=false;
    	this.VIEWUPDATES=false;
    	
    	this.VIEWMEDICATIONS=true;
    	
    	this.EDITCALENDAR=false;
    	this.PROXY=false;
    	this.VIEWNETWORK=false;
    	this.VIEWPHOTOS=false;
    	this.WRITEUPDATES=false;
    	
    }
    
    
    
    /**
     * For Self Means login user
     */
    public void setAllEnable()
    {
    this.UPLOADPHOTOS=true;
	this.VIEWPROFILE=true;
	this.CONNECTED=true;
	this.VIEWCALENDAR=true;
	this.EDITPHOTOS=true;
	this.EDITPROFILE=true;
	this.VIEWTASKS=true;
	this.EDITTASKS=true;
	this.VIEWUPDATES=true;
	
	this.VIEWMEDICATIONS=true;
	
	this.EDITCALENDAR=true;
	this.PROXY=true;
	this.VIEWNETWORK=true;
	this.VIEWPHOTOS=true;
	this.WRITEUPDATES=true;
    }
    
    
    public String getLoguserid() {
		return loguserid;
	}
    
    public void setLoguserid(String loguserid) {
		this.loguserid = loguserid;
	}
    
    public String getSelectid() {
		return selectid;
	}
    
    public void setSelectid(String selectid) {
		this.selectid = selectid;
	}
    
    /**
     * 
     * @return
     *     The UPLOADPHOTOS
     */
    public boolean isUPLOADPHOTOS() {
        return UPLOADPHOTOS;
    }

    /**
     * 
     * @param UPLOADPHOTOS
     *     The UPLOAD_PHOTOS
     */
    public void setUPLOADPHOTOS(boolean UPLOADPHOTOS) {
        this.UPLOADPHOTOS = UPLOADPHOTOS;
    }

    public ItemPermissions withUPLOADPHOTOS(boolean UPLOADPHOTOS) {
        this.UPLOADPHOTOS = UPLOADPHOTOS;
        return this;
    }

    /**
     * 
     * @return
     *     The VIEWPROFILE
     */
    public boolean isVIEWPROFILE() {
        return VIEWPROFILE;
    }

    /**
     * 
     * @param VIEWPROFILE
     *     The VIEW_PROFILE
     */
    public void setVIEWPROFILE(boolean VIEWPROFILE) {
        this.VIEWPROFILE = VIEWPROFILE;
    }

    public ItemPermissions withVIEWPROFILE(boolean VIEWPROFILE) {
        this.VIEWPROFILE = VIEWPROFILE;
        return this;
    }

    /**
     * 
     * @return
     *     The CONNECTED
     */
    public boolean isCONNECTED() {
        return CONNECTED;
    }

    /**
     * 
     * @param CONNECTED
     *     The CONNECTED
     */
    public void setCONNECTED(boolean CONNECTED) {
        this.CONNECTED = CONNECTED;
    }

    public ItemPermissions withCONNECTED(boolean CONNECTED) {
        this.CONNECTED = CONNECTED;
        return this;
    }

    /**
     * 
     * @return
     *     The VIEWCALENDAR
     */
    public boolean isVIEWCALENDAR() {
        return VIEWCALENDAR;
    }

    /**
     * 
     * @param VIEWCALENDAR
     *     The VIEW_CALENDAR
     */
    public void setVIEWCALENDAR(boolean VIEWCALENDAR) {
        this.VIEWCALENDAR = VIEWCALENDAR;
    }

    public ItemPermissions withVIEWCALENDAR(boolean VIEWCALENDAR) {
        this.VIEWCALENDAR = VIEWCALENDAR;
        return this;
    }

    /**
     * 
     * @return
     *     The EDITPHOTOS
     */
    public boolean isEDITPHOTOS() {
        return EDITPHOTOS;
    }

    /**
     * 
     * @param EDITPHOTOS
     *     The EDIT_PHOTOS
     */
    public void setEDITPHOTOS(boolean EDITPHOTOS) {
        this.EDITPHOTOS = EDITPHOTOS;
    }

    public ItemPermissions withEDITPHOTOS(boolean EDITPHOTOS) {
        this.EDITPHOTOS = EDITPHOTOS;
        return this;
    }

    /**
     * 
     * @return
     *     The EDITPROFILE
     */
    public boolean isEDITPROFILE() {
        return EDITPROFILE;
    }

    /**
     * 
     * @param EDITPROFILE
     *     The EDIT_PROFILE
     */
    public void setEDITPROFILE(boolean EDITPROFILE) {
        this.EDITPROFILE = EDITPROFILE;
    }

    public ItemPermissions withEDITPROFILE(boolean EDITPROFILE) {
        this.EDITPROFILE = EDITPROFILE;
        return this;
    }

    /**
     * 
     * @return
     *     The VIEWTASKS
     */
    public boolean isVIEWTASKS() {
        return VIEWTASKS;
    }

    /**
     * 
     * @param VIEWTASKS
     *     The VIEW_TASKS
     */
    public void setVIEWTASKS(boolean VIEWTASKS) {
        this.VIEWTASKS = VIEWTASKS;
    }

    public ItemPermissions withVIEWTASKS(boolean VIEWTASKS) {
        this.VIEWTASKS = VIEWTASKS;
        return this;
    }

    /**
     * 
     * @return
     *     The EDITTASKS
     */
    public boolean isEDITTASKS() {
        return EDITTASKS;
    }

    /**
     * 
     * @param EDITTASKS
     *     The EDIT_TASKS
     */
    public void setEDITTASKS(boolean EDITTASKS) {
        this.EDITTASKS = EDITTASKS;
    }

    public ItemPermissions withEDITTASKS(boolean EDITTASKS) {
        this.EDITTASKS = EDITTASKS;
        return this;
    }

    /**
     * 
     * @return
     *     The VIEWUPDATES
     */
    public boolean isVIEWUPDATES() {
        return VIEWUPDATES;
    }

    /**
     * 
     * @param VIEWUPDATES
     *     The VIEW_UPDATES
     */
    public void setVIEWUPDATES(boolean VIEWUPDATES) {
        this.VIEWUPDATES = VIEWUPDATES;
    }

    public ItemPermissions withVIEWUPDATES(boolean VIEWUPDATES) {
        this.VIEWUPDATES = VIEWUPDATES;
        return this;
    }

    /**
     * 
     * @return
     *     The VIEWMEDICATIONS
     */
    public boolean isVIEWMEDICATIONS() {
        return VIEWMEDICATIONS;
    }

    /**
     * 
     * @param VIEWMEDICATIONS
     *     The VIEW_MEDICATIONS
     */
    public void setVIEWMEDICATIONS(boolean VIEWMEDICATIONS) {
        this.VIEWMEDICATIONS = VIEWMEDICATIONS;
    }

    public ItemPermissions withVIEWMEDICATIONS(boolean VIEWMEDICATIONS) {
        this.VIEWMEDICATIONS = VIEWMEDICATIONS;
        return this;
    }

    /**
     * 
     * @return
     *     The EDITCALENDAR
     */
    public boolean isEDITCALENDAR() {
        return EDITCALENDAR;
    }

    /**
     * 
     * @param EDITCALENDAR
     *     The EDIT_CALENDAR
     */
    public void setEDITCALENDAR(boolean EDITCALENDAR) {
        this.EDITCALENDAR = EDITCALENDAR;
    }

    public ItemPermissions withEDITCALENDAR(boolean EDITCALENDAR) {
        this.EDITCALENDAR = EDITCALENDAR;
        return this;
    }

    /**
     * 
     * @return
     *     The PROXY
     */
    public boolean isPROXY() {
        return PROXY;
    }

  /**
   * 
   * @param PROXY
   */
    public void setPROXY(boolean PROXY) {
        this.PROXY = PROXY;
    }

    public ItemPermissions withPROXY(boolean PROXY) {
        this.PROXY = PROXY;
        return this;
    }

    /**
     * 
     * @return
     *     The VIEWNETWORK
     */
    public boolean isVIEWNETWORK() {
        return VIEWNETWORK;
    }

    /**
     * 
     * @param VIEWNETWORK
     *     The VIEW_NETWORK
     */
    public void setVIEWNETWORK(boolean VIEWNETWORK) {
        this.VIEWNETWORK = VIEWNETWORK;
    }

    public ItemPermissions withVIEWNETWORK(boolean VIEWNETWORK) {
        this.VIEWNETWORK = VIEWNETWORK;
        return this;
    }

    /**
     * 
     * @return
     *     The VIEWPHOTOS
     */
    public boolean isVIEWPHOTOS() {
        return VIEWPHOTOS;
    }

    /**
     * 
     * @param VIEWPHOTOS
     *     The VIEW_PHOTOS
     */
    public void setVIEWPHOTOS(boolean VIEWPHOTOS) {
        this.VIEWPHOTOS = VIEWPHOTOS;
    }

    public ItemPermissions withVIEWPHOTOS(boolean VIEWPHOTOS) {
        this.VIEWPHOTOS = VIEWPHOTOS;
        return this;
    }

    /**
     * 
     * @return
     *     The WRITEUPDATES
     */
    public boolean isWRITEUPDATES() {
        return WRITEUPDATES;
    }
    
    /**
     * 
     * @param WRITEUPDATES
     *     The WRITE_UPDATES
     */
    public void setWRITEUPDATES(boolean WRITEUPDATES) {
        this.WRITEUPDATES = WRITEUPDATES;
    }

    public ItemPermissions withWRITEUPDATES(boolean WRITEUPDATES) {
        this.WRITEUPDATES = WRITEUPDATES;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(UPLOADPHOTOS).append(VIEWPROFILE).append(CONNECTED).append(VIEWCALENDAR).append(EDITPHOTOS).append(EDITPROFILE).append(VIEWTASKS).append(EDITTASKS).append(VIEWUPDATES).append(VIEWMEDICATIONS).append(EDITCALENDAR).append(PROXY).append(VIEWNETWORK).append(VIEWPHOTOS).append(WRITEUPDATES).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ItemPermissions) == false) {
            return false;
        }
        ItemPermissions rhs = ((ItemPermissions) other);
        return new EqualsBuilder().append(UPLOADPHOTOS, rhs.UPLOADPHOTOS).append(VIEWPROFILE, rhs.VIEWPROFILE).append(CONNECTED, rhs.CONNECTED).append(VIEWCALENDAR, rhs.VIEWCALENDAR).append(EDITPHOTOS, rhs.EDITPHOTOS).append(EDITPROFILE, rhs.EDITPROFILE).append(VIEWTASKS, rhs.VIEWTASKS).append(EDITTASKS, rhs.EDITTASKS).append(VIEWUPDATES, rhs.VIEWUPDATES).append(VIEWMEDICATIONS, rhs.VIEWMEDICATIONS).append(EDITCALENDAR, rhs.EDITCALENDAR).append(PROXY, rhs.PROXY).append(VIEWNETWORK, rhs.VIEWNETWORK).append(VIEWPHOTOS, rhs.VIEWPHOTOS).append(WRITEUPDATES, rhs.WRITEUPDATES).isEquals();
    }

}
