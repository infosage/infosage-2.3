
package com.infosage.item;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;


public class Form {

    @Expose
    private String dosage;
    @Expose
    private String type;
    @Expose
    private String route;
    
    private String unknown;

    
    public Form(String dosage,String type,String route) {
		// TODO Auto-generated constructor stub
    	this.dosage=dosage;
    	this.type=type;
    	this.route=route;
    	this.unknown="Unknown";
	}
    
    
    
    /**
     * 
     * @return
     *     The unknown
     */
    public String getUnknown() {
        return unknown;
    }

    /**
     * 
     * @param unknown
     *     The unknown
     */
    public void setUnknown(String unknown) {
        this.unknown = unknown;
    }
    
    
    
    
    
    /**
     * 
     * @return
     *     The dosage
     */
    public String getDosage() {
        return dosage;
    }

    /**
     * 
     * @param dosage
     *     The dosage
     */
    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public Form withDosage(String dosage) {
        this.dosage = dosage;
        return this;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    public Form withType(String type) {
        this.type = type;
        return this;
    }

    /**
     * 
     * @return
     *     The route
     */
    public String getRoute() {
        return route;
    }

    /**
     * 
     * @param route
     *     The route
     */
    public void setRoute(String route) {
        this.route = route;
    }

    public Form withRoute(String route) {
        this.route = route;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dosage).append(type).append(route).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Form) == false) {
            return false;
        }
        Form rhs = ((Form) other);
        return new EqualsBuilder().append(dosage, rhs.dosage).append(type, rhs.type).append(route, rhs.route).isEquals();
    }

}
