
package com.infosage.item;

import com.google.gson.annotations.Expose;
import org.apache.commons.lang.builder.ToStringBuilder;


public class WallUser {

    @Expose
    private int id;
    @Expose
    private String firstName;
    @Expose
    private String middleName;
    @Expose
    private String lastName;
    @Expose
    private String title;
    @Expose
    private String emailAddress;
    @Expose
    private ProfileImage profileImage;
    @Expose
    private boolean enabled;
    @Expose
    private boolean isElder;
    @Expose
    private boolean isTest;
    @Expose
    private boolean isAdministrator;
    @Expose
    private boolean sendEmails;
    @Expose
    private String creationDate;

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * 
     * @param firstName
     *     The firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * 
     * @return
     *     The middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * 
     * @param middleName
     *     The middleName
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * 
     * @return
     *     The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * 
     * @param lastName
     *     The lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * 
     * @param emailAddress
     *     The emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * 
     * @return
     *     The profileImage
     */
    public ProfileImage getProfileImage() {
        return profileImage;
    }

    /**
     * 
     * @param profileImage
     *     The profileImage
     */
    public void setProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * 
     * @return
     *     The enabled
     */
    public boolean getEnabled() {
        return enabled;
    }

    /**
     * 
     * @param enabled
     *     The enabled
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * 
     * @return
     *     The isElder
     */
    public boolean getIsElder() {
        return isElder;
    }

    /**
     * 
     * @param isElder
     *     The isElder
     */
    public void setIsElder(boolean isElder) {
        this.isElder = isElder;
    }

    /**
     * 
     * @return
     *     The isTest
     */
    public boolean getIsTest() {
        return isTest;
    }

    /**
     * 
     * @param isTest
     *     The isTest
     */
    public void setIsTest(boolean isTest) {
        this.isTest = isTest;
    }

    /**
     * 
     * @return
     *     The isAdministrator
     */
    public boolean getIsAdministrator() {
        return isAdministrator;
    }

    /**
     * 
     * @param isAdministrator
     *     The isAdministrator
     */
    public void setIsAdministrator(boolean isAdministrator) {
        this.isAdministrator = isAdministrator;
    }

    /**
     * 
     * @return
     *     The sendEmails
     */
    public boolean getSendEmails() {
        return sendEmails;
    }

    /**
     * 
     * @param sendEmails
     *     The sendEmails
     */
    public void setSendEmails(boolean sendEmails) {
        this.sendEmails = sendEmails;
    }

    /**
     * 
     * @return
     *     The creationDate
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * 
     * @param creationDate
     *     The creationDate
     */
    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
