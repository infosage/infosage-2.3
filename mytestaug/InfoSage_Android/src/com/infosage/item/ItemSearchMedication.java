
package com.infosage.item;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;


public class ItemSearchMedication {

    @Expose
    private long code;
    @Expose
    private String title;

    
    public ItemSearchMedication(long code,String title) {
		// TODO Auto-generated constructor stub
    	this.code=code;
    	this.title=title;
    }
    
    
    /**
     * 
     * @return
     *     The code
     */
    public long getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(long code) {
        this.code = code;
    }

    public ItemSearchMedication withCode(long code) {
        this.code = code;
        return this;
    }

    /**
     * 
     * @return
     *     The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public ItemSearchMedication withTitle(String title) {
        this.title = title;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(code).append(title).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ItemSearchMedication) == false) {
            return false;
        }
        ItemSearchMedication rhs = ((ItemSearchMedication) other);
        return new EqualsBuilder().append(code, rhs.code).append(title, rhs.title).isEquals();
    }

}
