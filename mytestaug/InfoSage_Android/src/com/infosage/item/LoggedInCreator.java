
package com.infosage.item;

import com.google.gson.annotations.Expose;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class LoggedInCreator {

    @Expose
    private long id;
    @Expose
    private String firstName;
    @Expose
    private Object middleName;
    @Expose
    private String lastName;
    @Expose
    private Object title;
    @Expose
    private String emailAddress;
    @Expose
    private ProfileImage profileImage;
    @Expose
    private boolean enabled;
    @Expose
    private boolean isElder;
    @Expose
    private boolean isTest;
    @Expose
    private boolean isAdministrator;
    @Expose
    private boolean sendEmails;
    @Expose
    private Object creationDate;

    /**
     * 
     * @return
     *     The id
     */
    public long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(long id) {
        this.id = id;
    }

    public LoggedInCreator withId(long id) {
        this.id = id;
        return this;
    }

    /**
     * 
     * @return
     *     The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * 
     * @param firstName
     *     The firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LoggedInCreator withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * 
     * @return
     *     The middleName
     */
    public Object getMiddleName() {
        return middleName;
    }

    /**
     * 
     * @param middleName
     *     The middleName
     */
    public void setMiddleName(Object middleName) {
        this.middleName = middleName;
    }

    public LoggedInCreator withMiddleName(Object middleName) {
        this.middleName = middleName;
        return this;
    }

    /**
     * 
     * @return
     *     The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * 
     * @param lastName
     *     The lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LoggedInCreator withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * 
     * @return
     *     The title
     */
    public Object getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    public void setTitle(Object title) {
        this.title = title;
    }

    public LoggedInCreator withTitle(Object title) {
        this.title = title;
        return this;
    }

    /**
     * 
     * @return
     *     The emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * 
     * @param emailAddress
     *     The emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public LoggedInCreator withEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    /**
     * 
     * @return
     *     The profileImage
     */
    public ProfileImage getProfileImage() {
        return profileImage;
    }

    /**
     * 
     * @param profileImage
     *     The profileImage
     */
    public void setProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
    }

    public LoggedInCreator withProfileImage(ProfileImage profileImage) {
        this.profileImage = profileImage;
        return this;
    }

    /**
     * 
     * @return
     *     The enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * 
     * @param enabled
     *     The enabled
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public LoggedInCreator withEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    /**
     * 
     * @return
     *     The isElder
     */
    public boolean isIsElder() {
        return isElder;
    }

    /**
     * 
     * @param isElder
     *     The isElder
     */
    public void setIsElder(boolean isElder) {
        this.isElder = isElder;
    }

    public LoggedInCreator withIsElder(boolean isElder) {
        this.isElder = isElder;
        return this;
    }

    /**
     * 
     * @return
     *     The isTest
     */
    public boolean isIsTest() {
        return isTest;
    }

    /**
     * 
     * @param isTest
     *     The isTest
     */
    public void setIsTest(boolean isTest) {
        this.isTest = isTest;
    }

    public LoggedInCreator withIsTest(boolean isTest) {
        this.isTest = isTest;
        return this;
    }

    /**
     * 
     * @return
     *     The isAdministrator
     */
    public boolean isIsAdministrator() {
        return isAdministrator;
    }

    /**
     * 
     * @param isAdministrator
     *     The isAdministrator
     */
    public void setIsAdministrator(boolean isAdministrator) {
        this.isAdministrator = isAdministrator;
    }

    public LoggedInCreator withIsAdministrator(boolean isAdministrator) {
        this.isAdministrator = isAdministrator;
        return this;
    }

    /**
     * 
     * @return
     *     The sendEmails
     */
    public boolean isSendEmails() {
        return sendEmails;
    }

    /**
     * 
     * @param sendEmails
     *     The sendEmails
     */
    public void setSendEmails(boolean sendEmails) {
        this.sendEmails = sendEmails;
    }

    public LoggedInCreator withSendEmails(boolean sendEmails) {
        this.sendEmails = sendEmails;
        return this;
    }

    /**
     * 
     * @return
     *     The creationDate
     */
    public Object getCreationDate() {
        return creationDate;
    }

    /**
     * 
     * @param creationDate
     *     The creationDate
     */
    public void setCreationDate(Object creationDate) {
        this.creationDate = creationDate;
    }

    public LoggedInCreator withCreationDate(Object creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(firstName).append(middleName).append(lastName).append(title).append(emailAddress).append(profileImage).append(enabled).append(isElder).append(isTest).append(isAdministrator).append(sendEmails).append(creationDate).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof LoggedInCreator) == false) {
            return false;
        }
        LoggedInCreator rhs = ((LoggedInCreator) other);
        return new EqualsBuilder().append(id, rhs.id).append(firstName, rhs.firstName).append(middleName, rhs.middleName).append(lastName, rhs.lastName).append(title, rhs.title).append(emailAddress, rhs.emailAddress).append(profileImage, rhs.profileImage).append(enabled, rhs.enabled).append(isElder, rhs.isElder).append(isTest, rhs.isTest).append(isAdministrator, rhs.isAdministrator).append(sendEmails, rhs.sendEmails).append(creationDate, rhs.creationDate).isEquals();
    }

}
