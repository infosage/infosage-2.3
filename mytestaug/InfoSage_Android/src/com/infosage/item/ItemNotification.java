
package com.infosage.item;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;

public class ItemNotification {

    @Expose
    private List<Notification> notifications = new ArrayList<Notification>();
    @Expose
    private long numUnresolved;

    /**
     * 
     * @return
     *     The notifications
     */
    public List<Notification> getNotifications() {
        return notifications;
    }

    /**
     * 
     * @param notifications
     *     The notifications
     */
    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public ItemNotification withNotifications(List<Notification> notifications) {
        this.notifications = notifications;
        return this;
    }

    /**
     * 
     * @return
     *     The numUnresolved
     */
    public long getNumUnresolved() {
        return numUnresolved;
    }

    /**
     * 
     * @param numUnresolved
     *     The numUnresolved
     */
    public void setNumUnresolved(long numUnresolved) {
        this.numUnresolved = numUnresolved;
    }

    public ItemNotification withNumUnresolved(long numUnresolved) {
        this.numUnresolved = numUnresolved;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(notifications).append(numUnresolved).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ItemNotification) == false) {
            return false;
        }
        ItemNotification rhs = ((ItemNotification) other);
        return new EqualsBuilder().append(notifications, rhs.notifications).append(numUnresolved, rhs.numUnresolved).isEquals();
    }

}
