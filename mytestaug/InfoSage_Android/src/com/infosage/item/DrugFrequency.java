
package com.infosage.item;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;

public class DrugFrequency {

    @Expose
    private long id;
    @Expose
    private String name;
    @Expose
    private boolean allowsSymptom;

    /**
     * 
     * @return
     *     The id
     */
    public long getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(long id) {
        this.id = id;
    }

    public DrugFrequency withId(long id) {
        this.id = id;
        return this;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public DrugFrequency withName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 
     * @return
     *     The allowsSymptom
     */
    public boolean isAllowsSymptom() {
        return allowsSymptom;
    }

    /**
     * 
     * @param allowsSymptom
     *     The allowsSymptom
     */
    public void setAllowsSymptom(boolean allowsSymptom) {
        this.allowsSymptom = allowsSymptom;
    }

    public DrugFrequency withAllowsSymptom(boolean allowsSymptom) {
        this.allowsSymptom = allowsSymptom;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(name).append(allowsSymptom).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DrugFrequency) == false) {
            return false;
        }
        DrugFrequency rhs = ((DrugFrequency) other);
        return new EqualsBuilder().append(id, rhs.id).append(name, rhs.name).append(allowsSymptom, rhs.allowsSymptom).isEquals();
    }

}
