
package com.infosage.item;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemSocialLogin {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("incompleteConsentProcessRecordWithInvitationRecords")
    @Expose
    private Object incompleteConsentProcessRecordWithInvitationRecords;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ItemSocialLogin() {
    }

    /**
     * 
     * @param incompleteConsentProcessRecordWithInvitationRecords
     * @param user
     */
    public ItemSocialLogin(User user, Object incompleteConsentProcessRecordWithInvitationRecords) {
        this.user = user;
        this.incompleteConsentProcessRecordWithInvitationRecords = incompleteConsentProcessRecordWithInvitationRecords;
    }

    /**
     * 
     * @return
     *     The user
     */
    public User getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    public ItemSocialLogin withUser(User user) {
        this.user = user;
        return this;
    }

    /**
     * 
     * @return
     *     The incompleteConsentProcessRecordWithInvitationRecords
     */
    public Object getIncompleteConsentProcessRecordWithInvitationRecords() {
        return incompleteConsentProcessRecordWithInvitationRecords;
    }

    /**
     * 
     * @param incompleteConsentProcessRecordWithInvitationRecords
     *     The incompleteConsentProcessRecordWithInvitationRecords
     */
    public void setIncompleteConsentProcessRecordWithInvitationRecords(Object incompleteConsentProcessRecordWithInvitationRecords) {
        this.incompleteConsentProcessRecordWithInvitationRecords = incompleteConsentProcessRecordWithInvitationRecords;
    }

    public ItemSocialLogin withIncompleteConsentProcessRecordWithInvitationRecords(Object incompleteConsentProcessRecordWithInvitationRecords) {
        this.incompleteConsentProcessRecordWithInvitationRecords = incompleteConsentProcessRecordWithInvitationRecords;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(user).append(incompleteConsentProcessRecordWithInvitationRecords).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ItemSocialLogin) == false) {
            return false;
        }
        ItemSocialLogin rhs = ((ItemSocialLogin) other);
        return new EqualsBuilder().append(user, rhs.user).append(incompleteConsentProcessRecordWithInvitationRecords, rhs.incompleteConsentProcessRecordWithInvitationRecords).isEquals();
    }

}
