package org.infosagehealth.app;

import org.infosagehealth.app.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * 
 * @author Govinda P.
 *
 */
public class Launch extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.launch);
		
		// Add code to print out the key hash
	   	    
		new Thread(){
			public void run()
			{
				try {
					Thread.sleep(3000);					
					Intent intent=new Intent(getApplicationContext(),MainActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					finish();
				} catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();
	}
}
