package org.infosagehealth.app;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * 
 *@author Govinda P.
 *@use ToDo list task object model class. Convert json string to Class object.
 */
public class TaskObject implements Parcelable{
	
	String taskid,taskname,taskstartdt,taskenddt,taskdur,taskcomplete,creationdt,otherNotes;
	JSONObject assignedUser;
	private static final long serialVersionUID = 1L;
	
	public TaskObject(String taskid1,String taskname1,String taskstartdt1,String taskenddt1,String taskdur1,String taskcomplete1,JSONObject assignedUser1,String createdt,String otherNotes1)
	{
		taskid = taskid1;
		taskname = taskname1;
		taskstartdt = taskstartdt1; 
		taskenddt = taskenddt1;
		taskdur = taskdur1;
		taskcomplete = taskcomplete1;
		assignedUser = assignedUser1;
		if(createdt == null || createdt.equals("null"))
		{	creationdt = "null";}
		else
		{ 	creationdt = createdt;}
		otherNotes = otherNotes1;
	}
	
	public TaskObject(Parcel in)
	{
		
	     this.taskid = in.readString();
	     this.taskname = in.readString();
	     this.taskstartdt = in.readString();
	     this.taskenddt = in.readString();
	     this.taskdur = in.readString();
	     this.taskcomplete = in.readString();
	     this.creationdt = in.readString();
	     try {
			this.assignedUser = new JSONObject(in.readString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
	}

	public String getTaskId()
	{
		return taskid;
	}
	public String getotherNotes()
	{
		return otherNotes;
	}
	public String getTaskName()
	{
		return taskname;
	}
	public String getTaskStartdt()
	{
		return taskstartdt;
	}
	public String getTaskEnddt()
	{
		return taskenddt;
	}
	public String getTaskDur()
	{
		return taskdur;
	}
	public String getTaskComplete()
	{
		return taskcomplete;
	}
	public JSONObject getAssignedUser()
	{
		return assignedUser;
	}
	public String getCreatedt()
	{
		String dt = "null";
		
		if(creationdt.trim().compareTo("null") != 0)
		{
			
		SimpleDateFormat format = new SimpleDateFormat("MM/dd");
		
		Calendar cal = Calendar.getInstance();
		
		cal.setTimeInMillis(Long.parseLong(creationdt));
		
		
		dt = format.format(cal.getTime());
		}
		return dt;
	}
	
	@Override
	public String toString()
	{
		return "TaskObject[id="+taskid+",name="+taskname+",startdt="+taskstartdt+",enddt="+taskenddt+"]";
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		//dest.writeStringArray(new String[] { this.taskid, this.taskname, this.taskstartdt, this.taskenddt, this.taskdur, this.taskcomplete });
		dest.writeString(this.taskid);
		dest.writeString(this.taskname);
		dest.writeString(this.taskstartdt);
		dest.writeString(this.taskenddt);
		dest.writeString(this.taskdur);
		dest.writeString(this.taskcomplete);
		dest.writeString(this.creationdt);
		dest.writeString(this.assignedUser.toString());
	}
	
	 public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
	        public TaskObject createFromParcel(Parcel in) {
	            return new TaskObject(in);
	        }

	        public TaskObject[] newArray(int size) {
	        	
	        	
	            return new TaskObject[size];
	        }
	    };
}
