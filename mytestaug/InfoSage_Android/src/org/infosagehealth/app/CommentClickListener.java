package org.infosagehealth.app;

import android.widget.RelativeLayout;

public interface CommentClickListener {
	public void OnCommentClickListener(RelativeLayout v, int pos,int type);
	public void OnCommentEditListener(RelativeLayout v, int pos,String comm,boolean isEdit,int type);
	
}
