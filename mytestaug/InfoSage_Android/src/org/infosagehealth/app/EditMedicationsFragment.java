package org.infosagehealth.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.infosagehealth.app.R;
import org.json.JSONArray;
import org.json.JSONObject;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.infosage.adapter.AdapterMedAdapter;
import com.infosage.adapter.OnMedicationEditLisner;
import com.infosage.item.ItemMeditation;
import com.infosage.util.Constants;
import com.infosage.util.MyFunction;
import com.infosage.view.CustomAnimatedProgress;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

/**
 * 
 * @author Govinda P.
 *
 */
public class EditMedicationsFragment extends Fragment{

	ListView medlist;
	AdapterMedAdapter medadapter;
	RelativeLayout relcomm;
	LinearLayout lay2;
	Button btnadd;
	EditText txtupdate;
	float dist;
	addMedicationCallback callback  = null;
	public ArrayList <ItemMeditation> listMedi=new ArrayList<ItemMeditation>();
	CustomAnimatedProgress progress=null;
	AlertDialog.Builder warrningBox=null;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		View view = inflater.inflate(R.layout.editmedications, container, false);
		progress=new CustomAnimatedProgress(EditMedicationsFragment.this.getContext());
		medlist = (ListView)view.findViewById(R.id.medlist);
		btnadd = (Button)view.findViewById(R.id.btnadd);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onStop()
	 */
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if (progress!=null) {
			progress.dismiss();
		}
		AppController.getInstance().cancelPendingRequests("editlist");
	}
	
	@Override
	public void onAttach(Activity activity){
		
		super.onAttach(activity);
		
		if(activity instanceof addMedicationCallback)
		{
			callback = (addMedicationCallback)activity;
		}
	}
	
	@Override
	public void onDetach(){
		
		super.onDetach();
	}
	
	interface addMedicationCallback
	{
		public void showaddmedication();
		public void showEditmedication(ItemMeditation itemMeditation);
		
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (!AppController.permissions.isVIEWMEDICATIONS()) {
			warrningBox();
			return;
		}
		else if(UserTasks.userTarget!=null){
			getResponse(Constants.getURL_MedicationList(String.valueOf(AppController.getViewUser().getId())),
					AppController.session.getLogUserId());
		}		
		
		//LayoutInflater inflater = LayoutInflater.from(getActivity().getApplicationContext());
	//	View headerview = inflater.inflate(R.layout.editmedchildheader, null);
		
		//medlist.addHeaderView(headerview);
		medlist.setAdapter(medadapter);
		
		btnadd.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!AppController.permissions.isVIEWMEDICATIONS()) {
					warrningBox();
					return;
				}else{
				callback.showaddmedication();
				}
			}
		});
	}
	
	/**
	 * Web services call
	 */
	public void getResponse(String url,String userid)
	{
		///final String uid = userid;
		
		progress.show();  
		
		
		JsonArrayRequest req = new JsonArrayRequest(url, new Listener<JSONArray>() {

			@Override
			public void onResponse(JSONArray arg0) {			
			
			try {
				listMedi.clear();
				List<ItemMeditation> temp = Arrays.asList(AppController.gson.fromJson(arg0.toString(),ItemMeditation[].class));
				listMedi.addAll(temp);
			
				
				adapterInit();
				if (progress!=null) {
				progress.dismiss();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				if (progress!=null) {
					progress.dismiss();
				}
				
			}	
			
			}
			
		},err) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	   if (progress!=null && progress.isShowing()) {
						progress.dismiss();
		        	   }
		        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
		        		   VolleyError error = null;
		                   try {
		                	   error= new VolleyError(new String(volleyError.networkResponse.data));
		                	  
		                	   JSONObject jsonObject = new JSONObject(error.getMessage());
		                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
		                   		{
		                		   Message message = mHandler_warnning.obtainMessage();
		                           message.sendToTarget();
		                   		}
		                   }catch(Exception e){
		                	   e.printStackTrace();
		                   }
		        	   }
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            
	            	return super.getParamsEncoding();
	            }
	            
	        };
		
	      AppController.getInstance().addToRequestQueue(req, "editlist");

	}
	 Response.ErrorListener err = new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError volleyError) {
				// TODO Auto-generated method stub
				MyFunction.CheckError(getActivity(), volleyError);
				// TODO Auto-generated method stub
				try {
					progress.dismiss();
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
		};
	
	/**
	 * 
	 * @param url delete url
	 * @param pos Post position
	 * @param flag for Drug Post array or comment array for delete
	 */
	private void deletereq(String url,final int pos,final boolean flag)
	{
		// Tag used to cancel the request
		String tag_json_obj = "json_obj_req";
		if (!AppController.permissions.isVIEWMEDICATIONS()) {
			warrningBox();
			return;
		}
		progress.show();     		    
		
	   
		//JSONObject jsonBody = MyFunction.getJsonBody(keys, values);
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.DELETE,
                url, new Response.Listener<JSONObject>() {
 
                    @Override
                    public void onResponse(JSONObject response) {
                    
                    	if (flag) {
                    		   ItemMeditation item = AppController.gson.fromJson(response.toString(),ItemMeditation.class);                    	
                             // listupdate.set(pos, item);
                    		  try {
                    			  listMedi.remove(pos);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
                              medadapter.notifyDataSetChanged();
						}
                    	else if (!flag) {
							ItemMeditation item = AppController.gson.fromJson(response.toString(),ItemMeditation.class);                    	
							//Update Main post list	
							try {
								listMedi.remove(pos);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							
							medadapter.notifyDataSetChanged();
                            
						}
                 
                    	progress.dismiss();
                    }
                },err){
		           @Override
		        protected VolleyError parseNetworkError(
		        		VolleyError volleyError) {
		        	// TODO Auto-generated method stub
		        	   if (progress!=null && progress.isShowing()) {
							progress.dismiss();
			        	   }
			        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
			        		   VolleyError error = null;
			                   try {
			                	   error= new VolleyError(new String(volleyError.networkResponse.data));
			                	  
			                	   JSONObject jsonObject = new JSONObject(error.getMessage());
			                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
			                   		{
			                		   Message message = mHandler_warnning.obtainMessage();
			                           message.sendToTarget();
			                   		}
			                   }catch(Exception e){
			                	   e.printStackTrace();
			                   }
			        	   }
		        	return  MyFunction.parseNetworkErrorMy(volleyError);
		        }
		           @Override
		        	protected Response<JSONObject> parseNetworkResponse(
		        			NetworkResponse response) {
		        		// TODO Auto-generated method stub
		        	   try {
		        		   progress.dismiss();
					} catch (Exception e) {
						// TODO: handle exception
					}
		        	 
		        		return super.parseNetworkResponse(response);
		        	}
		            /**
		             * Passing some request headers
		             * */
		           @Override
			        public Map<String, String> getHeaders() throws AuthFailureError {
			        	// TODO Auto-generated method stub
			        	return AppController.createBasicAuthHeader();
			        }
		           
		        };
		 
		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
	}
	
	/**
	 * 
	 * @param url Share / Active url
	 * @param pos Medication drug position position
	 * @param flag for extra filter
	 */
	private void shareReq(String url,final int pos,final boolean flag)
	{
		// Tag used to cancel the request
		String tag_json_obj = "json_obj_req";
		if (!AppController.permissions.isVIEWMEDICATIONS()) {
			warrningBox();
			return;
		}
		progress.show();
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.PUT,
                url,
                 new Response.Listener<JSONObject>() {
 
                    @Override
                    public void onResponse(JSONObject response) {
                    	
                    	if (flag) {
                    		   ItemMeditation item = AppController.gson.fromJson(response.toString(),ItemMeditation.class);                    	
                             // listupdate.set(pos, item);
                    		  try {
                    			  listMedi.set(pos,item);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
                              medadapter.notifyDataSetChanged();
						}
                    	else if (!flag) {
							ItemMeditation item = AppController.gson.fromJson(response.toString(),ItemMeditation.class);                    	
							//Update Main post list	
							try {
								listMedi.set(pos,item);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							
							medadapter.notifyDataSetChanged();
                            
						}
                    	progress.dismiss();
                    }
                },err){
		           @Override
		        protected VolleyError parseNetworkError(
		        		VolleyError volleyError) {
		        	// TODO Auto-generated method stub
		        	   if (progress!=null && progress.isShowing()) {
							progress.dismiss();
			        	   }
			        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
			        		   VolleyError error = null;
			                   try {
			                	   error= new VolleyError(new String(volleyError.networkResponse.data));
			                	  
			                	   JSONObject jsonObject = new JSONObject(error.getMessage());
			                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
			                   		{
			                		   Message message = mHandler_warnning.obtainMessage();
			                           message.sendToTarget();
			                   		}
			                   }catch(Exception e){
			                	   e.printStackTrace();
			                   }
			        	   }
		        	   
		        	return  MyFunction.parseNetworkErrorMy(volleyError);
		        }
		           @Override
		        	protected Response<JSONObject> parseNetworkResponse(
		        			NetworkResponse response) {
		        		// TODO Auto-generated method stub
		        	   try {
		        		   progress.dismiss();
					} catch (Exception e) {
						// TODO: handle exception
					}
		        	
		        		return super.parseNetworkResponse(response);
		        	}
		            /**
		             * Passing some request headers
		             * */
		           @Override
			        public Map<String, String> getHeaders() throws AuthFailureError {
			        	// TODO Auto-generated method stub
			        	return AppController.createBasicAuthHeader();
			        }
		           
		        };
		 
		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
	}
	
	public void adapterInit()
	{
	medadapter = new AdapterMedAdapter(getActivity(), getActivity(), new OnMedicationEditLisner() {
		
		@Override
		public void editDrug(int pos, String flag) {
			// TODO Auto-generated method stub
			callback.showEditmedication(listMedi.get(pos));
		}
		
		@Override
		public void deleteDrug(final int pos, final boolean flag) {
			// TODO Auto-generated method stub
			
			new AlertDialog.Builder(getActivity())
        	.setTitle("Confirm to delete")
        	
        	.setMessage("Are you sure that you want to delete this medication record?")
        //	.setIcon(android.R.drawable.ic_dialog_alert)
        	
        	.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

        	    public void onClick(DialogInterface dialog, int whichButton) {
        	        
        	        deletereq(Constants.getURL_DeleteMedicationDrug(String.valueOf(AppController.getViewUser().getId()),""+listMedi.get(pos).getId()), pos, flag);
        	    }})
        	 .setNegativeButton(android.R.string.no, null).show();
		}

		@Override
		public void activeDrug(int pos, String flag) {
			// TODO Auto-generated method stub
			shareReq(Constants.getURL_Medi_Active(String.valueOf(AppController.getViewUser().getId()),""+listMedi.get(pos).getId()), pos, true);
		}

		@Override
		public void shareDrug(int pos, String flag) {
			// TODO Auto-generated method stub
			shareReq(Constants.getURL_Medi_Public(String.valueOf(AppController.getViewUser().getId()),""+listMedi.get(pos).getId()), pos, true);
		}
	}, listMedi);
	medlist.setAdapter(medadapter);
	}
	
	/**
	 * 
	 */
	private void warrningBox() {
		// TODO Auto-generated method stub		
			warrningBox=new AlertDialog.Builder(getActivity());
			warrningBox.setMessage(R.string.msg_pri_change);
			warrningBox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			try {
				dialog.dismiss();
				Intent relogin=new Intent(getActivity(), MainActivity.class);
				relogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(relogin);
				getActivity().finish();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	});
			AlertDialog dialog1 = warrningBox.create();
			dialog1.setCancelable(false);
			dialog1.show();
	}
	
	public Handler mHandler_warnning = new Handler(Looper.getMainLooper()) {
	    @Override
	    public void handleMessage(Message message) {
	      warrningBox();
	    
	    }
	};
	
}
