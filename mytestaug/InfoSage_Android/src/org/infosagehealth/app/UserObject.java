package org.infosagehealth.app;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * 
 *@author user 
 *@use user object model class. convert json string to user model.
 */
public class UserObject implements Parcelable{

	String uname, imgurl, id, fname, lname, imgpath, isElder; 
	JSONObject userobj, user, targetobj;
	
	public UserObject(String id1, String name, String url)
	{
		id = id1;
		uname = name;
		imgpath = url;
	}
	
	public UserObject(String id1, String fname1, String lname1, String imgpath1, JSONObject targetobj1)
	{
		id = id1;
		fname = fname1;
		lname = lname1;
		imgpath = imgpath1;
		
		if(targetobj1 == null || targetobj1.equals("null")){
			try {
				userobj = new JSONObject("null");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			userobj = targetobj1;
		}
	}
	
	public UserObject(Parcel in)
	{
		 //String[] data = new String[8];
	     //in.readStringArray(data);
	     this.id = in.readString();
	     this.uname = in.readString();
	     this.fname = in.readString();
	     this.lname = in.readString();
	     this.imgurl = in.readString();
	     this.imgpath = in.readString();
	     this.isElder = in.readString();
	   
	     try {
			this.userobj = new JSONObject(in.readString());
			this.user = new JSONObject(in.readString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
	}
	
	public String getName()
	{
		return uname;
	}
	public String getUrl()
	{
		return imgurl;
	}
	
	public void setName(String name)
	{
		uname = name;
	}
	
	public void setUrl(String url)
	{
		imgurl = url;
	}
	
	public void setUserobj(JSONObject uobj)
	{
		userobj = uobj;
	}
	
	public void setUser(JSONObject uobj)
	{
		user = uobj;
	}
	
	public void setTargetUser(JSONObject targetobj1)
	{
		targetobj = targetobj1;
	}
	
	public void setIsElder(String elder)
	{
		isElder = elder;
	}
	
	public String getId()
	{
		return id;
	}
	
	public String getIsElder()
	{
		return isElder;
	}
	
	public String getfname()
	{
		return fname;
	}
	
	public String getlname()
	{
		return lname;
	}
	
	public String getimgpath()
	{
		return imgpath;
	}
	
	public JSONObject getuserobj()
	{
		return userobj;
	}
	
	public JSONObject getuser()
	{
		return user;
	}
	
	public JSONObject getTargetUser()
	{
		return targetobj;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(this.id);
		dest.writeString(this.uname);
		dest.writeString(this.fname);
		dest.writeString(this.lname);
		dest.writeString(this.imgurl);
		dest.writeString(this.imgpath);
		dest.writeString(this.isElder);
		if(this.userobj != null)
		{	dest.writeString(this.userobj.toString());}
		else
		{	dest.writeString("");}
		if(this.user != null)
		{	dest.writeString(this.user.toString());}
		else
		{	dest.writeString("");}
	}
	
	 public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
	        public UserObject createFromParcel(Parcel in) {
	            return new UserObject(in);
	        }

	        public UserObject[] newArray(int size) {
	        	
	            return new UserObject[size];
	        }
	    };
}
