package org.infosagehealth.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.infosagehealth.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.infosage.item.Form;
import com.infosage.item.ItemMeditation;
import com.infosage.item.ItemPertaining;
import com.infosage.item.ItemSearchMedication;
import com.infosage.item.User;
import com.infosage.util.Constants;
import com.infosage.util.MyFunction;
import com.infosage.view.CustomAnimatedProgress;

/**
 * 
 * @author Govinda P.
 *
 */
public class AddMedicationsFragment extends Fragment {

	ListView medlist;
	RelativeLayout relcomm;
	LinearLayout lay2;
	Button btnsave, btnclose, btnclear, btnedit;
	ImageView btnback;
	CheckBox btnchk;
	EditText txtupdate, txtnotes;
	TextView txtmedroute, txtmeddose, txtmedtype, txtmedsymt, txtmedfreq;
	AutoCompleteTextView txtmedname;
	TextView txt_title;
	ArrayList<ItemSearchMedication> list_medi_search = new ArrayList<ItemSearchMedication>();
	ItemPertaining item_drug_option = new ItemPertaining();
	// Adapter
	ArrayAdapter<String> adapter_medi_search = null;
	ItemMeditation postItem = null;
	CustomAnimatedProgress progress = null;
	public static boolean clicks = false;
	public static boolean item_clicks = false;
	float dist;
	public static ArrayList<String> form_routearr = new ArrayList<String>();// {"Unknown","Oral","Injection"};
	public static ArrayList<String> form_typearr = new ArrayList<String>();// {"Unknown","Solution"};
	public static ArrayList<String> form_dosearr = new ArrayList<String>();// {"Unknown","0.4 mg/mL","1.5 mg/mL"};
	public static List<String> form_freqarr = new ArrayList<String>();// {"Unknown","As Needed","One Time","Once Daily","Twice Daily"};

	public static List<String> form_symptom = new ArrayList<String>();
	public static User postuser = new User();
	String itemJson = "";
	public static boolean isEditPage;
	AlertDialog.Builder warrningBox=null;
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onStart()
	 */
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		AppController.getInstance().cancelPendingRequests("editlist");
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		if (getArguments() != null) {
			try {
				itemJson = getArguments().getString("itemJson");

				postItem = (ItemMeditation) AppController.gson.fromJson(
						itemJson, ItemMeditation.class);
				Log.e("Edit Item", postItem.getName());

			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	
	/**
	 * @use use to enable /disable all view (Clickble ,selector)
	 * @param isEnable
	 */
	private void setViewEnable(boolean isEnable)
	{
		if (isEnable) {
			txtmedroute.setHintTextColor(Color.BLACK);
			txtmeddose.setHintTextColor(Color.BLACK);
			txtmedtype.setHintTextColor(Color.BLACK);
			txtmedfreq.setHintTextColor(Color.BLACK);
			txtmedname.setHintTextColor(Color.BLACK);
			txtnotes.setHintTextColor(Color.GRAY);
			txtmedsymt.setHintTextColor(Color.BLACK);
		}
		else
		{
			txtmedroute.setHintTextColor(Color.GRAY);
			txtmeddose.setHintTextColor(Color.GRAY);
			txtmedtype.setHintTextColor(Color.GRAY);
			txtmedfreq.setHintTextColor(Color.GRAY);
			txtmedname.setHintTextColor(Color.GRAY);
			txtnotes.setHintTextColor(Color.GRAY);
			txtmedsymt.setHintTextColor(Color.GRAY);
		}
	}
	
	/**
	 * @author user Gp
	 * @param view : add medication page view.
	 * @use : use to init all child view i.e. view setup
	 */
	private void init(View view){
		txt_title = (TextView) view.findViewById(R.id.headtxt);
		btnsave = (Button) view.findViewById(R.id.btnsave);
		btnback = (ImageView) view.findViewById(R.id.button1);
		txtmedroute = (TextView) view.findViewById(R.id.txtmedroute);
		txtmedroute.setHintTextColor(Color.GRAY);
		txtmeddose = (TextView) view.findViewById(R.id.txtmeddose);
		txtmeddose.setHintTextColor(Color.GRAY);
		txtmedtype = (TextView) view.findViewById(R.id.txtmedtype);
		txtmedtype.setHintTextColor(Color.GRAY);
		txtmedfreq = (TextView) view.findViewById(R.id.txtmedfreq);
		txtmedfreq.setHintTextColor(Color.GRAY);
		btnchk = (CheckBox) view.findViewById(R.id.btnchk);
		btnchk.setTextColor(Color.BLACK);
		btnchk.setText("Do not show this to my network (other than proxies).");
		btnchk.setButtonDrawable(R.drawable.selector_checkbox);

		txtmedname = (AutoCompleteTextView) view.findViewById(R.id.txtmedname);
		txtmedname.setHintTextColor(Color.GRAY);
		txtmedname.setMaxLines(1);
		txtmedname.setLines(1);
		txtmedname.setThreshold(1);
		
		txtnotes = (EditText) view.findViewById(R.id.txtnotes);
		txtnotes.setHintTextColor(Color.GRAY);
		txtnotes.setTextColor(Color.BLACK);
		//txtnotes.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS|InputType.TYPE_TEXT_FLAG_MULTI_LINE);
		txtnotes.setMaxLines(15);
		txtnotes.setLines(3);
		
		txtmedsymt = (TextView) view.findViewById(R.id.txtmedsymt);
		txtmedsymt.setHintTextColor(Color.GRAY);
		txtmedsymt.setHint("Associated Symptoms");
		txtmedsymt.setVisibility(View.GONE);
		btnclear = (Button) view.findViewById(R.id.btnclear);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.addmedications, container, false);
		progress = new CustomAnimatedProgress(getActivity());
		
		/**
		 *@use get default frequency array and symptoms array  
		 */
		form_freqarr = Constants.getFrequencyArray();
		form_symptom = Constants.getSymptomsArray();

		init(view);

		if (postItem != null) {
			isEditPage = true;
			clicks = true;
			btnback.setVisibility(View.VISIBLE);
			btnclear.setText("Cancel");
			btnsave.setText("Save Changes");
			txt_title.setText("Edit Medications");
			txtmedname.setText("" + postItem.getName());
			txtmeddose.setText("" + postItem.getDosage());
			txtmedroute.setText("" + postItem.getRoute());
			txtmedtype.setText("" + postItem.getType());
			txtnotes.setText("" + postItem.getOtherNotes());
			txtmedfreq.setText(""
					+ form_freqarr.get((int) (postItem.getDrugFrequency()
							.getId() - 1)));
			Log.e("Freq", " "+txtmedfreq.getText().toString().trim().toLowerCase());
			if (txtmedfreq.getText().toString().trim().toLowerCase().equals("as needed")) 
			{
				txtmedsymt.setVisibility(View.VISIBLE);
				try {
					if (postItem.getDrugFrequency().isAllowsSymptom()) {
						txtmedsymt.setText(""
								+ form_symptom.get((int) (postItem.getDrugSymptom()
										.getId() - 1)));
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}else
			{
				txtmedsymt.setVisibility(View.GONE);
			}
			
			setForEdit();
			txtmedroute.setEnabled(true);
			txtmedtype.setEnabled(true);
			txtmeddose.setEnabled(true);
			txtmedfreq.setEnabled(true);
			txtmedsymt.setEnabled(true);
			txtnotes.setEnabled(true);
			setViewEnable(true);
			
			Log.e("Is Public", " " + postItem.isIsPublic());

			if (postItem.isIsPublic()) {
				btnchk.setChecked(false);
			} else {
				btnchk.setChecked(true);
			}
			
			getMediIsValid(Constants.getURL_Medi_Check(""+AppController.getUser().getId(),""+postItem.getId()),"");
			
		} else {
			isEditPage = false;
			btnback.setVisibility(View.VISIBLE);
			txt_title.setText("Add New Medications");
			txtmedroute.setEnabled(false);
			txtmedtype.setEnabled(false);
			txtmeddose.setEnabled(false);
			txtmedfreq.setEnabled(false);
			txtmedsymt.setEnabled(false);
			txtnotes.setEnabled(false);
			
			btnchk.setEnabled(false);
			btnsave.setEnabled(false);
			btnclear.setEnabled(false);
		}

		return view;
	}

	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onDestroy()
	 */
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onStop()
	 */
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		try {
			HideKey.hideSoftKeyboard(getActivity());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		txtmedroute.setOnClickListener(new clicklistener(1));
		txtmedtype.setOnClickListener(new clicklistener(2));
		txtmeddose.setOnClickListener(new clicklistener(3));
		txtmedfreq.setOnClickListener(new clicklistener(4));
		txtmedsymt.setOnClickListener(new clicklistener(5));

		/**
		 *@user Save / add new medication call
		 */
		btnsave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (item_drug_option != null) {

					JSONObject obj = new JSONObject();
					String dosage = null, route = null, type = null, otherNotes, drugFrequency = null, drugSymptom = null;
					if (txtmeddose.getText().toString().equals("")
							|| txtmeddose.getText().toString()
									.equals("Unknown")) {
						dosage = "Unknown";
					} else {
						dosage = txtmeddose.getText().toString();
					}
					if (txtmedtype.getText().toString().equals("")
							|| txtmedtype.getText().toString()
									.equals("Unknown")) {
						type = "Unknown";
					} else {
						type = txtmedtype.getText().toString();
					}
					if (txtmedroute.getText().toString().equals("")
							|| txtmedroute.getText().toString()
									.equals("Unknown")) {
						route = "Unknown";
					} else {
						route = txtmedroute.getText().toString();
					}
					if (txtmedfreq.getText().toString().equals("")
							|| txtmedfreq.getText().toString()
									.equals("Unknown")) {

						drugFrequency = MyFunction.getJsonBody(
								new String[] { "id", "name" },
								new String[] { "1", "Unknown" }).toString();
					} else {
						obj = new JSONObject();

						if (txtmedfreq.getText().toString().equals("As needed")) {
							try {
								obj.put("allowsSymptom", 1);
								obj.put("id",
										form_freqarr.indexOf(txtmedfreq
												.getText().toString().trim()) + 1);
								obj.put("name", txtmedfreq.getText().toString()
										.trim());
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							drugFrequency = obj.toString();
						} else {
							try {
								obj.put("allowsSymptom", 0);
								obj.put("id",
										form_freqarr.indexOf(txtmedfreq
												.getText().toString().trim()) + 1);
								obj.put("name", txtmedfreq.getText().toString()
										.trim());
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							drugFrequency = obj.toString();
						}

						if (txtmedfreq.getText().toString().equals("As needed")) {
							obj = new JSONObject();
							try {
								obj.put("allowsSymptom", 0);
								obj.put("id",
										form_freqarr.indexOf(txtmedfreq
												.getText().toString().trim()) + 1);
								obj.put("name", txtmedfreq.getText().toString()
										.trim());
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					if (txtmedsymt.getText().toString().equals("")) {
						obj = new JSONObject();
						try {
							obj.put("id", 0);
							obj.put("name", "N/A");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						drugSymptom = obj.toString();

					} else {
						obj = new JSONObject();

						try {
							obj.put("id",
									form_symptom.indexOf(txtmedsymt.getText()
											.toString().trim()) + 1);
							obj.put("name", txtmedsymt.getText().toString()
									.trim());
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						drugSymptom = obj.toString();

					}

					otherNotes = MyFunction.LRTrim(txtnotes.getText().toString());
					
				
					JSONObject jsonBody = null;
					try {
						jsonBody = new JSONObject();

						// jsonBody.put("userId",UserTasks.userobject.getId());
						JSONObject userObj = new JSONObject(AppController.gson
								.toJson(AppController.getViewUser()));
						jsonBody.put("user", userObj);
						if (isEditPage && postItem != null) {
							jsonBody.put("id", postItem.getId());
							jsonBody.put("name", postItem.getName());
							jsonBody.put("drugImoCode",
									postItem.getDrugImoCode());
						} else {
							isEditPage = false;
							jsonBody.put("name", item_drug_option.getName());
							jsonBody.put("drugImoCode",
									item_drug_option.getCode());
						}
						jsonBody.put("dosage", dosage);
						jsonBody.put("route", route);
						jsonBody.put("type", type);
						jsonBody.put("drugFrequency", new JSONObject(
								drugFrequency));
						jsonBody.put("drugSymptom", new JSONObject(drugSymptom));
						jsonBody.put("otherNotes", otherNotes);
						if (btnchk.isChecked()) {
							jsonBody.put("isPublic", false);
						} else {
							jsonBody.put("isPublic", true);
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					
					
					if (isEditPage) {
						// AppController.session.getLogUserId()
						postAddMedication(
								Constants.getURL_MedicationList(AppController
										.getViewUser().getId() + ""), jsonBody,
								Method.PUT);
					} else {
						postAddMedication(
								Constants.getURL_MedicationList(AppController
										.getViewUser().getId() + ""), jsonBody,
								Method.POST);
					}
				} else {
					Toast.makeText(getActivity(), "Please select medication",
							2000).show();
				}
			}
		});

		btnclear.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					if (isEditPage) {
						getFragmentManager().popBackStackImmediate();
					} else {
						txtmedname.setText("");
						txtmedroute.setText("");
						txtmedtype.setText("");
						txtmeddose.setText("");
						txtmedfreq.setText("");
						txtmedsymt.setText("");
						txtnotes.setText("");
						// item_drug_option=new ItemPertaining();
						reset();
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});

		btnback.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getFragmentManager().popBackStackImmediate();
			}
		});

		txtmedname.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

				if (txtmedname.isPerformingCompletion()) {
			        // An item has been selected from the list. Ignore.
			        return;
			    }else{
				if (s.length() > 0 ) {//&& count <= 1
					item_drug_option = null;
					// reset_all();
					String result = String.valueOf(s).replaceAll(
							"[|?*<\":>+\\[\\]/']", " ");
					getMediSearch(Constants.getURL_Medi_search(String
							.valueOf(result)), result);
					}
			    }
				if (s.length() < 1) {
					item_drug_option = null;
					txtmedroute.setEnabled(false);
					txtmedtype.setEnabled(false);
					txtmeddose.setEnabled(false);
					txtmedfreq.setEnabled(false);
					txtmedsymt.setEnabled(false);
					txtnotes.setEnabled(false);
					btnchk.setEnabled(false);
					btnsave.setEnabled(false);
					btnclear.setEnabled(false);
					setViewEnable(false);
					reset();
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});

		txtmedname.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				// TODO Auto-generated method stub
				item_clicks=true;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
			}
		});
		
		txtmedname.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				try {					
					item_clicks=true;
					clicks = true;
					if (isEditPage) {
						postItem.setName(list_medi_search.get(position).getTitle());
						postItem.setDrugImoCode(""+ list_medi_search.get(position).getCode());
					}
					getDrugOption(
							Constants.getURL_Medi_drugopt(""+ list_medi_search.get(position).getCode()),"");
					
					/*
					 * Enable on drug click
					 */
					txtmedroute.setEnabled(true);
					txtmedtype.setEnabled(true);
					txtmeddose.setEnabled(true);
					txtmedfreq.setEnabled(true);
					txtmedsymt.setEnabled(true);
					txtnotes.setEnabled(true);

					btnchk.setEnabled(true);
					btnsave.setEnabled(true);
					btnclear.setEnabled(true);
					setViewEnable(true);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});

		btnchk.setTag("0");
		btnchk.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					btnchk.setTag("1");
				} else {
					btnchk.setTag("0");
				}
			}
		});
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	/**
	 *@use click lisnr class used to set click operation on TYPE 
	 */
	private class clicklistener implements View.OnClickListener {
		int pos;
		String title = "";
		ArrayAdapter<String> routeadapter;

		public clicklistener(int pos1) {
			// TODO Auto-generated constructor stub

			pos = pos1;
			if (pos == 1) {
				title = "Select Route";
				routeadapter = new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_list_item_1, form_routearr);
			}
			if (pos == 2) {
				title = "Select Type";
				routeadapter = new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_list_item_1, form_typearr);
			}
			if (pos == 3) {
				title = "Select Dosage";
				routeadapter = new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_list_item_1, form_dosearr);
			}
			if (pos == 4) {
				title = "Select Frequency";
				routeadapter = new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_list_item_1, form_freqarr);
			}
			if (pos == 5) {
				title = "Select Symptoms";
				routeadapter = new ArrayAdapter<String>(getActivity(),
						android.R.layout.simple_list_item_1, form_symptom);
			}
		}

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			new AlertDialog.Builder(getActivity()).setTitle(title)
					.setAdapter(routeadapter, new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							switch (pos) {
							case 1:
								txtmedroute.setText(form_routearr.get(which)
										.toString());

								setReset(form_routearr.get(which).toString(),
										txtmedtype.getText().toString(),
										txtmeddose.getText().toString(), 1);

								break;
							case 2:
								txtmedtype.setText(form_typearr.get(which)
										.toString());

								setReset(txtmedroute.getText().toString(),
										form_typearr.get(which).toString(),
										txtmeddose.getText().toString(), 2);

								break;
							case 3:
								txtmeddose.setText(form_dosearr.get(which)
										.toString());

								setReset(txtmedroute.getText().toString(),
										txtmedtype.getText().toString(),
										form_dosearr.get(which).toString(), 3);

								break;
							case 4:
								txtmedfreq.setText(form_freqarr.get(which)
										.toString());
								if (txtmedfreq.getText().toString()
										.equals("As needed")) {
									txtmedsymt.setVisibility(View.VISIBLE);
									txtmedsymt.setText("");
								} else {
									txtmedsymt.setVisibility(View.GONE);
									txtmedsymt.setText("");
								}
								break;
							case 5:
								txtmedsymt.setText(form_symptom.get(which)
										.toString());

								break;
							default:
								break;
							}

						}
					}).create().show();
		}
	}

	/**
	 * @use : type keyword by user and find medication 
	 * @param url 
	 * @param str char string .i.e. type by user in search box
	 */
	private void getMediSearch(String url, String str) {

		JsonArrayRequest req = new JsonArrayRequest(url,
				new Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray arg0) {
						Log.e("UPDATE PAGE", " " + arg0.toString());
						// gson.fromJson(arg0.toString(), ItemUpdate[].class);
						try {
							list_medi_search.clear();
							List<ItemSearchMedication> templist = Arrays
									.asList(AppController.gson.fromJson(
											arg0.toString(),
											ItemSearchMedication[].class));
							/**
							 * Deleted post and comments setup
							 */
							ArrayList<String> name = new ArrayList<String>();
							for (ItemSearchMedication itemmediseach : templist) {
								name.add(itemmediseach.getTitle());
							}
							list_medi_search.addAll(templist);
							// Log.e("Medi count=", " "+list_medi_search.size()
							// +"");

							adapterInit(name);
							txtmedname.showDropDown();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, err) {

			@Override
			protected VolleyError parseNetworkError(VolleyError volleyError) {
				// TODO Auto-generated method stub
				 if (progress!=null && progress.isShowing()) {
						progress.dismiss();
		        	   }
		        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
		        		   VolleyError error = null;
		                   try {
		                	   error= new VolleyError(new String(volleyError.networkResponse.data));
		                	  
		                	   JSONObject jsonObject = new JSONObject(error.getMessage());
		                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
		                   		{
		                		   Message message = mHandler_warnning.obtainMessage();
		                           message.sendToTarget();
		                   		}
		                   }catch(Exception e){
		                	   e.printStackTrace();
		                   }
		        	   }
				MyFunction.parseNetworkErrorMy(volleyError);
				return super.parseNetworkError(volleyError);
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				// TODO Auto-generated method stub
				return AppController.createBasicAuthHeader();
			}

			@Override
			protected String getParamsEncoding() {
				// TODO Auto-generated method stub
				Log.e("Encoding", " " + getParamsEncoding());
				return super.getParamsEncoding();
			}
		};
		AppController.getInstance().addToRequestQueue(req, "fetch");
	}

	/**
	 * @use get drug from selected option from drop down
	 * @param url
	 * @param edit
	 */
	public void getDrugOption(String url, final String edit) {

		JsonObjectRequest req = new JsonObjectRequest(Method.GET, url,
				new Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject arg0) {

						clicks = false;
						try {
							// item_drug_option.clear();
							ItemPertaining templist = AppController.gson
									.fromJson(arg0.toString(),
											ItemPertaining.class);
							item_drug_option = templist;
							if (edit.equals("true")) {
								setReset(postItem.getRoute(),
										postItem.getType(),
										postItem.getDosage(), 0);
							} else {
								txtmeddose.setText("");
								txtmedfreq.setText("");
								txtmedroute.setText("");
								txtmedsymt.setText("");
								txtmedtype.setText("");
								setReset("", "", "", 0);
							}
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							clicks = false;
						}
					}
				}, err) {

			@Override
			protected VolleyError parseNetworkError(VolleyError volleyError) {
				clicks = false;
				// TODO Auto-generated method stub
				 if (progress!=null && progress.isShowing()) {
						progress.dismiss();
		        	   }
		        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
		        		   VolleyError error = null;
		                   try {
		                	   error= new VolleyError(new String(volleyError.networkResponse.data));
		                	  
		                	   JSONObject jsonObject = new JSONObject(error.getMessage());
		                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
		                   		{
		                		   Message message = mHandler_warnning.obtainMessage();
		                           message.sendToTarget();
		                   		}
		                   }catch(Exception e){
		                	   e.printStackTrace();
		                   }
		        	   }
				MyFunction.parseNetworkErrorMy(volleyError);
				return super.parseNetworkError(volleyError);
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				// TODO Auto-generated method stub
				return AppController.createBasicAuthHeader();
			}

			@Override
			protected String getParamsEncoding() {
				// TODO Auto-generated method stub
				Log.e("Encoding", " " + getParamsEncoding());
				return super.getParamsEncoding();
			}
		};
		AppController.getInstance().addToRequestQueue(req, "fetch");
	}

	/**
	 * @use add new medication. if success then close page 
	 * @param url URL post new medication add
	 * @param jsonBody : param .i.e. post with call
	 * @param method POST (1)
	 */
	private void postAddMedication(String url, JSONObject jsonBody, int method) {
	

		try {
			jsonBody = new JSONObject(jsonBody.toString());
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String tag_json_obj = "ADD";
		final int met = method;
		progress.show();

		JsonObjectRequest jsonObjReq = new JsonObjectRequest(met, url,
				jsonBody, new Response.Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject response) {
						Log.e("Log Add medication", " "+response.toString());
						progress.dismiss();
						getFragmentManager().popBackStackImmediate();
						
					}
				}, err) {
			@Override
			protected VolleyError parseNetworkError(VolleyError volleyError) {
				// TODO Auto-generated method stub
				
				try {
					 if (progress!=null && progress.isShowing()) {
							progress.dismiss();
			        	   }
			        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
			        		   VolleyError error = null;
			                   try {
			                	   error= new VolleyError(new String(volleyError.networkResponse.data));
			                	  
			                	   JSONObject jsonObject = new JSONObject(error.getMessage());
			                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
			                   		{
			                		   Message message = mHandler_warnning.obtainMessage();
			                           message.sendToTarget();
			                   		}
			                   }catch(Exception e){
			                	   e.printStackTrace();
			                   }
			        	   }
					MyFunction.parseNetworkErrorMy(volleyError);
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				return super.parseNetworkError(volleyError);
			}

			@Override
			protected Response<JSONObject> parseNetworkResponse(
					NetworkResponse response) {
				// TODO Auto-generated method stub
			
				return super.parseNetworkResponse(response);
			}

			/**
			 * Passing some request headers
			 * */
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				// TODO Auto-generated method stub
				return AppController.createBasicAuthHeader();
			}

		};
		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);

	}

	Response.ErrorListener err = new Response.ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError volleyError) {
			// TODO Auto-generated method stub
			MyFunction.CheckError(getActivity(), volleyError);
		}
	};

	/**
	 * @use reset / set with receive response
	 * @param s1
	 * @param s2
	 * @param s3
	 * @param click
	 */
	private void setReset(String s1, String s2, String s3, int click) {

		if (click == 0) {
			reset();

		} else if (click == 1) {
			form_dosearr.clear();
			// form_freqarr.clear();
			form_typearr.clear();

			form_dosearr.add("Unknown");
			// form_freqarr.add("Unknown");
			form_typearr.add("Unknown");
		} else if (click == 2) {
			form_dosearr.clear();
			// form_freqarr.clear();
			form_routearr.clear();

			form_dosearr.add("Unknown");
			// form_freqarr.add("Unknown");
			form_routearr.add("Unknown");
		} else if (click == 3) {

			// form_freqarr.clear();
			form_routearr.clear();
			form_typearr.clear();

			// /form_freqarr.add("Unknown");
			form_routearr.add("Unknown");
			form_typearr.add("Unknown");
			
		}

		if (item_drug_option != null) {
			List<Form> form1 = item_drug_option.getForms();
			List<Form> finder = new ArrayList<Form>();

			for (int i = 0; i < form1.size(); i++) {
				if (s1.equals(form1.get(i).getRoute())
						|| (s1.equals("Unknown") || s1.equals(""))) {
					finder.add(form1.get(i));
				}
			}
			List<Form> finder1 = new ArrayList<Form>();
			for (int i = 0; i < finder.size(); i++) {
				if (s2.equals(finder.get(i).getType())
						|| (s2.equals("Unknown") || s2.equals(""))) {
					finder1.add(finder.get(i));
				}
			}
			List<Form> finder2 = new ArrayList<Form>();
			for (int i = 0; i < finder1.size(); i++) {
				if (s3.equals(finder1.get(i).getDosage())
						|| (s3.equals("Unknown") || s3.equals(""))) {
					finder2.add(finder1.get(i));
				}
			}
			for (int i = 0; i < finder2.size(); i++) {
				if (!form_dosearr.contains(finder2.get(i).getDosage())) {
					form_dosearr.add(finder2.get(i).getDosage());
				}
				if (!form_typearr.contains(finder2.get(i).getType())) {
					form_typearr.add(finder2.get(i).getType());
				}
				if (!form_routearr.contains(finder2.get(i).getRoute())) {
					form_routearr.add(finder2.get(i).getRoute());
				}
			}
		}
	}

	/**
	 * @use reset page field
	 * @deprecated Currently not in use.
	 */
	private void reset_all() {
		txtmeddose.setText("");
		txtmedfreq.setText("");
		txtmedname.setText("");
		txtmedroute.setText("");
		txtmedsymt.setText("");
		txtmedtype.setText("");
		reset();
	}

	/**
	 * @use first reset method that use to clear all fields.
	 */
	private void reset() {
		form_dosearr.clear();
		form_routearr.clear();
		form_typearr.clear();

		form_dosearr.add("Unknown");
		form_routearr.add("Unknown");
		form_typearr.add("Unknown");

		if (item_drug_option != null) {
			List<Form> form_orignal = item_drug_option.getForms();

			for (int i = 0; i < form_orignal.size(); i++) {
				if (!form_dosearr.contains(form_orignal.get(i).getDosage())) {
					form_dosearr.add(form_orignal.get(i).getDosage());
				}
				if (!form_typearr.contains(form_orignal.get(i).getType())) {
					form_typearr.add(form_orignal.get(i).getType());
				}
				if (!form_routearr.contains(form_orignal.get(i).getRoute())) {
					form_routearr.add(form_orignal.get(i).getRoute());
				}
			}
		}

	}

	public void setForEdit() {
		
		getDrugOption(
				Constants.getURL_Medi_drugopt("" + postItem.getDrugImoCode()),
				"true");

	}

	/**
	 * @use use to set/reset adapter with new list
	 * @param name drop down list.
	 */
	public void adapterInit(ArrayList<String> name) {
		// TODO Auto-generated method stub

		adapter_medi_search = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, name);
		// txtmedname.setThreshold(0);
		adapter_medi_search.notifyDataSetChanged();
		if (name.size() > 4) {
			int sumHeight = txtmedname.getHeight() * 4;
			txtmedname.setDropDownHeight(sumHeight);
		} else {
			txtmedname.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
		}

		// txtmedname.setDropDownBackgroundDrawable(getResources().getDrawable(R.drawable.dialog_baground));
		txtmedname.setAdapter(adapter_medi_search);
		adapter_medi_search.notifyDataSetChanged();

	}
	
	public Handler mHandler_warnning = new Handler(Looper.getMainLooper()) {
	    @Override
	    public void handleMessage(Message message) {
	        // This is where you do your work in the UI thread.
	        // Your worker tells you in the message what to do.
	    warrningBox();
	    
	    }
	};
	
	/**
	 * 
	 */
	private void warrningBox() {
		// TODO Auto-generated method stub
		
			warrningBox=new AlertDialog.Builder(getActivity());
			warrningBox.setMessage(R.string.msg_pri_change);
			warrningBox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			try {
				dialog.dismiss();
				Intent relogin=new Intent(getActivity(), MainActivity.class);
				relogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(relogin);
				getActivity().finish();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	});
			AlertDialog dialog1 = warrningBox.create();
			dialog1.setCancelable(false);
			dialog1.show();
	}
	
	
	/**
	 * @use : type keyword by user and find medication 
	 * @param url 
	 * @param str char string .i.e. type by user in search box
	 */
	private void getMediIsValid(String url, String str) {

		StringRequest req = new StringRequest(url,
				new Listener<String>() {
					@Override
					public void onResponse(String arg0) {
						Log.e("Medication Valid", " " + arg0.toString());
						// gson.fromJson(arg0.toString(), ItemUpdate[].class);
						try {
							if (arg0.toLowerCase().contains("false")) {
								
								warrningBox=new AlertDialog.Builder(getActivity());
								warrningBox.setMessage(R.string.msg_medication_invalid);
								warrningBox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {									
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								
								try {
									dialog.dismiss();									
								} 
								catch (Exception e) 
								{
									// TODO: handle exception
									e.printStackTrace();
								}
							}
						});
								AlertDialog dialog1 = warrningBox.create();
								dialog1.setCancelable(false);
								dialog1.show();
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, err) {

			@Override
			protected VolleyError parseNetworkError(VolleyError volleyError) {
				// TODO Auto-generated method stub
				 if (progress!=null && progress.isShowing()) {
						progress.dismiss();
		        	   }
		        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
		        		   VolleyError error = null;
		                   try {
		                	   error= new VolleyError(new String(volleyError.networkResponse.data));
		                	  
		                	   JSONObject jsonObject = new JSONObject(error.getMessage());
		                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
		                   		{
		                		   Message message = mHandler_warnning.obtainMessage();
		                           message.sendToTarget();
		                   		}
		                   }catch(Exception e){
		                	   e.printStackTrace();
		                   }
		        	   }
				MyFunction.parseNetworkErrorMy(volleyError);
				return super.parseNetworkError(volleyError);
			}

			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				// TODO Auto-generated method stub
				return AppController.createBasicAuthHeader();
			}

			@Override
			protected String getParamsEncoding() {
				// TODO Auto-generated method stub
				Log.e("Encoding", " " + getParamsEncoding());
				return super.getParamsEncoding();
			}
		};
		AppController.getInstance().addToRequestQueue(req, "fetch");
	}

}
