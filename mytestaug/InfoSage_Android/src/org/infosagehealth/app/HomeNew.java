package org.infosagehealth.app;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.infosagehealth.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.infosage.adapter.ImageAdapter;
import com.infosage.adapter.NotificationAdapter;
import com.infosage.item.ItemKeystone;
import com.infosage.item.ItemNotification;
import com.infosage.item.ItemPermissions;
import com.infosage.item.Notification;
import com.infosage.item.Owner;
import com.infosage.item.Target;
import com.infosage.item.User;
import com.infosage.pushservice.QuickstartPreferences;
import com.infosage.util.Constants;
import com.infosage.util.FindRedirectPage;
import com.infosage.util.MyFunction;
import com.infosage.view.BadgeView;
import com.infosage.view.CustomAnimatedProgress;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

/**
 * 
 * @author Govinda P.
 * @class home page . login user keystones list.
 */
public class HomeNew extends HideKey{

	GridView gridview;
	ArrayList<ItemKeystone> userobjlist = new ArrayList<ItemKeystone>();
	String path;
	DrawerLayout mDrawerLayout;
	Button menu_bt;
	ListView mDrawerList,listnotif;
	Context context;
	Button btnnotif,btnhide, btnlogout;
	BadgeView badge;
	RelativeLayout laynotif;
	NotificationAdapter adpt;
	ItemNotification list_notification=new ItemNotification();
	String userid;
	//static String fname, lname, profimg;
	LinearLayout lay_noti;
	User viewuser=null;
	CustomAnimatedProgress progressbar=null;
	// private CoordinatorLayout coordinatorLayout;
	// private BroadcastReceiver mRegistrationBroadcastReceiver;
	String logMobUserId,proxiedUser,operatingSystem,deviceType,screenSize; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		
		if (AppController.getMainUser()==null) {
			
			Intent intent=new Intent(getApplicationContext(), MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			return;
		}
		AppController.getInstance().setIntanceContext(getApplicationContext());
		progressbar=new CustomAnimatedProgress(HomeNew.this);
		progressbar.dismiss();
		
		String url = MainActivity.webserviceurl+"/users/"+AppController.getMainUser().getId()+"/connections?subscribed=true";
		getResponse(url,userid);
		getUserProfile();
		path = MainActivity.imgbaseurl+"/cache/w100xh100-2/images/";
		
		/**
		 * Notification and Count Display Declaration
		 */
		lay_noti=(LinearLayout)findViewById(R.id.lay_noti);
		btnnotif = (Button)findViewById(R.id.btnnotif);
		listnotif = (ListView)findViewById(R.id.listnotif);
		listnotif.setPadding(0, 0, 5, 0);
		laynotif = (RelativeLayout)findViewById(R.id.laynotif);
		btnlogout = (Button)findViewById(R.id.btnlogout);
		
		if (lay_noti!=null) {
			badge = new BadgeView(this, lay_noti);	
		}else
		{
			badge = new BadgeView(this, btnnotif);
		}
		
		badge.setBadgeMargin(0,0);
		
		badge.setTextSize(TypedValue.COMPLEX_UNIT_PX,
			    getResources().getDimension(R.dimen.badge_size));
		badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
		badge.hide();
		/**
		 * Get Notification
		 */
		//retry();
		
		btnnotif.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (list_notification.getNotifications().size()==0 || list_notification.getNotifications()==null) {
					Toast.makeText(getApplicationContext(), "You have no notifications!", Toast.LENGTH_LONG).show();
				}
				else
				{
					if(laynotif.getVisibility()==View.VISIBLE){
						laynotif.setVisibility(View.GONE);
						adpt.notifyDataSetChanged();
						if (list_notification.getNumUnresolved()<1) {
							badge.setText(""+list_notification.getNumUnresolved());
							badge.show();
						}
						else
						{
							badge.hide();
						}
					}else{
						laynotif.setVisibility(View.VISIBLE);
						badge.hide();
								for (int i = 0;i<list_notification.getNotifications().size();i++) {
									Notification item_noti = list_notification.getNotifications().get(i);
									if (!item_noti.isResolved()) {
										setRead(i,""+item_noti.getId(),true);
									}
								}
					}
				}
			}
		});
		
		btnhide = (Button)findViewById(R.id.btnhide);
		
		btnhide.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				laynotif.setVisibility(View.GONE);
				if (adpt!=null) {
					adpt.notifyDataSetChanged();
				}
				
				if (list_notification.getNumUnresolved()<1) {
					badge.setText(""+list_notification.getNumUnresolved());
					badge.show();
				}
				else
				{
					badge.hide();
				}
			}
		});
		
		btnlogout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				new AlertDialog.Builder(HomeNew.this)
				.setTitle("Confirm to Logout")
				.setMessage("Are you sure you want to Logout?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Logout.logoutAPI(new Listener<String>() {

							@Override
							public void onResponse(String response) {
								// TODO Auto-generated method stub
//								System.out.println("onResponse.........Logout Complete");
								 String uid=String.valueOf(AppController.getMainUser().getId());
						    	   
								StringRequest req = new StringRequest(Method.POST,Constants.getURL_logout(uid),new Listener<String>() {

									private SharedPreferences pref1;

									@Override
									public void onResponse(String arg0) {
										// TODO Auto-generated method stub
										
										 SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
										 pref1 = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
										 final String logintype=pref.getString("logintype","email");
										 if (logintype.equals("gp")) {
											
										}else if (logintype.equals("fb")) {
											 AccessToken acc=AccessToken.getCurrentAccessToken();
												if (acc!=null) {
													
													LoginManager.getInstance().logOut();
												}
										}
										 SharedPreferences.Editor editor = pref.edit();
										 editor.clear();
										 editor.apply();
										 /*anu........*/
										 String helpstatus = pref1.getString("helpstatus", "");
										 Log.d("helpstatus", ""+helpstatus);
										 
										 editor = pref1.edit();
										 editor.putString("helpstatus", helpstatus);
										 Log.d("helpstatus", ""+helpstatus);
										 editor.commit();
										 /*anu.......*/
										AppController.session.SetPushRegister(false);
										AppController.session.clearSession();
										Intent intent=new Intent(AppController.getInstance(),MainActivity.class);
										intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										startActivity(intent);
										finish();
										
									}
								},new ErrorListener() {

									@Override
									public void onErrorResponse(VolleyError arg0) {
										// TODO Auto-generated method stub
										Toast.makeText(getApplicationContext(), "Please try again.", Toast.LENGTH_LONG).show();
									}
								}) {
									
							           @Override
							        protected VolleyError parseNetworkError(
							        		VolleyError volleyError) 
							           {
							        	// TODO Auto-generated method stub
										     	
							        	return MyFunction.parseNetworkErrorMy(volleyError);
							        }
							           
							           @Override
							        public Map<String, String> getHeaders() throws AuthFailureError {
							        	// TODO Auto-generated method stub
							        	return AppController.createBasicAuthHeader();
							        }
							         
							            @Override
							            protected String getParamsEncoding() {
							            	// TODO Auto-generated method stub	            	
							            	return super.getParamsEncoding();
							            }	            
							        };		
							      AppController.getInstance().addToRequestQueue(req, "logout");
								
								
							}
						},new ErrorListener() {

							@Override
							public void onErrorResponse(VolleyError volleyError) {
								// TODO Auto-generated method stub
							//	Toast.makeText(getApplicationContext(), "Logout not done! Please try again.", Toast.LENGTH_LONG).show();
								System.out.println("onError.........Logout Incomplete");
								try {
					        		   volleyError= MyFunction.parseNetworkErrorMy(volleyError);
					        		   
						        			 try {
								        		   String uid=String.valueOf(AppController.getMainUser().getId());
								    	    	   if (uid.equals("")) {
								    				uid="-1";
								    	    	   }
									        		if (volleyError.getMessage().toString().contains("You can't do that")) {
									        			StringRequest req = new StringRequest(Method.POST,Constants.getURL_logout(uid),new Listener<String>() {

															@Override
															public void onResponse(String arg0) {
																// TODO Auto-generated method stub
																SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
																SharedPreferences pref1 = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
																final String logintype=pref.getString("logintype","email");
																 if (logintype.equals("gp")) {
																	
																}else if (logintype.equals("fb")) {
																	 AccessToken acc=AccessToken.getCurrentAccessToken();
																		if (acc!=null) {
																			Log.e("Available Access Token FB"," "+acc.getToken());
																			LoginManager.getInstance().logOut();
																		}
																}
																 SharedPreferences.Editor editor = pref.edit();
																 editor.clear();
																 editor.apply();
																 /*anu........*/
																 String helpstatus = pref1.getString("helpstatus", "");
																 Log.d("helpstatus", ""+helpstatus);
																 
																 editor = pref1.edit();
																 editor.putString("helpstatus", helpstatus);
																 Log.d("helpstatus", ""+helpstatus);
																 editor.commit();
																 /*anu.......*/
																AppController.session.SetPushRegister(false);
																AppController.session.clearSession();
																Intent intent=new Intent(AppController.getInstance(),MainActivity.class);
																intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
																startActivity(intent);
																finish();
															}
														},new ErrorListener() {

															@Override
															public void onErrorResponse(VolleyError arg0) {
																// TODO Auto-generated method stub
																
															}
														}) {
									    	    			
									 	    	           @Override
									 	    	        protected VolleyError parseNetworkError(
									 	    	        		VolleyError volleyError) 
									 	    	           {
									 	    	        	// TODO Auto-generated method stub
									 	    	        	 //  Log.e("Network"," "+ volleyError.toString());	            	
									 	    	        	return super.parseNetworkError(volleyError);
									 	    	        }
									 	    	           
									 	    	           @Override
									 	    	        public Map<String, String> getHeaders() throws AuthFailureError {
									 	    	        	// TODO Auto-generated method stub
									 	    	        	return AppController.createBasicAuthHeader();
									 	    	        }
									 	    	         
									 	    	            @Override
									 	    	            protected String getParamsEncoding() {
									 	    	            	// TODO Auto-generated method stub	            	
									 	    	            	return super.getParamsEncoding();
									 	    	            }	            
									 	    	        };		
									 	    	     AppController.getInstance().addToRequestQueue(req, "logout");
													}   
								        		   
											} catch (Exception e) {
												// TODO: handle exception
												e.printStackTrace();
											}            
					        	   }
					        	   catch(Exception e2)
					        	   {
					        		   e2.printStackTrace();
					        	   }
					        		   
							}
						} ) ;
					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
					}
				}).create().show();;
				
				
				
			}
			
		});
		
		listnotif.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
			//	view.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.animation_click));
				
				progressbar.show();
				laynotif.setVisibility(View.GONE);
				
				Notification item_noti = list_notification.getNotifications().get(position);
				if (!item_noti.isResolved()) {
					setRead(position,""+item_noti.getId(),false);
				}				
				final String[] link=FindRedirectPage.getLink(item_noti.getText(),
						item_noti.getForwardLink());
				
				String noti_user=link[1];
				viewuser=null;
				for (ItemKeystone usKeystone:userobjlist)
				{
					if (String.valueOf(usKeystone.getTarget().getId()).equals(noti_user)) 
					{
						String user=AppController.gson.toJson(usKeystone.getTarget());
						viewuser= AppController.gson.fromJson(user,User.class);
						break;
					}
				}
				
				if (viewuser==null) 
				{
					progressbar.dismiss();
					if(String.valueOf(AppController.getMainUser().getId()).equals(noti_user))
					{
						
					}else{
					Toast.makeText(getApplicationContext(),"You are not connected to this user.", Toast.LENGTH_LONG).show();
					}
				}
				else
				{
					final Intent intent = new Intent(getApplicationContext(), UserTasks.class);
					final Bundle b = new Bundle();
					if (link[0].equals("update")) {
						b.putInt("flag", 4);
					}
					if(link[0].equals("medication"))
					{
						b.putInt("flag", 5);
					}
					if (link[0].equals("todo")) {
						b.putInt("flag", 3);
					}					
					AppController.session.setMyUserid(""+viewuser.getId());
					
				
					b.putString("target",AppController.gson.toJson(viewuser));
				
					try {						
						AppController.setViewUser(viewuser);
						UserPermissionsSetup.getPermissionResponse(""+viewuser.getId(),""+AppController.getMainUser().getId());
					
						if (link[0].equals("medication")) {
					UserPermissionsSetup.getPer(""+viewuser.getId(),""+AppController.getMainUser().getId(),new Listener<JSONObject>() {

						@Override
						public void onResponse(JSONObject arg0) {
							// TODO Auto-generated method stub
							AppController.permissions=AppController.gson.fromJson(arg0.toString(), ItemPermissions.class);
							AppController.permissions.setLoguserid(""+AppController.getMainUser().getId());
							AppController.permissions.setSelectid(""+viewuser.getId());
							
							if (link[0].equals("medication")) {
								if (AppController.permissions.getSelectid().equals(""+viewuser.getId())
										&& AppController.permissions.isVIEWMEDICATIONS()
										) {								
										intent.putExtras(b);
										startActivity(intent);
										finish();
									}
									else
									{
										b.putInt("flag",3);;
										intent.putExtras(b);
										startActivity(intent);
										finish();
									}
							}
							
							
							/*anu 20 aug*/
							
							
							/*else if (link[0].equals("update")) {
								if (AppController.permissions.getSelectid().equals(""+viewuser.getId())
										&& AppController.permissions.isVIEWUPDATES()
										) {								
										intent.putExtras(b);
										startActivity(intent);
										finish();
									}
									else
									{
										b.putInt("flag",3);;
										intent.putExtras(b);
										startActivity(intent);
										finish();
									}
							}
							*/
							
							/*end anu*/
							
							
							
							
							progressbar.dismiss();	
						}
						});	
					}
					else
					{
						progressbar.dismiss();
						intent.putExtras(b);
						startActivity(intent);
						finish();
					}
						
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}					
				}
			}
		});
		
		// call Log Mobile Page View API
		logMobileView();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();		
	
		getNotification(Constants.getURL_Notification_Fetch(AppController.session.getLogUserId(),""), "", "", "");
		
		//for test
		getNotification(Constants.getURL_Notification_Fetch(AppController.session.getLogUserId(),""), "", "", "");
		
		AppController.activityResumed();
		NotificationManager notifManager= (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
  	  	notifManager.cancelAll();
		
		// Push Notification
		try {
			
	        LocalBroadcastManager.getInstance(HomeNew.this).registerReceiver(mRegistrationBroadcastReceiver,
	                new IntentFilter(QuickstartPreferences.PUSH_RECIVER));
	        
		} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
		}
	
	}
	
	private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           
         
            //getUnReadNotification(Constants.getURL_UnresolveNotification(AppController.session.getLogUserId()), "", "", "");
            Message message = mHandler.obtainMessage();
            message.sendToTarget();
            
        }
    };
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onPause()
	 */
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		
		super.onPause();
		AppController.getInstance().cancelPendingRequests("user");
		LocalBroadcastManager.getInstance(HomeNew.this).unregisterReceiver(mRegistrationBroadcastReceiver);
		AppController.activityPaused();
		
	}
	
	public void getResponse(String url,String userid)
	{
		progressbar.show();
		JsonArrayRequest req = new JsonArrayRequest(url, new Listener<JSONArray>() {

			@Override
			public void onResponse(JSONArray arg0) {
				
				List<ItemKeystone> templist = Arrays.asList(AppController.gson.fromJson(arg0.toString(), ItemKeystone[].class));
				progressbar.dismiss();
				userobjlist.clear();
				if (AppController.getMainUser().getIsElder()) {
					String user=AppController.gson.toJson(AppController.getMainUser());
					//User selectiuser = AppController.gson.fromJson(user,User.class);
					userobjlist.add(new ItemKeystone(AppController.gson.fromJson(user,Owner.class),
							AppController.gson.fromJson(user,Target.class)));
				}
				userobjlist.addAll(templist);
			
				if (templist.size()<1 || userobjlist.size()==1) {
					new AlertDialog.Builder(HomeNew.this)
					//.setTitle("You have no Keystones in your Network !")
					.setMessage(R.string.msg_pri_change)
					.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(getApplicationContext(),MainActivity.class);
    						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
    						startActivity(intent);
    						finish();
						}
					}).create().show();
				}else
				{
					
				}
					
					gridview = (GridView)findViewById(R.id.gridView1);
					
					gridview.setAdapter(new ImageAdapter(getApplicationContext(),HomeNew.this,userobjlist));
					progressbar.dismiss();
				//}
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				progressbar.dismiss();
				retry();
				 MyFunction.CheckError(getApplicationContext(), arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	return super.parseNetworkError(volleyError);
	        }
	           /* (non-Javadoc)
	         * @see com.android.volley.toolbox.JsonArrayRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
	         */
	        @Override
	        protected Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
	        	// TODO Auto-generated method stub
	      
	        	   Map<String, String> headers = response.headers;
	        	   String cookie = headers.get("Set-Cookie");
	        	   Log.e("Set-Cookie: "," "+ cookie);
	        	   AppController.getInstance().saveCookie(cookie);
	        	
	        	return super.parseNetworkResponse(response);
	        }
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	             	return super.getParamsEncoding();
	            }
	            
	        };		
	      AppController.getInstance().addToRequestQueue(req, "fetch");
	}
	
	public void getNotification(String url,String userid,String beforeid,String count)
	{
		
		JSONObject jsonBody=new JSONObject();
		try {
		if (!beforeid.equals("")) {		
				jsonBody.put("beforeId", beforeid);			
		}
		if (!count.equals("")) {
			jsonBody.put("count", count);
		}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
		
			e.printStackTrace();
		}
		JsonObjectRequest req = new JsonObjectRequest(Method.GET,url,jsonBody, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
					
			try {
			
				list_notification= AppController.gson.fromJson(arg0.toString(),ItemNotification.class);
				adpt = new NotificationAdapter(getApplicationContext(),list_notification.getNotifications());
				listnotif.setAdapter(adpt);
				try 
				{
					if (list_notification.getNumUnresolved()>0) {
						badge.setText(list_notification.getNumUnresolved()+"");					
						badge.show();
					}
					else
					{
						badge.hide();
					}
				} catch (Exception e) {
					// TODO: handle exception
				
					e.printStackTrace();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
			
				e.printStackTrace();
			}
			
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				/*progressbar.dismiss();
				retry();
				 MyFunction.CheckError(getApplicationContext(), arg0);*/
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	 return super.parseNetworkError(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	
	            	return super.getParamsEncoding();
	            }
	            
	        };
	        AppController.getInstance().getRequestQueue().getCache().remove(url);
	        AppController.getInstance().getRequestQueue().getCache().clear();
		
			AppController.getInstance().addToRequestQueue(req);
			AppController.getInstance().addToRequestQueue(req, "noti");
	      
	}
	
	public void getUnReadNotification(String url,String userid,String beforeid,String count)
	{
		JSONObject jsonBody=new JSONObject();
		try {
		if (!beforeid.equals("")) {		
				jsonBody.put("beforeId", beforeid);			
		}
		if (!count.equals("")) {
			jsonBody.put("count", count);
		}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JsonObjectRequest req = new JsonObjectRequest(Method.GET,url,jsonBody, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				
								
			try {
				ItemNotification temp= AppController.gson.fromJson(arg0.toString(),ItemNotification.class);
				
				// swaping 
				ItemNotification temp1=new ItemNotification();
				temp1=list_notification;
				list_notification=new ItemNotification();
				list_notification=temp;
				
				if (temp1.getNumUnresolved()!=0) {
					temp1.setNumUnresolved(temp1.getNumUnresolved()+list_notification.getNumUnresolved());
				}else
				{
					temp1.setNumUnresolved(list_notification.getNumUnresolved());
				}
				list_notification.getNotifications().addAll(temp1.getNotifications());
				
				adpt = new NotificationAdapter(getApplicationContext(),list_notification.getNotifications());
				listnotif.setAdapter(adpt);
				try 
				{
					if (list_notification.getNumUnresolved()>0) 
					{
						badge.setText(list_notification.getNumUnresolved()+"");					
						badge.show();
					}
					else
					{
						badge.hide();
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				 MyFunction.CheckError(getApplicationContext(), arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	 return super.parseNetworkError(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	
	            	return super.getParamsEncoding();
	            }
	            
	        };
		
	      AppController.getInstance().addToRequestQueue(req, "noti");

	}
	
	
	public void setRead(final int pos, String noti_id,final boolean all) {
		// TODO Auto-generated method stub
		// Tag used to cancel the request
				String tag_json_obj = "readnoti";
				
				progressbar.show();
				String url=Constants.getURL_Notification_Read(AppController.session.getLogUserId(), noti_id);
				//JSONObject jsonBody = MyFunction.getJsonBody(keys, values);
				JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.PUT,
		                url,
		                new Response.Listener<JSONObject>() {
		 
		                    @Override
		                    public void onResponse(JSONObject response) {
		                    
		                    	Notification object=AppController.gson.fromJson(response.toString(), Notification.class);
								
		                    	list_notification.getNotifications().set(pos, object);
		                    	if (all) {
		                    		
								}
		                    	else{
				                    	try {
				                    		long gp=list_notification.getNumUnresolved()-1;
				                    	
				                    		if (gp>0) {
				                    			badge.setText(gp+"");
												badge.show();
											}
				                    		else{
				                    			badge.setText(gp+"");
												badge.hide();
				                    		}
				                    		adpt.notifyDataSetChanged();
										} catch (Exception e) {
											// TODO: handle exception
											e.printStackTrace();
											badge.hide();
										}
				                    	
		                    	}
		                    	progressbar.dismiss();
		                    }
		                },new ErrorListener() {

							@Override
							public void onErrorResponse(VolleyError arg0) {
								// TODO Auto-generated method stub
								
								 MyFunction.CheckError(getApplicationContext(), arg0);
								progressbar.dismiss();
							}
						}){
				           @Override
				        protected VolleyError parseNetworkError(
				        		VolleyError volleyError) {
				        	// TODO Auto-generated method stub
				        	   
				        	return super.parseNetworkError(volleyError);
				        }
				           @Override
				        	protected Response<JSONObject> parseNetworkResponse(
				        			NetworkResponse response) {
				        		// TODO Auto-generated method stub
				        	   try {
				        		   
							} catch (Exception e) {
								// TODO: handle exception
							}
				        	  	return super.parseNetworkResponse(response);
				        	}
				            /**
				             * Passing some request headers
				             * */
				           @Override
					        public Map<String, String> getHeaders() throws AuthFailureError {
					        	// TODO Auto-generated method stub
					        	return AppController.createBasicAuthHeader();
					        }				           
				        };
				 
				// Adding request to request queue
				AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
	}
	
	public void retry()
	{
		 AlertDialog.Builder builder = new AlertDialog.Builder(HomeNew.this);
	        builder.setCancelable(false);
	        //builder.setTitle("No Internet");
	        builder.setMessage("Please Retry");

	        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	                dialog.dismiss();
	                finish();
	            }
	        });

	        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener(){
	            @Override
	            public void onClick(DialogInterface dialog, int which)
	            {
	                dialog.dismiss();
	                String url = MainActivity.webserviceurl+"/users/"+AppController.getMainUser().getId()+"/connections?subscribed=true";
            		getResponse(url,userid);
	            }
	        });
	        AlertDialog dialog = builder.create(); // calling builder.create after adding buttons
	        dialog.show();
		
	}
	private void getUserProfile()
	{
		
		
		JsonObjectRequest user_obj=new JsonObjectRequest(Constants.getUserProfile(""+AppController.getUser().getId()), new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				// TODO Auto-generated method stub
				User user=AppController.gson.fromJson(arg0.toString(),User.class);
				if (AppController.getMainUser().getIsElder()!=user.getIsElder()) {
					
					new AlertDialog.Builder(HomeNew.this)
					//.setTitle("Required Re-Login !!")
					.setMessage(R.string.msg_keystone_change)
					.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(getApplicationContext(),MainActivity.class);
    						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
    						startActivity(intent);
    						finish();
						}
					}).setCancelable(false).create().show();
				}
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				
			}
		}){
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	   //Log.e("Network"," "+ volleyError.toString());	            	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           /* (non-Javadoc)
	         * @see com.android.volley.toolbox.JsonArrayRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
	         */
	        @Override
	        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        	// TODO Auto-generated method stub
	        	  Map<String, String> headers = response.headers;
	        	   String cookie = headers.get("Set-Cookie");
	        	   Log.e("Set-Cookie: "," "+ cookie);
	        	   AppController.getInstance().saveCookie(cookie);
	        	
	        	return super.parseNetworkResponse(response);
	        }
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	//Log.e("Encoding"," "+getParamsEncoding());
	            	return super.getParamsEncoding();
	            }
	        };
	        AppController.getInstance().addToRequestQueue(user_obj, "user");
	}
	
	 public Handler mHandler = new Handler(Looper.getMainLooper()) {
		    @Override
		    public void handleMessage(Message message) {
		        // This is where you do your work in the UI thread.
		        // Your worker tells you in the message what to do.
		    	getNotification(Constants.getURL_Notification_Fetch(AppController.session.getLogUserId(),""), "", "", "");
		    }
		};
		
	private void logMobileView(){
		
		logMobUserId = AppController.permissions.loguserid;
		proxiedUser = String.valueOf(AppController.getUser().getId());
		operatingSystem = "Android";
		deviceType = MyFunction.getDeviceName();
		screenSize = MyFunction.getScreenSize(this);
		
		String url = Constants.HOST_URL+"/users/"+logMobUserId+"/elasticLog/pageview/proxiedUser/"+proxiedUser+"/mobile?operatingSystem="+operatingSystem+"&deviceType="+deviceType+"&screenSize="+screenSize;
		Log.e("logMobileView", "url: "+url);
		//url = "http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com/api/users/404/elasticLog/pageview/proxiedUser/404/mobile?operatingSystem=ANDROID&deviceType=MicromaxQ395&screenSize=720x1184";
		JSONObject jsonBody=new JSONObject();
		try {
			jsonBody.put("string", "home");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
		String urlEncoded = Uri.encode(url, ALLOWED_URI_CHARS);
		
		JsonObjectRequest user_obj=new JsonObjectRequest(Method.POST, urlEncoded, jsonBody, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				
				Log.e("logMobileView", "onResponse: "+arg0);
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				Log.e("logMobileView", "onErrorResponse: "+arg0);
			}
		}){
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	   //Log.e("Network"," "+ volleyError.toString());	            	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           /* (non-Javadoc)
	         * @see com.android.volley.toolbox.JsonArrayRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
	         */
	        @Override
	        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        	// TODO Auto-generated method stub
	        	  Map<String, String> headers = response.headers;
	        	   String cookie = headers.get("Set-Cookie");
	        	   Log.e("Set-Cookie: "," "+ cookie);
	        	   AppController.getInstance().saveCookie(cookie);
	        	   
	        	   try {
	                   String jsonString = new String(response.data,
	                           HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

	                   JSONObject result = null;

	                   if (jsonString != null && jsonString.length() > 0)
	                        result = new JSONObject(jsonString);

	                   return Response.success(result,
	                           HttpHeaderParser.parseCacheHeaders(response));
	               } catch (UnsupportedEncodingException e) {
	                   return Response.error(new ParseError(e));
	               } catch (JSONException je) {
	                   return Response.error(new ParseError(je));
	               }
	        	
	        	//return super.parseNetworkResponse(response);
	        }
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	//Log.e("Encoding"," "+getParamsEncoding());
	            	return super.getParamsEncoding();
	            }
	        };
	        AppController.getInstance().addToRequestQueue(user_obj, "user");
	}
	
}