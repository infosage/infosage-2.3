/**
*@authour user
*Apr 7, 2016
* @update date Apr 7, 2016
 */
package org.infosagehealth.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.infosage.googlesearch.Example;
import com.infosage.googlesearch.Item;
import com.infosage.googlesearch.SearchAdapter;
import com.infosage.util.Constants;
import com.infosage.util.MyFunction;
import com.infosage.view.CustomAnimatedProgress;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

/**
 * @author user
 *
 */


/*
 * engine id = '008268151070442059713:wo6u-ggjryo';
api key = 'AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU';
 * */

public class SearchActivity extends Fragment{
	
	SearchView searchview;
	ListView listview;
	SearchAdapter adapter;
	Button btnnxt,btnprev,btnhide;
	String query1;
	int cntr=1,totrecords=0;
	CustomAnimatedProgress progressbar=null;
	String logMobUserId,proxiedUser,operatingSystem,deviceType,screenSize; 
	TextView txtpgno,help_txt;
	EditText txtsearch;
	WebView srchweb;
	LinearLayout lay1;
	Example result1;
	SharedPreferences pref;
	private SharedPreferences pref1;
	/* (non-Javadoc)
	 * @see android.support.v7.app.AppCompatActivity#onCreate(android.os.Bundle)
	 */
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.search_activity, container, false);
		
		//searchview=(SearchView)rootView.findViewById(R.id.searchView1);
		txtsearch = (EditText)rootView.findViewById(R.id.txtsearch);
		listview=(ListView)rootView.findViewById(R.id.listSrch);
		progressbar=new CustomAnimatedProgress(getActivity());
		txtpgno=(TextView)rootView.findViewById(R.id.txtpgno);
	
		srchweb = (WebView)rootView.findViewById(R.id.srchweb);
		lay1 = (LinearLayout)rootView.findViewById(R.id.laysrch);
		help_txt = (TextView)rootView.findViewById(R.id.help_txt);
		btnhide = (Button)rootView.findViewById(R.id.btnhide);
		
		//pref = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
		pref1 = getActivity().getSharedPreferences("ssss", Context.MODE_PRIVATE);
		
		
		String helpstatus = pref1.getString("helpstatus", "");
		Log.e("helpstatus", ""+helpstatus);
		
		if(helpstatus == null || helpstatus.compareTo("")==0 || helpstatus.equals(null)){
			help_txt.setVisibility(View.VISIBLE);
			btnhide.setText("Hide");
			Log.e("helpstatus", ""+helpstatus);
		}
		else if(helpstatus.compareTo("Show")==0){
			help_txt.setVisibility(View.GONE);
			btnhide.setText("Show");
			Log.e("helpstatus", ""+helpstatus.toString());
		}else if(helpstatus.compareTo("Hide")==0){
			help_txt.setVisibility(View.VISIBLE);
			btnhide.setText("Hide");
			Log.e("helpstatus", ""+helpstatus);
		}
		/*else
		{
			help_txt.setVisibility(View.GONE);
			btnhide.setText("Show");
		}*/
		
		
		btnnxt = (Button)rootView.findViewById(R.id.btnnxt);
		btnnxt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				progressbar.show();
				cntr=cntr+10;
				SearchNextPrev(query1);
			}
		});
		btnprev = (Button)rootView.findViewById(R.id.btnprev);
		btnprev.setEnabled(false);
		btnprev.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				progressbar.show();
				if(cntr >= 10){
					cntr=cntr-10;
				}
				SearchNextPrev(query1);
			}
		});
		 
		btnprev.setText("< Previous");
		btnnxt.setText("Next >");
		
		btnprev.setVisibility(View.INVISIBLE);
		btnnxt.setVisibility(View.INVISIBLE);
		txtpgno.setVisibility(View.INVISIBLE);
		
		
		
		btnhide.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 SharedPreferences.Editor editor = pref1.edit();
		    	
		    	  
				if(help_txt.getVisibility() == View.VISIBLE){
					help_txt.setVisibility(View.GONE);
					btnhide.setText("Show");
					editor.putString("helpstatus", "Show");
			    	editor.commit();
				}else{
					help_txt.setVisibility(View.VISIBLE);
					btnhide.setText("Hide");
					editor.putString("helpstatus", "Hide");
			    	editor.commit();
				}
			}
		});
		
		listview.setDivider(null);
		listview.setDividerHeight(20);
		
		txtsearch.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
				// TODO Auto-generated method stub
				try {
					
					
					if(!txtsearch.getText().toString().trim().equals("")){
						query1=txtsearch.getText().toString();
						progressbar.show();
						HideKey.hideSoftKeyboard(getActivity());
						init_Search(query1);
					}
				}
				catch(Exception e){
					
				}
				return false;
			}
		});
		
		/*
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				Item item = result1.getItems().get(arg2);
				String url = item.getLink();
				
				lay1.setVisibility(View.GONE);
				srchweb.setVisibility(View.VISIBLE);
				srchweb.loadUrl(url);
			}
		});*/
		
		//searchview.setQueryHint(getResources().getString(R.string.txtsrchhint));
		
		 
		/* searchview.setOnSearchClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				      
			   InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			   imm.hideSoftInputFromWindow(searchview.getWindowToken(), 0);               
			}
		});
		 */
		/* searchview.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				return false;
			}
		});

		 */
		 
	        
	        //*** setOnQueryTextFocusChangeListener ***
		/*	
		searchview.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
					
				
			}
		});*/
	        
	        //*** setOnQueryTextListener ***
		/*	
		searchview.setOnQueryTextListener(new OnQueryTextListener() {
				
			@SuppressLint("NewApi")
			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				if(query != null && !query.equals(null)){
				Log.e("query", ""+query.toString());
				
				searchview.clearFocus();
				searchview.onActionViewCollapsed();
				query1=query;
				progressbar.show();
				init_Search(query);
				}
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
					
				
				return false;
			}
		});*/
		
		// call Log Mobile Page View API
		logMobileView();
			
		return rootView;
	}
	
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
	}
	
	@SuppressWarnings("static-access")
	public void onClick(View v) {
		
		  InputMethodManager im = (InputMethodManager)(getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE));
		   // im.showSoftInput(v, 0);
		  im.toggleSoftInput(0, 0);
	}
	
	
	private void init_Search(String seachStr){
		
		String url= "https://www.googleapis.com/customsearch/v1?key=AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU&cx=008268151070442059713:wo6u-ggjryo&q="+seachStr.toString()+"&alt=json";

		 JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, "", new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				Log.e("Json response", ""+response.toString());
	
				if (progressbar!=null && progressbar.isShowing()) {
					progressbar.dismiss();
				}
				if(response != null){
			    Example result=AppController.gson.fromJson(response.toString(),Example.class);
			    result1 = result;
  				//Log.e("Reocords"," "+AppController.gson.toJson(result).toString());
			    totrecords = Integer.parseInt(result.getSearchInformation().getTotalResults());
			    if(result.getItems().size() > 0){
	  				adapter=new SearchAdapter(getActivity(), result);
	  				listview.setAdapter(adapter);
	  				adapter.notifyDataSetChanged();
	  				
	  				btnprev.setVisibility(View.VISIBLE);
	  				btnnxt.setVisibility(View.VISIBLE);
	  				txtpgno.setVisibility(View.VISIBLE);
			    }
				}
			}
			}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
			// TODO Auto-generated method stub
				if (progressbar!=null && progressbar.isShowing()) {
					progressbar.dismiss();
				}
			}
			});

			AppController.getInstance().addToRequestQueue(jsObjRequest);
		
	}
	private void SearchNextPrev(String seachStr){
		
		String url= "https://www.googleapis.com/customsearch/v1?key=AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU&cx=008268151070442059713:wo6u-ggjryo&q="+seachStr.toString()+"&alt=json";

		if(cntr > 1){
			url = url + "&start="+cntr;
			btnprev.setEnabled(true);
		}else{
			btnprev.setEnabled(false);
		}
		int pgno = (cntr/10)+1;
		txtpgno.setText("Page "+pgno);
		
		 JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, "", new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				Log.e("Json response", ""+response.toString());
	
				if (progressbar!=null && progressbar.isShowing()) {
					progressbar.dismiss();
				}
				if(response != null){
				    Example result=AppController.gson.fromJson(response.toString(),Example.class);
				    result1 = result;
	  				Log.e("Reocords count"," "+result.getItems().size());
				    if(result.getItems().size() > 0){
		  				adapter=new SearchAdapter(getActivity(), result);
		  				listview.setAdapter(adapter);
		  				adapter.notifyDataSetChanged();
				    }
				}
			}
			}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
			// TODO Auto-generated method stub
			}
			});

			AppController.getInstance().addToRequestQueue(jsObjRequest);
		
	}
	
private void logMobileView(){
		
		logMobUserId = AppController.permissions.loguserid;
		proxiedUser = String.valueOf(AppController.getUser().getId());
		operatingSystem = "Android";
		deviceType = MyFunction.getDeviceName();
		screenSize = MyFunction.getScreenSize(getActivity());
		
		String url = Constants.HOST_URL+"/users/"+logMobUserId+"/elasticLog/pageview/proxiedUser/"+proxiedUser+"/mobile?operatingSystem="+operatingSystem+"&deviceType="+deviceType+"&screenSize="+screenSize;
		Log.e("logMobileView", "url: "+url);
		//url = "http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com/api/users/404/elasticLog/pageview/proxiedUser/404/mobile?operatingSystem=ANDROID&deviceType=MicromaxQ395&screenSize=720x1184";
		JSONObject jsonBody=new JSONObject();
		try {
			jsonBody.put("string", "resources");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
		String urlEncoded = Uri.encode(url, ALLOWED_URI_CHARS);
		
		JsonObjectRequest user_obj=new JsonObjectRequest(Method.POST, urlEncoded, jsonBody, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				
				Log.e("logMobileView", "onResponse: "+arg0);
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				Log.e("logMobileView", "onErrorResponse: "+arg0);
			}
		}){
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	   //Log.e("Network"," "+ volleyError.toString());	            	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           /* (non-Javadoc)
	         * @see com.android.volley.toolbox.JsonArrayRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
	         */
	        @Override
	        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        	// TODO Auto-generated method stub
	        	  Map<String, String> headers = response.headers;
	        	   String cookie = headers.get("Set-Cookie");
	        	   Log.e("Set-Cookie: "," "+ cookie);
	        	   AppController.getInstance().saveCookie(cookie);
	        	   
	        	   try {
	                   String jsonString = new String(response.data,
	                           HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

	                   JSONObject result = null;

	                   if (jsonString != null && jsonString.length() > 0)
	                        result = new JSONObject(jsonString);

	                   return Response.success(result,
	                           HttpHeaderParser.parseCacheHeaders(response));
	               } catch (UnsupportedEncodingException e) {
	                   return Response.error(new ParseError(e));
	               } catch (JSONException je) {
	                   return Response.error(new ParseError(je));
	               }
	        	
	        	//return super.parseNetworkResponse(response);
	        }
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	//Log.e("Encoding"," "+getParamsEncoding());
	            	return super.getParamsEncoding();
	            }
	        };
	        AppController.getInstance().addToRequestQueue(user_obj, "user");
	}
}
