package org.infosagehealth.app;


import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.infosage.item.ItemPermissions;
import com.infosage.item.User;
import com.infosage.util.BadDoubleDeserializer;
import com.infosage.util.Constants;
import com.infosage.util.SessionManager;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
//import android.support.multidex.MultiDex;
import android.text.TextUtils;
import android.util.Log;

/**
 * 
 * @author Govinda P.
 */
public class AppController extends Application {

	public static final boolean DEVELOPER=true;
	public static final String TAG = AppController.class
            .getSimpleName();
 
    private RequestQueue mRequestQueue;
    private static AppController mInstance;
 
    public static SessionManager sessobj,session;
    public static ItemPermissions permissions;
    public static boolean isProxy=false;
    public static boolean isKeystoneAvail=false;
    public static int isKeystoneAvailSize=0;
    public  User mUser=null;
    public  User pUser=null;
    public  User vUser=null;
    static public Gson gson =null;
    private Context mContext=null;
    public static String txthelplist="Help List";
   //static User userTarget=null;
   // CloseableHttpClient httpClient;
    
    // update date 04-nov-2015
    
      private static boolean activityVisible;
      
      public Context getIntanceContext(){
    	  return this.mContext;
      }      
      public void setIntanceContext(Context context){
    	  this.mContext=context;
      }
      
    @Override
    public void onCreate() {
        super.onCreate();
		Log.e("APPLICATION","onCreate");
		VolleyLog.DEBUG=false;
        mInstance = this;
        CookieHandler.setDefault(new CookieManager( null, CookiePolicy.ACCEPT_ALL ) );
        sessobj = new SessionManager(getApplicationContext());
        session = new SessionManager(getApplicationContext());
        gson= new GsonBuilder().registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
        permissions=new ItemPermissions();     
        
    }
 
    /**
     *@use to set proxy user available under login user or not.
     *@param isproxy TRUE if proxy user available under login user. False if no proxy user available.
     *@note This method is different from session.setIsProxy. 
     */
    public static void setProxy(boolean isproxy) {
		isProxy = isproxy;
	}
    /**
     *@use check proxy user user available or not under login user. This is used for show/hide change account menu option.
     *@return isproxy TRUE if proxy user available under login user. False if no proxy user available
     *@note This method is different from session.getIsProxy. 
     */
    public static boolean getProxy() {
		return isProxy;
    	//return getInstance().pUser;
	}
    
    /**
     * @use if Main login user work as proxy then return proxy user that are changed from change account page.
     * Otherwise retrun main user.
     * @return secondary login user.
     */
    public static User getUser()
    {
    	return getInstance().pUser;
    }
    
   /**
    * @use set User in app. and this user means Proxy user. Secondry User act as change account user.
    * @param puser
    * @Note User is second category user in app. This is change if Main login user act as Proxy from change account in app.
    */
    public static void setUser(User puser)
    {
    	getInstance().pUser=puser;
    }
    
    /**
     * @use if login user redirect to any other user like keystone,caregiver,participents etc from home page, notification redirection.
     * That time destination user act as a View User in app.
     * This is imp because Permission check => view user To Login user.
     * @return view user.
     */
    public static User getViewUser()
    {
    	return getInstance().vUser;
    }
    
   
    /**
     * @use Set View user. 
     * if login user redirect to any other user like keystone,caregiver,participents etc from home page, notification redirection.
     * That time destination user act as a View User in app.
     * This is imp because Permission check => view user To Login user.
     * @param puser 
     * @Note This is Third no of user in app. and always change when login user view todo,update,medication section for other user.
     */
    public static void setViewUser(User view_user)
    {
    	getInstance().vUser=view_user;
    }
    
    /**
     * @use use for main login user and check with user profile change or not, And also use in every web call if needed login user id or full json object as body. 
     * @return Main Login user.
     * @Note This is primary user object. Not change in app.
     */
    public static User getMainUser()
    {
    	return getInstance().mUser;
    }
    
    /**
     * @user_category Primary User.Always Same
     * @use use to set main login user in app. 
     * @param Main Login user..i.e fetch at main login time.
     * @Note This is primary user object. Not change in app.
     * and Main user,User,and view user is same at main login time or user is main login user.
     */
    public static void setMainUser(User muser)
    {
    	getInstance().mUser=muser;
    	getInstance().pUser=muser;
    	getInstance().vUser=muser;
    	session.setIsProxy(false);    	
    }
    
    /**
     * 
     * @return Instance of your application file/ app from main controller class
     */
    public static synchronized AppController getInstance() {
        return mInstance;
    }
 
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
        /*	DefaultHttpClient httpclient = new DefaultHttpClient();

        	CookieStore cookieStore = new BasicCookieStore();
        	httpclient.setCookieStore( cookieStore );

        	HttpStack httpStack = new HttpClientStack( httpclient );
        	RequestQueue requestQueue = Volley.newRequestQueue( context, httpStack  );*/
        	
        		 //mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        	 mRequestQueue = Volley.newRequestQueue(getApplicationContext());	
        }
 
        return mRequestQueue;
    }
 
   /* public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }*/
 
    /**
     * 
     * @param req JSON web request
     * @param tag tag for find or delete process from queue
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
    	
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
      
        getRequestQueue().add(req);
    }
 
    /**
     * 
     * @param req JSON web request without tag
     */
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
  
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
    public void cancelAllRequest()
    {
    mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
        @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    
    }
    
    /**
     * @use used to create  and return authorization header.
     * @return main authorization header that required for every web call.
     */
    public static Map<String, String> createBasicAuthHeader() {
        Map<String, String> headerMap = new HashMap<String, String>();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mInstance);
       
        headerMap.put("Authorization", "Basic " + pref.getString("authstr", ""));
        headerMap.put("Content-Type", "application/json");
        headerMap.put("X-Vendor-Id",Constants.vendorID);
        if(sessobj.getIsProxy())
        {
        	System.out.println("if proxy......");
        	headerMap.put("X-Proxy-User",sessobj.getLogUserId());
        }      
        return headerMap;
    }
    
    /**
     * Test for isDeveloper or Not
     * @return
     */
    public static boolean isDeveloper() {
		return DEVELOPER;
	}
    
   /**
    * @use save cookies
    * @param cookie
    */
    public void saveCookie(String cookie) {
    	  if (cookie == null) {
    	    //the server did not return a cookie so we wont have anything to save
    	    return;
    	  }
    	  // Save in the preferences
    	  SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    	  if (null == prefs) {
    	    return;
    	  }
    	  SharedPreferences.Editor editor = prefs.edit();
    	  editor.putString("cookie", cookie);
    	  editor.commit();
    	}
    
    /**
     * @return return already save cookies
     * @param cookie
     */
    public String getCookie()
    {
     SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
     String cookie = prefs.getString("cookie", "");
     if (cookie.contains("expires")) { 
    /** you might need to make sure that your cookie returns expires when its expired. I also noted that cokephp returns deleted */
      removeCookie();
      return "";
     }
    return cookie;
    }

    /**
     * @use remove save cookies 
     */
    public void removeCookie() {
     SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
     SharedPreferences.Editor editor = prefs.edit();
     editor.remove("cookie");
     editor.commit();
    }
    
    
    public static boolean isActivityVisible() {
        return activityVisible;
      }  

      public static void activityResumed() {
        activityVisible = true;
      }

      public static void activityPaused() {
        activityVisible = false;
      }

     
      
     /* (non-Javadoc)
     * @see android.app.Application#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
    	// TODO Auto-generated method stub
    	super.onConfigurationChanged(newConfig);
    	
    }
    
    /* (non-Javadoc)
     * @see android.app.Application#onLowMemory()
     */
    @Override
    public void onLowMemory() {
    	// TODO Auto-generated method stub
    	super.onLowMemory();
    	
    }
     
	 /* (non-Javadoc)
	 * @see android.app.Application#onTerminate()
	 */
	@Override
	public void onTerminate() {
		// TODO Auto-generated method stub
		super.onTerminate();
	
	}   
      
	
}
