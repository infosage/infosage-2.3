package org.infosagehealth.app;

import java.util.ArrayList;
import org.infosagehealth.app.R;
import org.json.JSONException;
import org.json.JSONObject;
import com.squareup.picasso.Picasso;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @use This is a Assign user adapter class. Used to create assign user list and click for assign/unassign task to that user.
 * @author user Govinda P.
 *
 */
public class AssignUserAdapter extends BaseAdapter{

	Context context;
	ArrayList<UserObject> usrobjArr;
	String uid;
	TaskObject taskobj;
	Activity act;
	boolean isClickble;
	/**
	 * 
	 * @param context1 activity context / instance
	 * @param act activity
	 * @param usrobjArr1 user array
	 * @param userid1 your user id
	 * @param taskobj1 task object
	 * @param isClickble is clickble or not
	 */
	public AssignUserAdapter(Context context1, Activity act, ArrayList<UserObject> usrobjArr1, String userid1, TaskObject taskobj1,boolean isClickble)
	{
		context = context1;
		usrobjArr = usrobjArr1;
		uid = userid1;
		taskobj = taskobj1;
		this.act = act;
		this.isClickble=isClickble;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return usrobjArr.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public static class ViewHolder
	{
		Button btnusr, btncal;
		TextView txtname;
		ImageView img;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View v = convertView;
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewHolder holder = null;
		
		UserObject uobj = usrobjArr.get(position);
		try
		{
			if(convertView == null){
				v = inflater.inflate(R.layout.assignuserchild, null);
				
				holder = new ViewHolder();
				holder.txtname = (TextView) v.findViewById(R.id.txtname);
				holder.img = (ImageView)v.findViewById(R.id.profimg);
				
				v.setTag(holder);
			}
			else
			{
				holder = (ViewHolder)v.getTag();
			}
			
			holder.txtname.setText(uobj.getfname()+" "+uobj.getlname());
			
			if(uobj.getimgpath() != null)
			{								
				Picasso.with(context).load(MainActivity.imgbaseurl+"/cache/w40xh40-2/images/"+uobj.getimgpath())
				.placeholder(R.drawable.default_profile).into(holder.img);
			}
			else
			{
				holder.img.setImageResource(R.drawable.default_profile);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		//v.setOnClickListener(new clicklistener(uobj));
		JSONObject asusrobj = taskobj.getAssignedUser();
		try {
			String assignuid = asusrobj.getJSONObject("target").getString("id");
			
			if(assignuid.compareTo(uobj.getId()) == 0){
				v.setBackgroundColor(act.getResources().getColor(R.color.grayclr));
			}
			else{
				v.setBackgroundColor(0);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		if (isClickble) {
			//holder.txtname.setAlpha(0.3f);
		}
		else
		{
			holder.txtname.setAlpha(0.3f);
		}
		
		
		
		return v;
	}
	
	
}
