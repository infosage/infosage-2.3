package org.infosagehealth.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.infosagehealth.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.volley.AuthFailureError;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;
import com.infosage.adapter.ProxyImageAdapter;
import com.infosage.item.ItemPermissions;
import com.infosage.item.ItemPoxy;
import com.infosage.item.User;
import com.infosage.util.Constants;
import com.infosage.util.MyFunction;
import com.infosage.util.SessionManager;
import com.infosage.view.CustomAnimatedProgress;
import com.squareup.picasso.Picasso;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * @use This is Change Account fragment class. Used to create a Change Account module in UserTask.class Activity.
 * @author user Govinda P.
 *
 */
public class ChangeAccount extends Fragment{

	String userid, path;
	GridView gridview;
	ImageView selfuser;
	TextView textView3;
	ArrayList<ItemPoxy> userobjlist = new ArrayList<ItemPoxy>();
	DrawerLayout mDrawerLayout;
	Button menu_bt, btnnotif, btnhide;
	ListView mDrawerList,listnotif;
	Context context;
	View v=null;
	public ChangeAccount() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		/**
		 * Used to hide user profile section .i.e. same as a iOS.
		 */
		try {
			if (AppController.getMainUser()==null) {
				getActivity().finish();
				return;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		UserTasks.lay_profile.setVisibility(View.GONE);
		super.onResume();
		
	}
	
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		/**
		 * Used to show user profile section on close change account page.i.e. same as a iOS
		 */
		UserTasks.lay_profile.setVisibility(View.VISIBLE);
		super.onStop();
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (UserTasks.lay_profile!=null) {
		UserTasks.lay_profile.setVisibility(View.GONE);
		}
			v=inflater.inflate(R.layout.changeaccount, container, false);
			//}
		//setContentView(R.layout.changeaccount);
		selfuser = (ImageView)v.findViewById(R.id.selfuser);
		selfuser.setBackgroundResource(R.drawable.default_profile);
		textView3=(TextView)v.findViewById(R.id.textView3);
		textView3.setText("");
		
		return v;
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onStart()
	 */
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		context =getActivity();
		
		JSONObject loginobj = MainActivity.loginuserobj;
		if(!loginobj.isNull("profileImage"))
		{
			String profimg;			
			try {
				profimg = loginobj.getJSONObject("profileImage").getJSONObject("image").getString("path");
				
				Picasso.with(getActivity()).load(MainActivity.imgbaseurl+"/cache/w100xh100-2/images/"+profimg)
				.placeholder(R.drawable.default_profile)
				.into(selfuser);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		textView3.setText(""+AppController.getMainUser().getFirstName()+" "+AppController.getMainUser().getLastName());
		/*SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		userid = pref.getString("loginuserid", "");*/
		
		userid=String.valueOf(AppController.getMainUser().getId());
		//String url = MainActivity.webserviceurl+"/users/"+userid+"/privileges/PROXY/outgoing";
		getProxy(""+userid);
		path = MainActivity.imgbaseurl+"/cache/w100xh100-2/images/";
		
		selfuser.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				/*
				 * 
				 */
				AppController.permissions.setAllEnable();
				AppController.setUser(AppController.getMainUser());
				AppController.setViewUser(AppController.getMainUser());
				SessionManager sessobj = new SessionManager(getActivity());
				sessobj.setIsProxy(false);
				sessobj.setLogUserId(""+AppController.getMainUser().getId());
				
				if (AppController.getMainUser().getIsElder()) {
					if (AppController.isKeystoneAvailSize>0) {
						Intent intent=new Intent(getActivity(),HomeNew.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
						getActivity().finish();
						
					}
					else
					{
						Intent intent = new Intent(getActivity(), UserTasks.class);
						Bundle b = new Bundle();
						b.putString("flagact", "");
						b.putInt("flag",3);
						intent.putExtras(b);
						startActivity(intent);
						getActivity().finish();
						AppController.permissions.setAllEnable();
					}
				}
				else
				{
					//getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE); 
					//Bundle b2 = new Bundle();
					if (userobjlist.size()>0) {
					if (AppController.isKeystoneAvailSize==1) {
						
						SessionManager sesobj = new SessionManager(getActivity());
						sesobj.setMyUserid(""+userobjlist.get(0).getId());
						sesobj.setIsProxy(false);
						
						final Intent intent = new Intent(getActivity(), UserTasks.class);
						final Bundle b = new Bundle();
						b.putString("target",AppController.gson.toJson(userobjlist.get(0)));
					
						try {
							//UserTasks.userobject=userobjlist.get(id);
							String user=AppController.gson.toJson(userobjlist.get(0));
							final User selectiuser = AppController.gson.fromJson(user,User.class);
							AppController.setViewUser(selectiuser);
							//Listener<JSONObject> resp = null;
							final CustomAnimatedProgress progress=new CustomAnimatedProgress(getActivity());
							UserPermissionsSetup.getPermissionByResponse(progress,""+selectiuser.getId(),""+AppController.getMainUser().getId(), new Listener<JSONObject>() {

								@Override
								public void onResponse(JSONObject arg0) {
									// TODO Auto-generated method stub
									AppController.permissions=AppController.gson.fromJson(arg0.toString(), ItemPermissions.class);
									AppController.permissions.setLoguserid(""+AppController.getMainUser().getId());
									AppController.permissions.setSelectid(""+selectiuser.getId());
									intent.putExtras(b);
									progress.dismiss();
									getActivity().startActivity(intent);
									getActivity().finish();
								}
							});
						}
						catch(Exception e)
						{
							e.printStackTrace();
						}
					}else
					{
					
					Intent intent=new Intent(getActivity(),HomeNew.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					getActivity().finish();
					}
					}
				}				
			}
		});		
	}
	
	/**
	 * @use get PROXY users and add in gridview adapter setting on success response.
	 * @param userid
	 */
	private void getProxy(String userid)
	{
		String url = Constants.HOST_URL+"/users/"+userid+"/privileges/PROXY/outgoing";
		JsonArrayRequest req = new JsonArrayRequest(url, new Listener<JSONArray>() 
				{
			@Override
			public void onResponse(JSONArray arg0) {
				Log.e("User Proxy", " "+arg0.toString());
				
				try {
					List<ItemPoxy> item = Arrays.asList(AppController.gson.fromJson(arg0.toString(), ItemPoxy[].class));
					
				if (item.size()>0) 
				{
					AppController.setProxy(true);
				}
				else
				{
					AppController.setProxy(false);
					
					new AlertDialog.Builder(getActivity())
					//.setTitle("Need to Relogin !!")
					.setMessage(R.string.msg_pri_change)
					.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(context,MainActivity.class);
    						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
    						context.startActivity(intent);
						}
					}).setCancelable(false).create().show();
				}
				userobjlist.clear();
				userobjlist.addAll(item);
				gridview = (GridView)getView().findViewById(R.id.gridView1);
				
				gridview.setAdapter(new ProxyImageAdapter(getActivity(),getActivity(),userobjlist));
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				AppController.setProxy(false);
				e.printStackTrace();
			}
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				MyFunction.CheckError(getActivity(), arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	            	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	//Log.e("Encoding"," "+getParamsEncoding());
	            	return super.getParamsEncoding();
	            }
	            
	        };		
	      AppController.getInstance().addToRequestQueue(req, "PROXY");
	}
	
}
