package org.infosagehealth.app;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.infosagehealth.app.R;
import org.infosagehealth.app.EditMedicationsFragment.addMedicationCallback;
import org.infosagehealth.app.MedicationsFragment.EditMedicationCallback;
import org.infosagehealth.app.ToDoListFragment.ToDoListCallback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.infosage.adapter.MenuAdapter;
import com.infosage.adapter.NotificationAdapter;
import com.infosage.adapter.PhotoUpdateListener;
import com.infosage.item.ItemKeystone;
import com.infosage.item.ItemMeditation;
import com.infosage.item.ItemNotification;
import com.infosage.item.ItemPermissions;
import com.infosage.item.ItemPoxy;
import com.infosage.item.NavDrawerItem;
import com.infosage.item.Notification;
import com.infosage.item.Owner;
import com.infosage.item.Target;
import com.infosage.item.User;
import com.infosage.pushservice.QuickstartPreferences;
import com.infosage.util.Constants;
import com.infosage.util.FindRedirectPage;
import com.infosage.util.MyFunction;
import com.infosage.util.SessionManager;
import com.infosage.view.BadgeView;
import com.infosage.view.CustomAnimatedProgress;
import com.squareup.picasso.Picasso;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class UserTasks extends HideKey implements EditMedicationCallback, addMedicationCallback,ToDoListCallback,PhotoUpdateListener{

	Button btntodo, btnupdate;
	FragmentTransaction ft;
	FragmentManager fm ;
	LinearLayout todolay,updatelay,lay_noti,lay_btn_noti;
	RelativeLayout todotriangle,uptriangle,laynotif,mDrawerSlide;
	
	DrawerLayout mDrawerLayout;
	
	ArrayList<NavDrawerItem> mSliderItem=new ArrayList<NavDrawerItem>();
	MenuAdapter menuAdapter;
	Button menu_bt, btnnotif, btnhide;
	ListView mDrawerList,listnotif;
	Context context;
	
	NotificationAdapter adpt;  
	int flag;
	String fname, lname, imgpath;
	TextView username;
	ImageView userpic;
	
	public static User userTarget=null;
	String userid;	
	SessionManager sessobj;	
	ItemNotification list_notification=new ItemNotification();
	BadgeView badge;
	ArrayList<ItemKeystone> userobjlist=new ArrayList<ItemKeystone>();
	public static RelativeLayout lay_profile=null;
	boolean doubleBackToExitPressedOnce = false;
	CustomAnimatedProgress progress=null;	
	Fragment fragment = null;
	
	//String txthelplist="Help List";
	
	@Override
	public void onBackPressed()
	{
	    if (mDrawerLayout.isDrawerOpen(Gravity.START)){
	        mDrawerLayout.closeDrawer(Gravity.START);
	    }
	    else{
	    	 mDrawerLayout.openDrawer(Gravity.START);
	    	} 
	  }
	
	/* (non-Javadoc)
	 * @see android.support.v7.app.AppCompatActivity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onSaveInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putAll(getIntent().getExtras());
		
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onRestoreInstanceState(android.os.Bundle)
	 */
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/**
		 * Check login user is null or not if null then again back to login window
		 */
		if (AppController.getMainUser()==null) {
			getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			Intent intent=new Intent(getApplicationContext(), MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
			return;
		}else{
		
		AppController.getInstance().setIntanceContext(getApplicationContext());
		setContentView(R.layout.usertasks);
		
		//set help list text based on user role
		sessobj = new SessionManager(getApplicationContext());
	
			
		if(!sessobj.getIsProxy() && AppController.permissions.getLoguserid().compareTo(AppController.permissions.getSelectid()) != 0){
			
			AppController.txthelplist = "Can You Help With This?";
		}else{
			AppController.txthelplist="Help List";
		}
		
		//------drawer code-------
				menu_bt=(Button)findViewById(R.id.menu_bt);
				mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			
				mDrawerList = (ListView) findViewById(R.id.left_drawer);				
				lay_profile=(RelativeLayout)findViewById(R.id.rel1);
				lay_btn_noti=(LinearLayout)findViewById(R.id.lay_noti_btn);
				lay_btn_noti.setClickable(true);
				lay_profile.setVisibility(View.VISIBLE);
				View header = getLayoutInflater().inflate(R.layout.header, null);
				header.setPadding(5, 5, 5, 5);
				mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
				int[] colors = {0, Color.GREEN, 0}; // red for the example
				mDrawerList.setDivider(new GradientDrawable(Orientation.RIGHT_LEFT, colors));
				mDrawerList.setDividerHeight(2);
				mDrawerList.addHeaderView(header);
				mDrawerList.setPadding(0, 0, 0, 20);
				setdrawer();
				context = this.getApplicationContext();
				progress=new CustomAnimatedProgress(getBaseContext());
				progress.setCanceledOnTouchOutside(false);
				progress.dismiss();
							
				//--------******----------
				
				mDrawerLayout.setOnTouchListener(new View.OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// TODO Auto-generated method stub
						try {
							hideSoftKeyboard(UserTasks.this);
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						return false;
					}
				});
				  
				
				mDrawerLayout.setDrawerListener(new DrawerListener() {
					
					@Override
					public void onDrawerStateChanged(int arg0) {
						// TODO Auto-generated method stub
					}
					
					@Override
					public void onDrawerSlide(View arg0, float arg1) {
						// TODO Auto-generated method stub
					}
					
					@Override
					public void onDrawerOpened(View arg0) {
						// TODO Auto-generated method stub
						UserPermissionsSetup.getProxy(""+AppController.getMainUser().getId(),new Listener<JSONArray>() {

							@Override
							public void onResponse(JSONArray arg0) {
								// TODO Auto-generated method stub
								try {
									ItemPoxy[] item = AppController.gson.fromJson(arg0.toString(), ItemPoxy[].class);
								if (item.length>0) {
									AppController.setProxy(true);
								}
								else
								{
									AppController.setProxy(false);
								}
								setdrawer();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								AppController.setProxy(false);
								e.printStackTrace();
								setdrawer();
							}
							}
						});
						try {
							hideSoftKeyboard(UserTasks.this);
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						laynotif.setVisibility(View.GONE);
						if (adpt!=null) {
							adpt.notifyDataSetChanged();
						}
						
						if(!sessobj.getIsProxy()){
							String url = MainActivity.webserviceurl+"/users/"+AppController.getMainUser().getId()+"/connections?subscribed=true";
							UserPermissionsSetup.getKeystone(url,new Listener<JSONArray>() {

								@Override
								public void onResponse(JSONArray arg0) {
									// TODO Auto-generated method stub
									List<ItemKeystone> templist = Arrays.asList(AppController.gson.fromJson(arg0.toString(), ItemKeystone[].class));
									AppController.isKeystoneAvailSize=templist.size();
									if(AppController.isKeystoneAvailSize>=1)
									{
										AppController.isKeystoneAvail=true;
									}else
									{
										if(!AppController.getMainUser().getIsElder())
										{												
											new AlertDialog.Builder(UserTasks.this)
											.setMessage(R.string.msg_pri_change)
											.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
												
												@Override
												public void onClick(DialogInterface dialog, int which) {
													// TODO Auto-generated method stub
													Intent intent = new Intent(getApplicationContext(),MainActivity.class);
						    						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
						    						startActivity(intent);
												}
											}).setCancelable(false).create().show();
										}
									}
									setdrawer();
								}
							});
						}
						
						UserPermissionsSetup.getPer(""+AppController.getViewUser().getId(),""+AppController.getUser().getId(),new Listener<JSONObject>() {
				
							@Override
							public void onResponse(JSONObject arg0) {
								// TODO Auto-generated method stub
								AppController.permissions=AppController.gson.fromJson(arg0.toString(), ItemPermissions.class);
								AppController.permissions.setLoguserid(""+AppController.getUser().getId());
								AppController.permissions.setSelectid(""+AppController.getViewUser().getId());
								setdrawer();
							}
						});
						
						setdrawer();
						getUserProfile();
						
					}
					
					@Override
					public void onDrawerClosed(View arg0) {
						// TODO Auto-generated method stub
					}
				});
								
				lay_btn_noti.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						laynotif.setVisibility(View.GONE);
						if (adpt!=null) {
							adpt.notifyDataSetChanged();
						}
					}
				});
				
			    menu_bt.setOnClickListener(new View.OnClickListener() 
			    {
						@Override
						public void onClick(View v){
							mDrawerLayout.openDrawer(mDrawerList);
						}
				});
			
				username = (TextView)findViewById(R.id.username);
				userpic = (ImageView)findViewById(R.id.userpic);
				lay_noti=(LinearLayout)findViewById(R.id.lay_noti);
				btnnotif = (Button)findViewById(R.id.btnnotif);				
				listnotif = (ListView)findViewById(R.id.listnotif);
				listnotif.setBackgroundResource(R.drawable.txtbg);
				listnotif.setSelector(android.R.color.transparent);
				laynotif = (RelativeLayout)findViewById(R.id.laynotif);
				btnhide = (Button)findViewById(R.id.btnhide);
				
				btnhide.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						laynotif.setVisibility(View.GONE);
						adpt.notifyDataSetChanged();
						if (list_notification.getNumUnresolved()<1) {
							badge.setText(""+list_notification.getNumUnresolved());
							badge.show();
						}
						else
						{
							list_notification.setNumUnresolved(0);
							badge.hide();
						}
					}
				});
				
				/**
				 * Get and set Notification views
				 */
				badge = new BadgeView(this, lay_noti);
				//badge.setTextSize(18.0f);
				badge.setBadgeMargin(0,0);
				badge.setTextSize(TypedValue.COMPLEX_UNIT_PX,
					    getResources().getDimension(R.dimen.badge_size));
				badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
				badge.hide();
				
				listnotif.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						
						Notification item_noti = list_notification.getNotifications().get(position);
						if (!item_noti.isResolved()) {
							setRead(position,""+item_noti.getId(),false);
						}				
						final String[] link=FindRedirectPage.getLink(item_noti.getText(),
								item_noti.getForwardLink());
						
						String noti_user=link[1];
						userTarget=null;
						if (userobjlist.size()>0) {
							
						for (ItemKeystone usKeystone:userobjlist)
						{
							if (String.valueOf(usKeystone.getTarget().getId()).equals(noti_user)) 
							{
								String user=AppController.gson.toJson(usKeystone.getTarget());
								userTarget= AppController.gson.fromJson(user,User.class);
								break;
							}
						}
						}
						if (userTarget==null) 
						{
					
							laynotif.setVisibility(View.GONE);
							Toast.makeText(getApplicationContext(),"You are not connected to this user.", 2000).show();
						}
						else
						{
							AppController.session.setMyUserid(""+userTarget.getId());
							try {
									if (userTarget.getId()==AppController.getMainUser().getId() && link[2].equals("profile") && userobjlist.size()>1){
										if (!AppController.getUser().getIsElder()) {
											
										if (AppController.isKeystoneAvail) {
											if (AppController.isKeystoneAvailSize>1) {
												progress=null;
												laynotif.setVisibility(View.GONE);
												getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
												startActivity(new Intent(getApplicationContext(), HomeNew.class));
												finish();
												return;
											}
											else
											{
												String user=AppController.gson.toJson(userobjlist.get(1).getTarget());
												userTarget= AppController.gson.fromJson(user,User.class);
												laynotif.setVisibility(View.GONE);
											}
										}	
										}
									}
									else
									{
										if (!AppController.getUser().getIsElder() && userTarget.getId()==AppController.getUser().getId()) {
											
											if (AppController.isKeystoneAvail) {
												if (AppController.isKeystoneAvailSize>1) {
													progress=null;
													laynotif.setVisibility(View.GONE);
													getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
													startActivity(new Intent(getApplicationContext(), HomeNew.class));
													finish();
													return;
												}
												else
												{
													laynotif.setVisibility(View.GONE);
													String user=AppController.gson.toJson(userobjlist.get(1).getTarget());
													userTarget= AppController.gson.fromJson(user,User.class);
													link[0]="todo";
												}
											}	

											}else
											{
												// Other User
												
											}
									}
							}catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							
							
							AppController.session.setMyUserid(""+userTarget.getId());
													
						try {	
								AppController.setViewUser(userTarget);
							
								if (link[0].equals("medication")) {
									
							UserPermissionsSetup.getPer(""+userTarget.getId(),""+AppController.getUser().getId(),new Listener<JSONObject>() {

								@Override
								public void onResponse(JSONObject arg0) {
									// TODO Auto-generated method stub
									try {
										//progress.dismiss();
										laynotif.setVisibility(View.GONE);
									AppController.permissions=AppController.gson.fromJson(arg0.toString(), ItemPermissions.class);
									AppController.permissions.setLoguserid(""+AppController.getUser().getId());
									AppController.permissions.setSelectid(""+userTarget.getId());
									
									if (link[0].equals("medication")) {
										if (AppController.permissions.getSelectid().equals(""+userTarget.getId())
												&& AppController.permissions.isVIEWMEDICATIONS()
												) {								
											fm = getSupportFragmentManager();
											ft = fm.beginTransaction().setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out, R.anim.abc_fade_in, R.anim.abc_fade_out);
											ft.replace(R.id.placeholder, new MedicationsFragment(), "MedFrag");
											ft.commit();
											}
											else
											{	fm = getSupportFragmentManager();
												ft = fm.beginTransaction().setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out, R.anim.abc_fade_in, R.anim.abc_fade_out);
												ToDoListFragment todofrag = new ToDoListFragment();
												ft.replace(R.id.placeholder, todofrag);
												ft.commit();
											}
									}
									if (link[0].equals("update")) {
										
											fm = getSupportFragmentManager();
											ft = fm.beginTransaction().setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out, R.anim.abc_fade_in, R.anim.abc_fade_out);
											ft.replace(R.id.placeholder, new UpdatesFragment());
											ft.commit();
										}
										
										if (link[0].equals("todo")) {
										
											fm = getSupportFragmentManager();
											ft = fm.beginTransaction().setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out, R.anim.abc_fade_in, R.anim.abc_fade_out);
											ToDoListFragment todofrag = new ToDoListFragment();
											ft.replace(R.id.placeholder, todofrag);
											ft.commit();
										}
									} catch (Exception e) {
										// TODO: handle exception
										e.printStackTrace();
									}
								}
								});	
							}
							else
							{
								UserPermissionsSetup.getPer(""+userTarget.getId(),""+AppController.getUser().getId(), new Listener<JSONObject>() {

									@Override
									public void onResponse(JSONObject arg0) {
										// TODO Auto-generated method stub
										
										laynotif.setVisibility(View.GONE);
										AppController.permissions=AppController.gson.fromJson(arg0.toString(), ItemPermissions.class);
										AppController.permissions.setLoguserid(""+AppController.getUser().getId());
										AppController.permissions.setSelectid(""+userTarget.getId());
										
										if (link[0].equals("update")) {
										
												fm = getSupportFragmentManager();
												ft = fm.beginTransaction().setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out, R.anim.abc_fade_in, R.anim.abc_fade_out);
												ft.replace(R.id.placeholder, new UpdatesFragment());
												ft.commit();
											}
											
											if (link[0].equals("todo")) {
											
												fm = getSupportFragmentManager();
												ft = fm.beginTransaction().setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out, R.anim.abc_fade_in, R.anim.abc_fade_out);
												ToDoListFragment todofrag = new ToDoListFragment();
											
												ft.replace(R.id.placeholder, todofrag);
												ft.commit();
											}
												if(link[0].equals("medication"))
												{
											
											}
									}
								});
							}
								
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							
							/*
							 * Main view user setup
							 */
							fname =userTarget.getFirstName();
							
							lname = userTarget.getLastName();
							username.setText(fname+" "+lname);
							if (userTarget.getProfileImage()!=null) {
								imgpath =userTarget.getProfileImage().getImage().getPath();	
								Picasso.with(getApplicationContext())
								.load(MainActivity.imgbaseurl+"/cache/w120xh120-2/images/"+imgpath)
								.placeholder(R.drawable.default_profile)
								.into(userpic);
							}else
							{
								imgpath ="";
								Picasso.with(getApplicationContext())
								.load(MainActivity.imgbaseurl+"/cache/w120xh120-2/images/"+imgpath)
								.placeholder(R.drawable.default_profile)
								.into(userpic);
							}
							
							try
							{
								ToDoListFragment.userid =""+userTarget.getId();
							}catch(Exception e)
							{
								e.printStackTrace();
							}
						}
					}
				});				
				
				btnnotif.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (list_notification.getNotifications().size()==0 || list_notification.getNotifications()==null) {
							Toast.makeText(getApplicationContext(), "You have no notifications!", 2000).show();
						}
						else
						{
							if(laynotif.getVisibility()==View.VISIBLE){
								laynotif.setVisibility(View.GONE);
								adpt.notifyDataSetChanged();
								if (list_notification.getNumUnresolved()<1) {
									badge.setText(""+list_notification.getNumUnresolved());
									badge.show();
								}
								else
								{
									badge.hide();
								}
							}else{
								laynotif.setVisibility(View.VISIBLE);
								badge.hide();
										for (int i = 0;i<list_notification.getNotifications().size();i++) {
											Notification item_noti = list_notification.getNotifications().get(i);
											if (!item_noti.isResolved()) {
												setRead(i,""+item_noti.getId(),true);
											}
										}
								}
							}
						}
				});

				sessobj = new SessionManager(getApplicationContext());				
				
				if(getIntent().getExtras() != null)
				{
					Bundle b = getIntent().getExtras();
					
					
					flag = b.getInt("flag", 3);
					User fetchuser = AppController.gson.fromJson(b.getString("target"), User.class);
					
					if (fetchuser!=null) {
						userTarget=AppController.gson.fromJson(b.getString("target"), User.class);
					}
					else {
						userTarget=AppController.getViewUser();
					}
					if (userTarget==null) {
						finish();
					}								
					try
					{
						fname =userTarget.getFirstName();						
						lname = userTarget.getLastName();
						username.setText(fname+" "+lname);
						
						if (userTarget.getProfileImage()!=null) {
							imgpath =userTarget.getProfileImage().getImage().getPath();	
							Picasso.with(getApplicationContext())
							.load(MainActivity.imgbaseurl+"/cache/w120xh120-2/images/"+imgpath)
							.placeholder(R.drawable.default_profile)
							.into(userpic);
						} else {
							imgpath ="";
							Picasso.with(getApplicationContext())
							.load(MainActivity.imgbaseurl+"/cache/w120xh120-2/images/"+imgpath)
							.placeholder(R.drawable.default_profile)
							.into(userpic);
						}
						
						ToDoListFragment.userid =""+userTarget.getId();
						}catch(Exception e)
						{
							e.printStackTrace();
						}
					
					if(flag == 3)
					{
						fm = getSupportFragmentManager();
						ft = fm.beginTransaction();
						ToDoListFragment todofrag = new ToDoListFragment();
						todofrag.setArguments(b);
						ft.replace(R.id.placeholder, todofrag);
						ft.commit();
					}
					else if(flag == 4)
					{
						fm = getSupportFragmentManager();
						ft = fm.beginTransaction();
						ft.replace(R.id.placeholder, new UpdatesFragment());
						ft.commit();
					}
					else if(flag == 5)
					{
						fm = getSupportFragmentManager();
						ft = fm.beginTransaction();
						ft.replace(R.id.placeholder, new MedicationsFragment(), "MedFrag");
						ft.commit();
					}
				}
				else
				{
				/*	fm = getSupportFragmentManager();
					ft = fm.beginTransaction();
					ft.replace(R.id.placeholder, new ToDoListFragment(), "TodoFrag");
					ft.commit();*/
				}
			
				/**
				 * Get / Check proxy or not
				 */
				try {
					UserPermissionsSetup.getProxy(""+AppController.getMainUser().getId());	
					String url = MainActivity.webserviceurl+"/users/"+AppController.getMainUser().getId()+"/connections"; //?subscribed=true
					getKeystone(url,""+AppController.getMainUser().getId());
				} catch (Exception e) {
					// TODO: handle exception
					finish();
				}
		}
		
		
		/*Intent temp=new Intent(getApplicationContext(), SearchActivity.class);
		startActivity(temp);
		*/
			}
	
			@Override
			protected void onResume() {
				// TODO Auto-generated method stub
				super.onResume();		
				// Fetch notification 
				getNotification(Constants.getURL_Notification_Fetch(AppController.session.getLogUserId(),""), "", "", "");
				
				//for test
				getNotification(Constants.getURL_Notification_Fetch(AppController.session.getLogUserId(),""), "", "", "");
				
				AppController.activityResumed();
				NotificationManager notifManager= (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
		  	  	notifManager.cancelAll();
				
				/**
				 * @use in app push notification registration. and unregister if page close. this method in onPause()
				 */
				try {					
			        LocalBroadcastManager.getInstance(UserTasks.this).registerReceiver(mRegistrationBroadcastReceiver,
			                new IntentFilter(QuickstartPreferences.PUSH_RECIVER));
			        
				} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
				}
			}
			
			private BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
		        @Override
		        public void onReceive(Context context, Intent intent) {
		            SharedPreferences sharedPreferences =
		                    PreferenceManager.getDefaultSharedPreferences(context);
		          
		          //AppController.getInstance().cancelPendingRequests("noti");
		          //  getNotification(Constants.getURL_Notification_Fetch(AppController.session.getLogUserId(),""), "", "", "");
		          Message message = mHandler_noti.obtainMessage();
		            message.sendToTarget();
		        }
		    };
			/* (non-Javadoc)
			 * @see android.support.v4.app.FragmentActivity#onPause()
			 */
			@Override
			protected void onPause() {
				// TODO Auto-generated method stub				
				super.onPause();
				
				AppController.getInstance().cancelPendingRequests("user");
				LocalBroadcastManager.getInstance(UserTasks.this).unregisterReceiver(mRegistrationBroadcastReceiver);
				AppController.activityPaused();
				
			}
			
			/* (non-Javadoc)
			 * @see android.support.v7.app.AppCompatActivity#onStop()
			 */
			@Override
			protected void onStop() {
				// TODO Auto-generated method stub
				if (progress!=null && progress.isShowing()) {
					progress.dismiss();
				}
				super.onStop();
			}
			
			@Override
			protected void onActivityResult(int requestCode, int resultCode, Intent data) {
				// TODO Auto-generated method stub
				super.onActivityResult(requestCode, resultCode, data);	
				try {
					if(data!=null && data.getExtras() != null){
						Bundle b = data.getExtras();
						int key = b.getInt("key");
					
						fm = getSupportFragmentManager();
						ft = fm.beginTransaction();
						ft.replace(R.id.placeholder, new ToDoListFragment(), "TodoFrag");
						ft.commit();
					}
				} catch (Exception e) {
					// TODO: handle exception
			
				}
				
			}

			@Override
			public void showEditFragment() {
				// TODO Auto-generated method stub
				
				fm = getSupportFragmentManager();
				ft = fm.beginTransaction();
				ft.replace(R.id.placeholder, new EditMedicationsFragment(), "EditMedFrag");
				ft.commit();				
			}

			@Override
			public void showaddmedication() {
				// TODO Auto-generated method stub
				fm = getSupportFragmentManager();
				ft = fm.beginTransaction();
				ft.addToBackStack("add");
				ft.replace(R.id.placeholder, new AddMedicationsFragment(), "AddMedFrag");
				ft.commit();
			}

			
			@Override
			public void showEditmedication(ItemMeditation itemMeditation) {
				// TODO Auto-generated method stub
				Bundle bundle = new Bundle();
				bundle.putString("itemJson", ""+new Gson().toJson(itemMeditation).toString());
				
				AddMedicationsFragment fragment_one = new AddMedicationsFragment();
				fragment_one.setArguments(bundle);
				fm = getSupportFragmentManager();
				ft = fm.beginTransaction();
				ft.addToBackStack("edit");
				ft.replace(R.id.placeholder, fragment_one, "editMedFrag");
				ft.commit();				
			}
			
			
			
			/**
			 * 
			 * @param url notification url
			 * @param userid login userid
			 * @param beforeid 
			 * @param count
			 */
			public void getNotification(String url,String userid,String beforeid,String count)
			{
					
				JSONObject jsonBody=new JSONObject();
				try {
				if (!beforeid.equals("")) {
				
						jsonBody.put("beforeId", beforeid);
					
				}
				if (!count.equals("")) {
					jsonBody.put("count", count);
				}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
				}
				JsonObjectRequest req = new JsonObjectRequest(Method.GET,url,jsonBody, new Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject arg0) {
						
					try {
						list_notification= AppController.gson.fromJson(arg0.toString(),ItemNotification.class);
						
						List<Notification> listnot = new ArrayList<Notification>();
						Notification not = new Notification();
						not.setText("Test");
						listnot.add(not);
						listnot.add(not);
						
						
						adpt = new NotificationAdapter(getApplicationContext(),list_notification.getNotifications());
						//adpt = new NotificationAdapter(getApplicationContext(),listnot);
						listnotif.setAdapter(adpt);
						try 
						{
							if (list_notification.getNumUnresolved()>0) {
								badge.setText(list_notification.getNumUnresolved()+"");
								badge.show(true);
							}
							else
							{
								badge.hide();
							}
						} catch (Exception e) {
							// TODO: handle exception
						
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						}
					
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						 MyFunction.CheckError(getApplicationContext(), arg0);
					}
				}) {
					
			           @Override
			        protected VolleyError parseNetworkError(
			        		VolleyError volleyError) 
			           {
			        	// TODO Auto-generated method stub			       	            	
			        	return MyFunction.parseNetworkErrorMy(volleyError);
			        }
			           
			           @Override
			        public Map<String, String> getHeaders() throws AuthFailureError {
			        	// TODO Auto-generated method stub
			        	return AppController.createBasicAuthHeader();
			        }
			         
			            @Override
			            protected String getParamsEncoding() {
			            	// TODO Auto-generated method stub
			            	return super.getParamsEncoding();
			            }	            
			        };		
			       
			        AppController.getInstance().getRequestQueue().getCache().remove(url);
			        AppController.getInstance().getRequestQueue().getCache().clear();
			       /* req.setRetryPolicy(new DefaultRetryPolicy(5000,0,1));
					int socketTimeout = 30000;//30 seconds - change to what you want
					RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
					req.setRetryPolicy(policy);
					req.setShouldCache(false);*/
					AppController.getInstance().addToRequestQueue(req);
			      AppController.getInstance().addToRequestQueue(req, "noti");
			}

			/**
			 * 
			 * @param pos selected position from notification listview.
			 * @param noti_id notification id for mark as read
			 * @param all
			 */
			public void setRead(final int pos, String noti_id,final boolean all) {
				// TODO Auto-generated method stub
				// Tag used to cancel the request
						String tag_json_obj = "readnoti";
				
					   
						String url=Constants.getURL_Notification_Read(AppController.session.getLogUserId(), noti_id);
						//JSONObject jsonBody = MyFunction.getJsonBody(keys, values);
						JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.PUT,
				                url,
				                new Response.Listener<JSONObject>() {
				 
				                    @Override
				                    public void onResponse(JSONObject response) {
				                    	
				                    	Notification object=AppController.gson.fromJson(response.toString(), Notification.class);
										
				                    	list_notification.getNotifications().set(pos, object);
				                    	
				                    	if (all) {
				                    		
										}
				                    	else{
				                    	try {
				                    		long gp=list_notification.getNumUnresolved()-1;
				                    		
				                    		if (gp>0) {
				                    			badge.setText(gp+"");
												badge.show(true);
											}
				                    		else{
				                    			badge.setText(gp+"");
												badge.hide();
				                    		}
				                    		adpt.notifyDataSetChanged();
										} catch (Exception e) {
											// TODO: handle exception
											e.printStackTrace();
											badge.hide();
										}
				                    	
				                    	}
				                    	
				                    }
				                },new ErrorListener() {

									@Override
									public void onErrorResponse(VolleyError arg0) {
										// TODO Auto-generated method stub
										 MyFunction.CheckError(getApplicationContext(), arg0);	
									}
								}){
						           @Override
						        protected VolleyError parseNetworkError(
						        		VolleyError volleyError) {
						        	// TODO Auto-generated method stub
						        	    	
						        	return MyFunction.parseNetworkErrorMy(volleyError);
						        }
						           @Override
						        	protected Response<JSONObject> parseNetworkResponse(
						        			NetworkResponse response) {
						        		// TODO Auto-generated method stub
						        	   try {
						        		   
									} catch (Exception e) {
										// TODO: handle exception
									}
						        	return super.parseNetworkResponse(response);
						        	}
						            /**
						             * Passing some request headers
						             * */
						           @Override
							        public Map<String, String> getHeaders() throws AuthFailureError {
							        	// TODO Auto-generated method stub
							        	return AppController.createBasicAuthHeader();
							        }
						        };						 
						// Adding request to request queue
						AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
			}
			
			/**
			 * @use fetch keystone for user
			 * @param url get keystone url
			 * @param userid
			 */
			public void getKeystone(String url,String userid)
			{
				
				JsonArrayRequest req = new JsonArrayRequest(url, new Listener<JSONArray>() {

					@Override
					public void onResponse(JSONArray arg0) {
					
						List<ItemKeystone> templist = Arrays.asList(AppController.gson.fromJson(arg0.toString(), ItemKeystone[].class));
						userobjlist.clear();
					
							String user=AppController.gson.toJson(AppController.getUser());
					
							userobjlist.add(new ItemKeystone(AppController.gson.fromJson(user,Owner.class),
									AppController.gson.fromJson(user,Target.class)));
						userobjlist.addAll(templist);
						setdrawer();
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						 MyFunction.CheckError(getApplicationContext(), arg0);
					}
				}) {
					
			           @Override
			        protected VolleyError parseNetworkError(
			        		VolleyError volleyError) 
			           {
			        	// TODO Auto-generated method stub			                  	
			        	return MyFunction.parseNetworkErrorMy(volleyError);
			        }
			           
			           @Override
			        public Map<String, String> getHeaders() throws AuthFailureError {
			        	// TODO Auto-generated method stub
			        	return AppController.createBasicAuthHeader();
			        }
			         
			            @Override
			            protected String getParamsEncoding() {
			            	// TODO Auto-generated method stub
			            	return super.getParamsEncoding();
			            }
			            
			        };		
			      AppController.getInstance().addToRequestQueue(req, "fetch");
			}
			
			/**
			 * Slide menu item click listener
			 * */
			private class SlideMenuClickListener implements
					ListView.OnItemClickListener {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position,
						long id) {
					// display view for selected nav drawer item
					if (position==0) {
						mDrawerLayout.closeDrawer(mDrawerList);
					}else
					{
						displayView(position=position-1);
					}
				}
			}
			
			/**
			 * Diplaying fragment view for selected nav drawer list item
			 * */
			private void displayView(int position) {
				// update the main content by replacing fragments
				
				String stack=null;
				lay_profile.setVisibility(View.VISIBLE);
				if (mSliderItem.get(position).getTitle().equals("Home")) {
					getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE); 
					
					Intent intent=new Intent(getApplicationContext(),HomeNew.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					finish();
				}else
				if (mSliderItem.get(position).getTitle().equals("Change Account")) {
					//lay_profile.setVisibility(View.GONE);
					fragment=new ChangeAccount();
					getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
				}else
				if (mSliderItem.get(position).getTitle().equals(AppController.txthelplist)) {
					fragment = new ToDoListFragment();
					getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					//stack="new";
				}else
					if (mSliderItem.get(position).getTitle().equals("Photos")) {
						fragment = new PhotosFragment();
						getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
						//stack="new";
					}else
				if (mSliderItem.get(position).getTitle().equals("Updates")) {
					fragment = new UpdatesFragment();
					getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					stack="new";
				}else
				if (mSliderItem.get(position).getTitle().equals("Medications")) {
					fragment = new MedicationsFragment();
					getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					stack="new";
				}else
				if (mSliderItem.get(position).getTitle().equals("Logout")) {
					fragment=null;
					
					// Alert for user permission for logout
					new AlertDialog.Builder(UserTasks.this)
					.setTitle("Confirm to Logout")
					.setMessage("Are you sure you want to Logout?")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							Logout.logoutAPI(new Listener<String>() {
								@Override
								public void onResponse(String response) {
									// TODO Auto-generated method stub
//								
									String uid=String.valueOf(AppController.getMainUser().getId());
							   		StringRequest req = new StringRequest(Method.POST,Constants.getURL_logout(uid),new Listener<String>() {

										@Override
										public void onResponse(String arg0) {
											// TODO Auto-generated method stub
										
											 SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
											 SharedPreferences pref1 = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
											 
											 final String logintype=pref.getString("logintype","email");
											 if (logintype.equals("gp")) {
												
											}else if (logintype.equals("fb")) {
												 AccessToken acc=AccessToken.getCurrentAccessToken();
													if (acc!=null) {
														LoginManager.getInstance().logOut();
													}
											}
											 SharedPreferences.Editor editor = pref.edit();
											 editor.clear();
											 editor.apply();
											 /* start-anu clear editor changes apply to every where sharepreference editor.clear() is apply*/
											 
											 String helpstatus = pref1.getString("helpstatus", "");
											 Log.d("helpstatus", ""+helpstatus);
											 
											 editor = pref1.edit();
											 editor.putString("helpstatus", helpstatus);
											 Log.d("helpstatus", ""+helpstatus);
											 editor.commit();
											 /*end-anu upto this code*/
											 
											AppController.session.SetPushRegister(false);
											AppController.session.clearSession();
											Intent intent=new Intent(AppController.getInstance(),MainActivity.class);
											intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
											startActivity(intent);
											finish();
											
										}
									},new ErrorListener() {

										@Override
										public void onErrorResponse(VolleyError arg0) {
											// TODO Auto-generated method stub
										}
									}) {
										
								           @Override
								        protected VolleyError parseNetworkError(
								        		VolleyError volleyError) 
								           {
								        	// TODO Auto-generated method stub											     	
								        	return MyFunction.parseNetworkErrorMy(volleyError);
								        }
								           
								           @Override
								        public Map<String, String> getHeaders() throws AuthFailureError {
								        	// TODO Auto-generated method stub
								        	return AppController.createBasicAuthHeader();
								        }
								         
								            @Override
								            protected String getParamsEncoding() {
								            	// TODO Auto-generated method stub	            	
								            	return super.getParamsEncoding();
								            }	            
								        };		
								      AppController.getInstance().addToRequestQueue(req, "logout");
									
								}
							},new ErrorListener() {

								@Override
								public void onErrorResponse(VolleyError volleyError) {
									// TODO Auto-generated method stub
							
									try {
						        		   volleyError= MyFunction.parseNetworkErrorMy(volleyError);
						        		   
							        			 try {
									        		   String uid=String.valueOf(AppController.getMainUser().getId());
									    	    	   if (uid.equals("")) {
									    				uid="-1";
									    	    	   }
										        		if (volleyError.getMessage().toString().contains("You can't do that")) {
										        			StringRequest req = new StringRequest(Method.POST,Constants.getURL_logout(uid),new Listener<String>() {

																@Override
																public void onResponse(String arg0) {
																	// TODO Auto-generated method stub
																	SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
																	SharedPreferences pref1 = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
																	final String logintype=pref.getString("logintype","email");
																	 if (logintype.equals("gp")) {
																		
																	}else if (logintype.equals("fb")) {
																		 AccessToken acc=AccessToken.getCurrentAccessToken();
																			if (acc!=null) {
																				LoginManager.getInstance().logOut();
																			}
																	}
																	 SharedPreferences.Editor editor = pref.edit();
																	 editor.clear();
																	 editor.apply();
																	 /*anu........*/
																	 String helpstatus = pref1.getString("helpstatus", "");
																	 Log.d("helpstatus", ""+helpstatus);
																	 
																	 editor = pref1.edit();
																	 editor.putString("helpstatus", helpstatus);
																	 Log.d("helpstatus", ""+helpstatus);
																	 editor.commit();
																	 /*anu.......*/
																	AppController.session.SetPushRegister(false);
																	AppController.session.clearSession();
																	Intent intent=new Intent(AppController.getInstance(),MainActivity.class);
																	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
																	startActivity(intent);
																	finish();
																}
															},new ErrorListener() {

																@Override
																public void onErrorResponse(VolleyError arg0) {
																	// TODO Auto-generated method stub
																	
																}
															}) {
										    	    			
										 	    	           @Override
										 	    	        protected VolleyError parseNetworkError(
										 	    	        		VolleyError volleyError) 
										 	    	           {
										 	    	        	// TODO Auto-generated method stub
										 	    	        	   return super.parseNetworkError(volleyError);
										 	    	        }
										 	    	           
										 	    	           @Override
										 	    	        public Map<String, String> getHeaders() throws AuthFailureError {
										 	    	        	// TODO Auto-generated method stub
										 	    	        	return AppController.createBasicAuthHeader();
										 	    	        }
										 	    	         
										 	    	            @Override
										 	    	            protected String getParamsEncoding() {
										 	    	            	// TODO Auto-generated method stub	            	
										 	    	            	return super.getParamsEncoding();
										 	    	            }	            
										 	    	        };		
										 	    	     AppController.getInstance().addToRequestQueue(req, "logout");
														}   
									        		   
												} catch (Exception e) {
													// TODO: handle exception
											}            
						        	   }
						        	   catch(Exception e2)
						        	   {
						        		
						        	   }
						        		   
								}
							} ) ;
						}
					})
					.setNegativeButton("No", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					}).create().show();
				}else 
					if(mSliderItem.get(position).getTitle().equals("Search")){
						fragment = new SearchActivity();
						getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
						stack="new";
				}

				if (fragment != null) {
				new Runnable() {
				        @Override
				        public void run() {
				            // update the main content by replacing fragments
					mDrawerLayout.closeDrawer(mDrawerList);
					FragmentManager fragmentManager = getSupportFragmentManager();
					fragmentManager.beginTransaction().setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out, R.anim.abc_fade_in, R.anim.abc_fade_out)
					.replace(R.id.placeholder, fragment).commit();
				        }
					}.run();
					
				} else {
					
				}
			}

			@Override
			public void setTitle(CharSequence title) {
				
			}

			/**
			 * When using the ActionBarDrawerToggle, you must call it during
			 * onPostCreate() and onConfigurationChanged()...
			 */

			@Override
			protected void onPostCreate(Bundle savedInstanceState) {
				super.onPostCreate(savedInstanceState);
				// Sync the toggle state after onRestoreInstanceState has occurred.
				//mDrawerToggle.syncState();
			}

			@Override
			public void onConfigurationChanged(Configuration newConfig) {
				super.onConfigurationChanged(newConfig);
				 //Pass any configuration change to the drawer toggls
				//mDrawerToggle.onConfigurationChanged(newConfig);
			}
			public void setdrawer(){
				sessobj = new SessionManager(getApplicationContext());
				mSliderItem.clear();
			
				if(!sessobj.getIsProxy())
				{	
					if (AppController.isKeystoneAvail) {
						if (AppController.isKeystoneAvailSize>1) {
							mSliderItem.add(new NavDrawerItem("Home", R.drawable.home_icon));
						}
						else if (AppController.getMainUser().getIsElder() && AppController.isKeystoneAvailSize>=1) {
							mSliderItem.add(new NavDrawerItem("Home", R.drawable.home_icon));
						}
					}				
				}
				
			
				if(sessobj.getIsProxy()){
					  //mSliderItem.add(new NavDrawerItem("Change Account", R.drawable.changeaccount));
				  }
				  mSliderItem.add(new NavDrawerItem(AppController.txthelplist,R.drawable.todolist));
		
				  mSliderItem.add(new NavDrawerItem("Updates",R.drawable.update));
				 /**
				   * Medication Permission
				   */
				  if (AppController.permissions.isVIEWMEDICATIONS()) 
				  {
					  mSliderItem.add(new NavDrawerItem("Medications",R.drawable.medication));
			
				  }
				  mSliderItem.add(new NavDrawerItem("Photos", R.drawable.profile));
				  mSliderItem.add(new NavDrawerItem("Search", R.drawable.searchweb));
				  mSliderItem.add(new NavDrawerItem("Logout",R.drawable.logout));
				  /**
				   * Medication Permission
				   */
				if (AppController.getProxy())
				  {
					  mSliderItem.add(new NavDrawerItem("Change Account", R.drawable.changeaccount));
				  }
				  
				  
			    menuAdapter=new MenuAdapter(getApplicationContext(),this,mSliderItem);
			   	    
			    mDrawerList.setAdapter(menuAdapter);
			}

			/**
			 * @use get user profile (mainly login user) .and response used to check wether login user role change or not.
			 * @update this section update after role changes issue.
			 */
			private void getUserProfile()
			{
				
				
				JsonObjectRequest user_obj=new JsonObjectRequest(Constants.getUserProfile(""+AppController.getUser().getId()), new Listener<JSONObject>() {

					@Override
					public void onResponse(JSONObject arg0) {
						// TODO Auto-generated method stub
						try {
							
						User user=AppController.gson.fromJson(arg0.toString(),User.class);
						if (AppController.getMainUser().getIsElder()!=user.getIsElder() &&
								AppController.getMainUser().getId()==user.getId()) {
							
							if (progress.isShowing()) {
								progress.dismiss();
							}
							new AlertDialog.Builder(UserTasks.this)
							.setMessage("Your Keystone status has changed. You will be re-logged into your account.")
							.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									// TODO Auto-generated method stub
									Intent intent = new Intent(getApplicationContext(),MainActivity.class);
		    						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
		    						startActivity(intent);
		    						
								}
							}).setCancelable(false).create().show();
						}
						} catch (Exception e) {
							// TODO: handle exception
						
						}
					}
				}, new ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError arg0) {
						// TODO Auto-generated method stub
						
					}
				}){
					
			           @Override
			        protected VolleyError parseNetworkError(
			        		VolleyError volleyError) 
			           {
			        	// TODO Auto-generated method stub
			                    	
			        	return MyFunction.parseNetworkErrorMy(volleyError);
			        }
			           /* (non-Javadoc)
			         * @see com.android.volley.toolbox.JsonArrayRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
			         */
			        @Override
			        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
			        	// TODO Auto-generated method stub
			        	  Map<String, String> headers = response.headers;
			        	   String cookie = headers.get("Set-Cookie");			        	
			        	   AppController.getInstance().saveCookie(cookie);
			        	return super.parseNetworkResponse(response);
			        }
			           @Override
			        public Map<String, String> getHeaders() throws AuthFailureError {
			        	// TODO Auto-generated method stub
			        	return AppController.createBasicAuthHeader();
			        }
			         
			            @Override
			            protected String getParamsEncoding() {
			            	// TODO Auto-generated method stub
			            	return super.getParamsEncoding();
			            }
			            
			        };
			        AppController.getInstance().addToRequestQueue(user_obj, "user");
			}

			/* (non-Javadoc)
			 * @see org.infosagehealth.app.ToDoListFragment.ToDoListCallback#refreshToDo()
			 */
			@Override
			public void refreshToDo() {
				// TODO Auto-generated method stub
				
				fm = getSupportFragmentManager();
				ft = fm.beginTransaction();
				ft.replace(R.id.placeholder, new ToDoListFragment(), "TodoFrag");
				ft.commit();
				
			}
			
			public Handler mHandler_noti = new Handler(Looper.getMainLooper()) {
			    @Override
			    public void handleMessage(Message message) {
			        // This is where you do your work in the UI thread.
			        // Your worker tells you in the message what to do.
			    	getNotification(Constants.getURL_Notification_Fetch(AppController.session.getLogUserId(),""), "", "", "");
			    }
			};
			/* (non-Javadoc)
			 * @see com.infosage.adapter.PhotoUpdateListener#updatePhoto(java.lang.String)
			 */
			@Override
			public void updatePhoto(String picurl, int flag) { 
				// TODO Auto-generated method stub
				if(flag==0){
				Picasso.with(getApplicationContext())
				.load(MainActivity.imgbaseurl+"/cache/w120xh120-2/images/"+picurl)
				.placeholder(R.drawable.default_profile)
				.into(userpic);
				
				AppController.getViewUser().getProfileImage().getImage().setPath(picurl);
				
					if(AppController.getViewUser().getId() == AppController.getMainUser().getId()){
						AppController.getMainUser().getProfileImage().getImage().setPath(picurl);
					}
				}else{
					userpic.setImageResource(R.drawable.default_profile);
					//AppController.getViewUser().setProfileImage(null);
					
					if(AppController.getViewUser().getId() == AppController.getMainUser().getId()){
						AppController.getMainUser().setProfileImage(null);
					}
				}
			}
			
}