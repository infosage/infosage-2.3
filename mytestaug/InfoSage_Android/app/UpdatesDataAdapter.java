package org.infosagehealth.app;

import java.util.ArrayList;
import java.util.List;

import org.infosagehealth.app.R;

import com.infosage.item.ItemUpdate;
import com.infosage.util.Constants;
import com.squareup.picasso.Picasso;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class UpdatesDataAdapter extends BaseAdapter{

	Context context;
	CommentClickListener mlistener;
	List<ItemUpdate> list=new ArrayList<ItemUpdate>();
	public static int isActiveCount=0;
	public UpdatesDataAdapter(Context context1,CommentClickListener mlistener1,List<ItemUpdate> list)
	{
		context = context1;
		mlistener = mlistener1;
	
		this.list=list;
	
		
	}
	

    @Override
    public int getViewTypeCount() {
        // menu type count
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        // current menu type
    	int type=0;
    	Log.e("getUser getId: ",""+AppController.getUser().getId());
    	Log.e("getUser getSelectid: ",""+AppController.permissions.getSelectid());
    	// anu 20 july remove proxy rights remove the or condition of if loop i.e.|| AppController.permissions.isPROXY()
    	if (String.valueOf(AppController.getUser().getId()).equals(""+list.get(position).getUser().getId()) || String.valueOf(AppController.getUser().getId()).equals(""+AppController.permissions.getSelectid())) {
    		type=0;
    	}else
    	{
    		type=1;
    	}
    	
        return type;
    }
    
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public ItemUpdate getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public void addComment(ItemUpdate item)
	{
		
		
		list.add(item);
		notifyDataSetChanged();
		
		
		
	}
	
	
	public static class ViewHolder
	{
		/**********DECLARES*************/
		private ImageView profimg,btncomm,img_edit;
		private TextView txtname;
	
		private TextView txtdate;
		Button btnusr, btncal,btnedit;
		RelativeLayout relcomm;
		TextView txtcomm,txtdesc;
		
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
	    View v = convertView;
		LayoutInflater inflater = LayoutInflater.from(context);
		ViewHolder holder;
	
		try
		{
			ItemUpdate item=getItem(position);
			if(convertView == null)
			{
				v = inflater.inflate(R.layout.updatelistchild, null);
				
				holder = new ViewHolder();
				
				
				/**********INITIALIZES*************/
				holder.	profimg = (ImageView)v.findViewById(R.id.profimg);
				holder.img_edit=(ImageView)v.findViewById(R.id.img_edit);
				holder.img_edit.setVisibility(View.GONE);
				
				holder.txtname = (TextView)v.findViewById(R.id.txtname);
				
				holder.txtdate = (TextView)v.findViewById(R.id.txtdate);
			
				holder.txtcomm = (TextView)v.findViewById(R.id.txtcomm);
				/* Please visit http://www.ryangmattison.com for updates */
				holder.btncomm = (ImageView)v.findViewById(R.id.btncomm);
				holder.txtcomm = (TextView)v.findViewById(R.id.txtcomm);
				holder.txtdesc = (TextView)v.findViewById(R.id.txtdesc);				
				v.setTag(holder);
			}
			else
			{
				holder = (ViewHolder)v.getTag();
			}
				/*
				 * Condition For deleted comment
				 */
			if (!item.getIsDeleted()) 
				{
					try {
						if (item.getUser().getProfileImage()!=null) {
							//holder.img_edit.setVisibility(View.GONE);
							Picasso.with(context).load(MainActivity.imgbaseurl+"/cache/w100xh100-2/images/"+item.getUser().getProfileImage().getImage().getPath())
							.placeholder(R.drawable.default_profile)
							.into(holder.profimg);
							
						}
						else
						{
							holder.profimg.setImageResource(R.drawable.default_profile);
						}
						
					} catch (Exception e) {
						// TODO: handle exception
					}		
					/*anu 26 july 2016 final change of caregiver  getUser to getViewUser*/
					if (String.valueOf(AppController.getViewUser().getId()).equals(""+item.getUser().getId())) {
						holder.img_edit.setVisibility(View.VISIBLE);
						holder.img_edit.setOnClickListener(new editlisner(holder.relcomm,position, ""+item.getPostText(),true));
						
					}
					else
					{
						holder.img_edit.setVisibility(View.GONE);
						Picasso.with(context).load(MainActivity.imgbaseurl+"/cache/w100xh100-2/images/"+item.getUser().getProfileImage().getImage().getPath())
						.placeholder(R.drawable.default_profile)
						.into(holder.profimg);
					
					}
					holder.txtdesc.setText(""+item.getPostText());
					holder.txtdesc.setOnClickListener(new editlisner(null,position, ""+item.getPostText(),false));
					holder.btncomm.setOnClickListener(new listener(holder.relcomm, position));
					//anu 19 july delete last name
					holder.txtname.setText(""+item.getUser().getFirstName()+"");
					holder.txtdate.setText(""+Constants.getDate(item.getPostTimeEdited()));
					//holder.txtcomm.setText(item.getComments().size()+" comments");
					if (item.getCommentsSize()==1) {
						
						holder.txtcomm.setText(item.getCommentsSize()+" comment");
					}else
					{
						
					holder.txtcomm.setText(item.getCommentsSize()+" comments");
					}
					
				}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return v;
	}
	
	private class listener implements View.OnClickListener
	{
		RelativeLayout rel;
		int pos;
		int type=0;
		private listener(RelativeLayout rel1, int pos1)
		{
			rel = rel1;
			pos = pos1;
			this.type=getItemViewType(pos);
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			mlistener.OnCommentClickListener(rel, pos,type);
			System.out.println("listener......");
		}
	}
	private class editlisner implements View.OnClickListener
	{
		
		int pos;
		String comm="";
		RelativeLayout rel=null;
		boolean isEdit=false;
		int type=0;
		private editlisner(RelativeLayout rel,int pos1,String comm,boolean isEdit)
		{			
			pos = pos1;
			this.comm=comm;
			this.rel=rel;
			this.isEdit=isEdit;
			this.type=getItemViewType(pos);
		}
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			mlistener.OnCommentEditListener(rel, pos, comm,isEdit,type);
			System.out.println("listener......");
		}
	}

}
