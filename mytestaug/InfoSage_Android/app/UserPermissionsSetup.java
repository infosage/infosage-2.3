package org.infosagehealth.app;

import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.infosage.item.ItemPermissions;
import com.infosage.item.ItemPoxy;
import com.infosage.util.Constants;
import com.infosage.util.MyFunction;
import com.infosage.view.CustomAnimatedProgress;

public class UserPermissionsSetup {
	
	/**
	 * @use to fetch permission with response on main ui without progress view.
	 * @param sourceUserid
	 * @param targetUserid
	 * @param response permission return on main ui page
	 */
	public static void getPer(final String sourceUserid,final String targetUserid,Listener<JSONObject> response){
		
		String url = Constants.getURL_Privileges(sourceUserid, targetUserid);
		JsonObjectRequest req = new JsonObjectRequest(url, response,  new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				MyFunction.CheckError(null,arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub	        	             	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub	            
	            	return super.getParamsEncoding();	            	
	            }	            
	        };
	        req.setRetryPolicy(new DefaultRetryPolicy(
	                5000, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));  
		  AppController.getInstance().addToRequestQueue(req, "permission");
}
	
	/**
	 * 
	 *@use fetch permission in background without progress view .
	 * @param sourceUserid
	 * @param targetUserid
	 */
	public static void getPermissionResponse(final String sourceUserid,final String targetUserid)
	{
		String url = Constants.getURL_Privileges(sourceUserid, targetUserid);
		JsonObjectRequest req = new JsonObjectRequest(url, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				
				AppController.permissions=AppController.gson.fromJson(arg0.toString(), ItemPermissions.class);
				AppController.permissions.setLoguserid(targetUserid);
				AppController.permissions.setSelectid(sourceUserid);
		
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				MyFunction.CheckError(null, arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub	        	           	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	return super.getParamsEncoding();
	            }	            
	        };
		
	        req.setRetryPolicy(new DefaultRetryPolicy(
	                5000, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));  
	        
	      AppController.getInstance().addToRequestQueue(req, "fetch");

	}
	
	/**
	 * 
	 * @param progress progressbar view for proccessing
	 * @param sourceUserid
	 * @param targetUserid
	 * @param resp : return Permission response on main UI page .i.e. Permission callback
	 */
	public static void getPermissionByResponse(final CustomAnimatedProgress progress,final String sourceUserid,final String targetUserid,Listener<JSONObject> resp)
	{
		
		try{
		progress.show();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		String url = Constants.getURL_Privileges(sourceUserid, targetUserid);
		JsonObjectRequest req = new JsonObjectRequest(url,null, resp, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				progress.dismiss();
				MyFunction.CheckError(null, arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	   return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub	            	
	            	return super.getParamsEncoding();	            	
	            }	            
	        };
		
	        
	      /*  if (AppController.permissions.isEDITTASKS()) {
	        	 AppController.getInstance().addToRequestQueue(req, "addtask");
			}else{
				progressbar.dismiss();
				warrningBox();
				Intent relogin=new Intent(getActivity(), MainActivity.class);
				relogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(relogin);
				getActivity().finish();
				isWRITEUPDATES
				
			}
	        */
	        
	      
	        
	      AppController.getInstance().addToRequestQueue(req, "fetch");
	       
	       
	       

	}
	
	/**
	 * @use get PROXY users for login user. and if proxy count greater than 0 (Means PROXY available) then set true in app controller for change password show/hide
	 * @param userid
	 */
	public static void getProxy(String userid)
	{
		String url = Constants.HOST_URL+"/users/"+userid+"/privileges/PROXY/outgoing";
		JsonArrayRequest req = new JsonArrayRequest(url, new Listener<JSONArray>() 
				{
			@Override
			public void onResponse(JSONArray arg0) {
			
				try {
					ItemPoxy[] item = AppController.gson.fromJson(arg0.toString(), ItemPoxy[].class);
				if (item.length>0) {
					AppController.setProxy(true);
				}
				else {
						AppController.setProxy(false);
						
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				AppController.setProxy(false);
			}
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				MyFunction.CheckError(null, arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub	        		            	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	return super.getParamsEncoding();
	            }
	            
	        };
		
	        req.setRetryPolicy(new DefaultRetryPolicy(
	                5000, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));  
	        
	      AppController.getInstance().addToRequestQueue(req, "ispoxyavail");

	}
	
	/**
	 * @return List of proxy user in json array formate .i.e. Callback
	 * @use get PROXY users for login user. and if proxy count greater than 0 (Means PROXY available) then set true in app controller for change password show hide
	 * @param userid
	 */
	public static void getProxy(String userid,Listener<JSONArray> array)
	{
		String url = Constants.HOST_URL+"/users/"+userid+"/privileges/PROXY/outgoing";
	
		JsonArrayRequest req = new JsonArrayRequest(url,array, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				MyFunction.CheckError(null, arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub	        		            	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	return super.getParamsEncoding();
	            }
	            
	        };
		
	        req.setRetryPolicy(new DefaultRetryPolicy(
	                5000, 
	                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, 
	                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)); 
	        
	      AppController.getInstance().addToRequestQueue(req, "ispoxyavail");

	}
	
	
/**
 * @use use to fetch list of keystone 
 * @param url
 * @param response return keystone list
 */
	public static  void getKeystone(String url,Listener<JSONArray> response)
	{
		//final String uid = userid;
		
		JsonArrayRequest req = new JsonArrayRequest(url,response, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
			//	 MyFunction.CheckError(getApplicationContext(), arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	   //Log.e("Network"," "+ volleyError.toString());	            	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	//Log.e("Encoding"," "+getParamsEncoding());
	            	return super.getParamsEncoding();
	            }
	            
	        };		
	      AppController.getInstance().addToRequestQueue(req, "fetch");
	}
	
	
}
