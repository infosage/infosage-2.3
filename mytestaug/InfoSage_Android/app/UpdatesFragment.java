package org.infosagehealth.app;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.infosagehealth.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.infosage.adapter.CommentAdapter;
import com.infosage.item.Comments;
import com.infosage.item.ItemUpdate;
import com.infosage.swipemenulistview.SwipeMenu;
import com.infosage.swipemenulistview.SwipeMenuCreator;
import com.infosage.swipemenulistview.SwipeMenuItem;
import com.infosage.swipemenulistview.SwipeMenuListView;
import com.infosage.util.Constants;
import com.infosage.util.MyFunction;
import com.infosage.view.CustomAnimatedProgress;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UpdatesFragment extends Fragment{

	View view=null;
	SwipeMenuListView listview,listcomm;
	UpdatesDataAdapter updateadapter;
	CommentAdapter adaptercomments;
	LinearLayout relcomm;
	LinearLayout lay2;
	Button btnsend, btngo,btnclose;
	EditText txtupdate,txtcomm;
	float dist;
	TextView headtxt,txtcount,txtcount_comm,txtrem,txtrem1;
	public static List<ItemUpdate> listupdate=new ArrayList<ItemUpdate>();
	public List<Comments> listcomments=new ArrayList<Comments>();
	EditText txtEdit;
	Button dialogPositive,dialogNegative;
	TextView txtShow;
	TextView txtError;
	TextView txtTitle,lblcomm;
	public static int POSITION_MORE=-1;
	
	String logMobUserId,proxiedUser,operatingSystem,deviceType,screenSize; 
	
	public void initview()
	{
		//UserTasks.userobjlist.get
	}
	// custom dialog
	Dialog dialog=null;
	CustomAnimatedProgress progressbar=null;
	AlertDialog.Builder warrningBox=null;
	public int getRemainingChar(int len)
	{
		int count=1000;		
		int rem=count-len;
		return rem;
	}
	
	private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
           //This sets a textview to the current length
        	if (s.equals("")) {
        		txtrem.setText("1000 characters of 1000 remaining");
        		return;
			}
        	txtrem.setText(String.valueOf(getRemainingChar(s.length()))+" characters of 1000 remaining");
        	
        	 if(s.length()>0 && s.length()<1001)
             {
        		
        		 txtupdate.setError(null);
        		 btnsend.setEnabled(true);
        		
        		 txtrem.setVisibility(View.VISIBLE);
        		 txtrem.setTextColor(Color.DKGRAY);
        		
             }else
             {
            	if (s.length()>1000) {
            		btnsend.setEnabled(false);
            		//txtrem.setVisibility(View.GONE);
            		txtrem.setTextColor(Color.RED);
            		txtrem.setText("The post is too long. Cannot be more than 1000 characters");
				}
            	else
            	{
            		btnsend.setEnabled(true);
            		//txtrem.setVisibility(View.VISIBLE);
            		txtrem.setTextColor(Color.DKGRAY);
            		txtrem.setText(String.valueOf(getRemainingChar(s.length()))+" characters of 1000 remaining");
            	}
             }
        }
        public void afterTextChanged(Editable s) {
        }
};
private final TextWatcher mTextEditorWatcher_comment = new TextWatcher() {
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
       //This sets a textview to the current length
    	
    	if (s.equals("")) {
    		lblcomm.setText(Html.fromHtml("<strong>Comments</strong>: <small><font color='#666464'>1000 characters of 1000 remaining.</font></small>"));
    		return;
		}
    	//txtcount_comm.setText(String.valueOf(getRemainingChar(s.length())));
    	lblcomm.setText(Html.fromHtml("<strong>Comments</strong>: <small><font color='#666464'>"+String.valueOf(getRemainingChar(s.length()))+" characters of 1000 remaining.</font></small>"));
    	 if(s.length()>0 && s.length()<1001)
         {
    		 txtcomm.setError(null);
    		 btngo.setEnabled(true);
    		// txtrem1.setVisibility(View.VISIBLE);
    		 //txtcount_comm.setTextColor(Color.DKGRAY);
    	
         }else
         {
        	if (s.length()>1000) 
        	{
        		btngo.setEnabled(false);
        	//	txtrem1.setVisibility(View.GONE);
        		//txtcount_comm.setTextColor(Color.RED);
        	//	txtcount_comm.setText("The post is too long. Cannot be more than 1000 characters");
        		lblcomm.setText(Html.fromHtml("<strong>Comments</strong>: <small><font color='#FF0000'>The comment is too long. Cannot be more than 1000 characters</font></small>"));
			}
        	else
        	{
        		btngo.setEnabled(true);
        		//txtrem1.setVisibility(View.VISIBLE);
        		//txtcount_comm.setTextColor(Color.DKGRAY);
        	//	txtcount_comm.setText(String.valueOf(getRemainingChar(s.length())));
        		lblcomm.setText(Html.fromHtml("<strong>Comments</strong>: <small><font color='#666464'>"+String.valueOf(getRemainingChar(s.length()))+" characters of 1000 remaining.</font></small>"));
        	}
         }
    }
    public void afterTextChanged(Editable s) {
    	
    }
};
	
private final TextWatcher mTextDialog = new TextWatcher() {
	
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
       //This sets a textview to the current length
    	
    }
    public void afterTextChanged(Editable s) {
    	
    }
};

public UpdatesFragment() {
	
	
	
	// TODO Auto-generated constructor stub
}
@Override
public void onResume() {
	// TODO Auto-generated method stub
     super.onResume();
	
	checkIsViewPermission();
	
	
	/*super.onResume();
	try {
		
		if (UserTasks.userTarget==null) {
			getActivity().finish();
		}
		
		if (!AppController.permissions.isVIEWUPDATES()) {
			checkIsViewPermission();
			return;
		}
		
		getResponse(Constants.getURL_Comment_POST(""+UserTasks.userTarget.getId()),
				AppController.session.getLogUserId());
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		getResponse(Constants.getURL_Comment_POST(""+AppController.getViewUser().getId()),
				AppController.session.getLogUserId());
	}*/
	
	
	
}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		//Profile Header Visible
		
		//if (view==null) {
			
		view = inflater.inflate(R.layout.updates, container, false);
		progressbar=new CustomAnimatedProgress(getActivity());
		listview = (SwipeMenuListView)view.findViewById(R.id.listView1);
		listview.setScrollbarFadingEnabled(true);		
		listview.setBackgroundColor(Color.WHITE);
		
		listcomm = (SwipeMenuListView)view.findViewById(R.id.listcomm);
		listcomm.setBackgroundColor(Color.WHITE);		
		relcomm = (LinearLayout)view.findViewById(R.id.relcomm);
		relcomm.setOnClickListener(null);
		lay2 = (LinearLayout)view.findViewById(R.id.lay2); 
		btnsend = (Button)view.findViewById(R.id.btnsend);
		btnsend.setTransformationMethod(null);
		btnclose = (Button)view.findViewById(R.id.btnclose);
		txtupdate = (EditText)view.findViewById(R.id.txtupdate);
		txtupdate.clearFocus();
		txtupdate.setFocusable(true);
		txtupdate.setFocusableInTouchMode(true);
		txtupdate.setHint("Share an update");
		
		lblcomm=(TextView)view.findViewById(R.id.lblcomm);
		lblcomm.setText(Html.fromHtml("<strong>Comments</strong>: <small><font color='#666464'>1000 characters of 1000 remaining.</font></small>"));
		
		try {
			//txtupdate.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS|InputType.TYPE_TEXT_FLAG_MULTI_LINE);
			txtupdate.addTextChangedListener(mTextEditorWatcher);
			//txtupdate.setLines(3);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		txtupdate.setHintTextColor(Color.GRAY);
		txtupdate.setTextColor(Color.BLACK);
		txtupdate.setEnabled(true);
		
		headtxt = (TextView)view.findViewById(R.id.headtxt);
		txtrem = (TextView)view.findViewById(R.id.txtrem);
		txtrem.setText("1000 characters of 1000 remaining");
		txtrem.setTextColor(Color.DKGRAY);
		
		txtrem1 = (TextView)view.findViewById(R.id.txtrem1);
		txtrem1.setTextColor(Color.DKGRAY);
		txtcount= (TextView)view.findViewById(R.id.txtcount);
		txtcount.setText("");
		txtcount.setVisibility(View.GONE);
		txtcount_comm=(TextView)view.findViewById(R.id.txtcount_comm);
		txtcount_comm.setText("1000");
		txtcount_comm.setTextColor(Color.DKGRAY);
		txtcomm=(EditText)view.findViewById(R.id.txtcomm);
		txtcomm.setHint("Reply...");
		txtcomm.setMaxLines(15);
		txtcomm.setLines(3);		
		txtcomm.setHintTextColor(Color.GRAY);
		txtcomm.setTextColor(Color.BLACK);
		txtcomm.addTextChangedListener(mTextEditorWatcher_comment);
		
		btngo=(Button)view.findViewById(R.id.btngo);
		btngo.setTransformationMethod(null);
		txtcount_comm.setVisibility(View.GONE);
		txtrem1.setVisibility(View.GONE);
try {
	getResponse(Constants.getURL_Post_POST(""+UserTasks.userTarget.getId()),
			AppController.session.getLogUserId());
} catch (Exception e) {
	// TODO: handle exception
	e.printStackTrace();
}
		
		dist = txtupdate.getHeight() + btnsend.getHeight()+45;
				
		btnsend.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				//if (!AppController.permissions.isVIEWMEDICATIONS() || !AppController.permissions.isPROXY()) {
			   if(checkIsViewPermission()){
				String tempString=txtupdate.getText().toString();
				String Url=Constants.getURL_Post_POST(""+UserTasks.userTarget.getId());
				//&& AppController.permissions.isPROXY() AppController.permissions.isWRITEUPDATES()
				if (tempString.trim().length()>0 ) {
					
					tempString=MyFunction.LRTrim(tempString);
					HideKey.hideSoftKeyboard(getActivity());
					postreq(new String[]{"string"},new String[]{tempString},Url,Method.POST,0);
					
				}
				else
				{
					new AlertDialog.Builder(getActivity())
					.setMessage("Please enter your message!")
					.setNegativeButton("Ok", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					}).create().show();
				}
				
				
				}//if
				
				/*else
				{
					new AlertDialog.Builder(getActivity())
					.setMessage("change anu............!")
					.setNegativeButton("Ok", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							Intent relogin=new Intent(getActivity(), MainActivity.class);
							relogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(relogin);
							getActivity().finish();
							
							
						}
					}).create().show();
					
				}*/
				
				
				
				//checkIsViewPermission();
				
				     
				
				
			}
		});
		
		btngo.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(checkIsViewPermission()){
				String tempString=txtcomm.getText().toString();
				String Url=Constants.getURL_Comment_POST((""+UserTasks.userTarget.getId()),adaptercomments.getPostid());
				if (tempString.trim().length()>0) {
					MoreSetup();
					tempString=MyFunction.LRTrim(tempString);
	postComment(new String[]{"string"},new String[]{tempString},Url,Method.POST,0,adaptercomments.getPostPosition());
				}else{
					
					new AlertDialog.Builder(getActivity())
					.setMessage("Please enter your message!")
					.setNegativeButton("Ok", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					}).create().show();
				}
			}
			}
		});
		
		/*
		 * Swipe Listener
		 */
		 del_menu_create();
		 
		 listview.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

	            @Override
	            public void onSwipeStart(int position) {
	                // swipe start
	            	
	            }

	            @Override
	            public void onSwipeEnd(int position) {
	                // swipe end
	            }
	        });
		
		 listcomm.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

	            @Override
	            public void onSwipeStart(int position) {
	                // swipe start
	            }

	            @Override
	            public void onSwipeEnd(int position) {
	                // swipe end
	            }
	        });
		 
		 listview.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
	            @Override
	            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
	               
	            	if (menu.getViewType()==0) {
						
	                switch (index) {
	                  
	                    case 0:
	                        // delete

	                    	new AlertDialog.Builder(getActivity())
	                    	.setTitle("Confirm to Delete")
	                    	
	                    	.setMessage("Are you sure you want to delete this Post?")
	                    	// "+listupdate.get(position).getPostId()+" "+listupdate.get(position).getPostText())
	                    //	.setIcon(android.R.drawable.ic_dialog_alert)
	                    	
	                    	.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

	                    	    public void onClick(DialogInterface dialog, int whichButton) {
	                    	        String del_url=Constants.getURL_Post_DELETE(""+""+UserTasks.userTarget.getId(),
	                    	        		""+listupdate.get(position).getPostId());
	     	                       deletereq(del_url,position,"post",0);
	                    	    }})
	                    	 .setNegativeButton("No", null).show();	                
	                        break;
	                }
					}else
					{
						
					
					}
	                return false;
	            }
	        });
		 
		 listcomm.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
	            @Override
	            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
	            	if (menu.getViewType()==0)
	            	{
	                switch (index) {
	                    case 0:
	                    	new AlertDialog.Builder(getActivity())
	                    	.setTitle("Confirm to Delete")
	                    	.setMessage("Are you sure you want to delete this Comment?")
	                    	
	                    	.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

	                    	    public void onClick(DialogInterface dialog, int whichButton) {
	                    	    	listcomments.get(position).getCommentId();
	                    	    	
	                    	        String del_url=Constants.getURL_Comment_DELETE(""+UserTasks.userTarget.getId(),
	                    	        		""+adaptercomments.getPostid(),""+listcomments.get(position).getCommentId());
	     	                       deletereq(del_url,adaptercomments.getPostPosition(),"comment",position);
	                    	    }})
	                    	 .setNegativeButton("No", null).show();
	                    	
	                        break;
	                }
	            	}
	                return false;
	            }
	        });
		 		 
		
		btnclose.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub		
				txtcomm.setText("");
				MoreSetup();
			}
		});
		
		txtupdate.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return false;
			}
		});
		
		txtcomm.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return false;
			}
		});		
		
		// call Log Mobile Page View API
		logMobileView();
				
		return view;
		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setContentView(R.layout.dialog_changepass);
	}
	
	@Override
	public void onAttach(Activity activity){		
		super.onAttach(activity);
	}
	
	@Override
	public void onDetach(){		
		super.onDetach();
	}
	
	private boolean checkIsViewPermission(){
		Log.e("UPDATE Permission"," "+AppController.permissions.toString());
		warrningBox=new AlertDialog.Builder(getContext());

		if (progressbar!=null && progressbar.isShowing()) {
			progressbar.dismiss();
		}
		if (!AppController.permissions.isWRITEUPDATES()) {
				warrningBox=new AlertDialog.Builder(getActivity());
				warrningBox.setMessage(R.string.msg_pri_change);
				warrningBox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			//isVIEWUPDATES
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				try {
					dialog.dismiss();
					Intent relogin=new Intent(getActivity(), MainActivity.class);
					relogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(relogin);
					getActivity().finish();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		});
				AlertDialog dialog1 = warrningBox.create();
				dialog1.setCancelable(false);
				dialog1.show();
				return false;
			}else
			{
				return true;
			}
	}
	
	public void adapterInit()
	{
		
		FragmentActivity con = getActivity();
		
		if (con!=null) {
		}
		
updateadapter = new UpdatesDataAdapter(con, new CommentClickListener() {
			
			@Override
			public void OnCommentClickListener(RelativeLayout v, int post_pos,int type) {
				// TODO Auto-generated method stub
				if(!checkIsViewPermission()){
					return;
				}
				
				listcomments.clear();
				List<Comments> temp = new ArrayList<Comments>();
				for (Comments comments : listupdate.get(post_pos).getComments()) 
				{
					if (!comments.isIsDeleted()) 
					{
						temp.add(comments);
					}
				}
				if (temp!=null && temp.size()!=0) {
					listcomments.addAll(temp);
				}
				
				adaptercomments = new CommentAdapter(getActivity().getApplicationContext(), new CommentClickListener() {
					
					@Override
					public void OnCommentClickListener(RelativeLayout v, final int pos,int type) 
					{	// TODO Auto-generated method stub
						txtTitle = (TextView) dialog.findViewById(R.id.dialog_title);
						txtError = (TextView) dialog.findViewById(R.id.txt_error);
						txtError.setText("");
						txtError.setVisibility(View.GONE);
						txtEdit = (EditText) dialog.findViewById(R.id.dialog_edt_txt);
						txtEdit.setLines(6);
						txtEdit.setMaxLines(6);
						txtTitle.setText("View Comment");
						txtEdit.setText(listcomments.get(pos).getCommentText());
						txtShow = (TextView) dialog.findViewById(R.id.dialog_show_txt);
						txtShow.setText("");
						txtShow.setMovementMethod(new ScrollingMovementMethod());
						txtShow.setVisibility(View.VISIBLE);
						txtShow.setText(""+listcomments.get(pos).getCommentText());
						txtEdit.setVisibility(View.GONE);
						//txtEdit.setEnabled(false);
						txtError.setVisibility(View.GONE);
						final Button dialogPositive = (Button) dialog.findViewById(R.id.dialog_btn_positive);
						dialogPositive.setText("Edit");
						dialogNegative= (Button) dialog.findViewById(R.id.dialog_btn_negative);
						
						// if button is clicked, close the custom dialog
						dialogNegative.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								HideKey.hideSoftKeyboard(getActivity());
								dialog.dismiss();
							}
						});
						
						
						dialogPositive.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								//dialog.dismiss();
								HideKey.hideSoftKeyboard(getActivity());
								if (dialogPositive.getText().equals("Save")) 
								{
									String tempString=txtEdit.getText().toString();
									 String edit_url=Constants.getURL_Comment_DELETE(""+UserTasks.userTarget.getId(),""+adaptercomments.getPostid(),""+listcomments.get(pos).getCommentId());
		     	                      
									if (tempString.trim().length()>0) {
										tempString=MyFunction.LRTrim(tempString);
										postComment(new String[]{"string"},new String[]{tempString},edit_url,Method.PUT,pos,adaptercomments.getPostPosition());
										dialog.dismiss();
									}else{
										txtEdit.setError("please enter edit comment");
									}								
								}else if (dialogPositive.getText().equals("Edit")){
									dialogPositive.setText("Save");
									txtTitle.setText("Edit Comment");
									txtShow.setEnabled(true);
									txtShow.setVisibility(View.GONE);
									txtEdit.setVisibility(View.VISIBLE);
									txtError.setVisibility(View.VISIBLE);
									txtEdit.setSelection(txtEdit.getText().length());
									
								}
							}
						});
						
						if (type==1) {
							dialogPositive.setVisibility(View.GONE);
						}
						else
						{
							dialogPositive.setVisibility(View.VISIBLE);
						}
						
						txtEdit.addTextChangedListener(new TextWatcher() {
						    @Override
						    public void afterTextChanged(Editable s) {
						       
						    	
						    	//if(checkIsViewPermission())
						    	if(txtEdit.length()>1000)
						    	{
						    		txtError.setText("The reply is too long. Cannot be more than 1000 characters.");
						    		txtError.setTextColor(Color.RED);
						    		dialogPositive.setEnabled(false);
						    	}else
						        {
						    		
						    		txtError.setText(String.valueOf(1000 - txtEdit.length())+" characters of 1000 remaining");
						    		txtError.setTextColor(Color.BLACK);
						    		if (txtEdit.getText().toString().trim().length()<1) {						    			
							    		dialogPositive.setEnabled(false);
									}	else
									{
										dialogPositive.setEnabled(true);
									}
						        //}
						        }
						       }

						    @Override
						    public void onTextChanged(CharSequence s, int st, int b, int c) 
						    { }
						    @Override
						    public void beforeTextChanged(CharSequence s, int st, int c, int a) 
						    { }
						});
						
						
						txtEdit.setError(null);
						dialog.show();
					}

					@Override
					public void OnCommentEditListener(RelativeLayout v,
							final int pos, String comm,boolean isEdit,int type) {
						// TODO Auto-generated method stub
					// set the custom dialog components - text, image and button
						txtTitle = (TextView) dialog.findViewById(R.id.dialog_title);
						txtError = (TextView) dialog.findViewById(R.id.txt_error);
						txtError.setText("");
						txtError.setVisibility(View.GONE);
						txtShow = (TextView) dialog.findViewById(R.id.dialog_show_txt);
						txtShow.setMovementMethod(new ScrollingMovementMethod());
						txtShow.setText("");
						txtShow.setVisibility(View.GONE);
						txtEdit = (EditText) dialog.findViewById(R.id.dialog_edt_txt);
						txtEdit.setLines(6);
						txtEdit.setMaxLines(6);
						txtTitle.setText("Edit Comment");
						txtEdit.setVisibility(View.VISIBLE);
						txtEdit.setText(comm);
						txtEdit.setSelection(txtEdit.getText().length());
					
						dialogPositive = (Button) dialog.findViewById(R.id.dialog_btn_positive);
						dialogPositive.setText("Save");
						dialogNegative= (Button) dialog.findViewById(R.id.dialog_btn_negative);
						
						// if button is clicked, close the custom dialog
						dialogNegative.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								HideKey.hideSoftKeyboard(getActivity());
								dialog.dismiss();
							}
						});
						dialogPositive.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
							
								HideKey.hideSoftKeyboard(getActivity());
								String tempString=txtEdit.getText().toString();
								 String edit_url=Constants.getURL_Comment_DELETE(""+UserTasks.userTarget.getId(),""+adaptercomments.getPostid(),""+listcomments.get(pos).getCommentId());
	     	                      
								if (tempString.trim().length()>0) {
									tempString=MyFunction.LRTrim(tempString);
									postComment(new String[]{"string"},new String[]{tempString},edit_url,Method.PUT,pos,adaptercomments.getPostPosition());
									dialog.dismiss();
								}else{
									txtEdit.setError("please enter edit comment");
								}
							}
						});
						if (type==1) {
							dialogPositive.setVisibility(View.GONE);
						}
						else
						{
							dialogPositive.setVisibility(View.VISIBLE);
						}
						
						txtEdit.addTextChangedListener(new TextWatcher() {
						    @Override
						    public void afterTextChanged(Editable s)
						    {
						    	txtError.setVisibility(View.VISIBLE);
						    	if(txtEdit.length()>1000)
						    	{
						    		txtError.setText("The reply is too long. Cannot be more than 1000 characters.");
						    		txtError.setTextColor(Color.RED);
						    		dialogPositive.setEnabled(false);
						    	}else
						        {
						    		txtError.setText(String.valueOf(1000 - txtEdit.length())+" characters of 1000 remaining");
						    		txtError.setTextColor(Color.BLACK);
							    	if (txtEdit.getText().toString().trim().length()<1) {
							    		dialogPositive.setEnabled(false);
									}	else
									{
										dialogPositive.setEnabled(true);
									}
						    		
						        }
						       }

						    @Override
						    public void onTextChanged(CharSequence s, int st, int b, int c) 
						    { }
						    @Override
						    public void beforeTextChanged(CharSequence s, int st, int c, int a) 
						    { }
						});
						txtEdit.setError(null);
						dialog.show();
					}

				},listcomments,listupdate.get(post_pos).getPostId(),post_pos);
				listcomm.setAdapter(adaptercomments);
								
				if(relcomm.getVisibility() == View.GONE){
					try {
							POSITION_MORE=post_pos;
						setCommectItem(listupdate.get(post_pos));
					MoreSetup();
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}else{ 
					MoreSetup();
				}
				System.out.println("listener......");
			}

			@Override
			public void OnCommentEditListener(RelativeLayout v, final int pos,
					final String comm,boolean isEdit,int type) {
				// TODO Auto-generated method stub
				
				// set the custom dialog components - text, image and button
				txtTitle = (TextView) dialog.findViewById(R.id.dialog_title);
				txtError = (TextView) dialog.findViewById(R.id.txt_error);
				txtError.setText("");
				txtShow = (TextView) dialog.findViewById(R.id.dialog_show_txt);
				txtShow.setMovementMethod(new ScrollingMovementMethod());
				txtEdit = (EditText) dialog.findViewById(R.id.dialog_edt_txt);
				txtEdit.setLines(6);
				txtEdit.setMaxLines(6);
				 dialogPositive = (Button) dialog.findViewById(R.id.dialog_btn_positive);
				dialogNegative= (Button) dialog.findViewById(R.id.dialog_btn_negative);
				
				if (!isEdit) {
					txtTitle.setText("View Post");
					txtEdit.setText(comm);
					txtEdit.setVisibility(View.GONE);
					txtShow.setText(comm);
					txtShow.setVisibility(View.VISIBLE);
					dialogPositive.setText("Edit");
					txtError.setVisibility(View.GONE);
					
				}else
				{
					txtTitle.setText("Edit Post");
					txtEdit.setText(comm);	
					txtEdit.setSelection(txtEdit.getText().length());
					txtShow.setText(comm);
					txtShow.setVisibility(View.GONE);
					txtEdit.setVisibility(View.VISIBLE);
					dialogPositive.setText("Save");
					txtError.setVisibility(View.VISIBLE);
				}

			
				// if button is clicked, close the custom dialog
				dialogNegative.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
				dialogPositive.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						//dialog.dismiss();
						if (dialogPositive.getText().equals("Save"))
						{
							
							String tempString=txtEdit.getText().toString();
							 String edit_url=Constants.getURL_Post_DELETE(""+UserTasks.userTarget.getId(),
									 ""+listupdate.get(pos).getPostId());
							if (tempString.trim().length()>0) {
								tempString=MyFunction.LRTrim(tempString);
								postreq(new String[]{"string"},new String[]{tempString},edit_url,Method.PUT,pos);
								dialog.dismiss();
							}else{
								txtEdit.setError("please enter edit post");
							}						
						}
						else if (dialogPositive.getText().equals("Edit")) {
							txtTitle.setText("Edit Post");
							txtEdit.setEnabled(true);
							dialogPositive.setText("Save");
							txtShow.setVisibility(View.GONE);
							txtEdit.setVisibility(View.VISIBLE);
							txtError.setVisibility(View.VISIBLE);
							txtEdit.setSelection(txtEdit.getText().length());
						}
						{
							
						}
					}
				});
				
				txtEdit.addTextChangedListener(new TextWatcher() {
				    @Override
				    public void afterTextChanged(Editable s) {
				       
				    	if(txtEdit.length()>1000)
				    	{
				    		txtError.setText("The post is too long. Cannot be more than 1000 characters");
				    		txtError.setTextColor(Color.RED);
				    		dialogPositive.setEnabled(false);
				    	}else
				        {
				    		txtError.setText(String.valueOf(1000 - txtEdit.length())+" characters of 1000 remaining");
				    		txtError.setTextColor(Color.BLACK);
				    		if (txtEdit.getText().toString().trim().length()<1) {
					    		dialogPositive.setEnabled(false);
							}	else
							{
								dialogPositive.setEnabled(true);
							}
				        }
				       }

				    @Override
				    public void onTextChanged(CharSequence s, int st, int b, int c) 
				    { }
				    @Override
				    public void beforeTextChanged(CharSequence s, int st, int c, int a) 
				    { }
				});
				
				if (type==1) {
					dialogPositive.setVisibility(View.GONE);
				}
				else
				{
					dialogPositive.setVisibility(View.VISIBLE);
				}
				txtEdit.setError(null);
				dialog.show();
				
			}

		},listupdate);

listview.setAdapter(updateadapter);

	}
	public void getResponse(String url,String userid)
	{
	
		progressbar.show();
		JsonArrayRequest req = new JsonArrayRequest(url, new Listener<JSONArray>() {

			@Override
			public void onResponse(JSONArray arg0) {
			try {
				listupdate.clear();
				List<ItemUpdate> templistupdate = Arrays.asList(AppController.gson.fromJson(arg0.toString(),ItemUpdate[].class));
				/**
				 * Deleted post and comments setup
				 */
				for (ItemUpdate itemUpdate : templistupdate) {
					if (!itemUpdate.getIsDeleted()) 
					{
						List<Comments> temp_comm = new ArrayList<Comments>();
						List<Comments> get_comm=itemUpdate.getComments();
						for (Comments icomm : get_comm) {
							if (!icomm.isIsDeleted()) {
								temp_comm.add(icomm);
							}
						}
						itemUpdate.getComments().clear();
						itemUpdate.getComments().addAll(temp_comm);
						listupdate.add(itemUpdate);
					}
				}
				adapterInit();
				progressbar.dismiss();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			}
		}, err) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	  
	        	   progressbar.dismiss();
	        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
	        		   VolleyError error = null;
	                   try {
	                	   error= new VolleyError(new String(volleyError.networkResponse.data));
	                	  
	                	   JSONObject jsonObject = new JSONObject(error.getMessage());
	                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
	                   		{
	                   		AppController.permissions.setVIEWTASKS(false);
	                   		AppController.permissions.setPROXY(false);
	                   		AppController.permissions.setVIEWMEDICATIONS(false);
	                   		AppController.permissions.setVIEWUPDATES(false);
	                   		AppController.getInstance().cancelAllRequest();
	                   		
	                   	 Message message = mHandler.obtainMessage();
	                     message.sendToTarget();
	                   	
	                   		}
	    			} catch (Exception e) {
	    				// TODO: handle exception
	    				e.printStackTrace();
	    			}
	                   volleyError = error;
	               }
	        	   
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	           }
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	return super.getParamsEncoding();
	            }
	        };		
	      AppController.getInstance().addToRequestQueue(req, "fetch");
	}
	
	
	private void postreq(String[] keys, String[] values, String url,int method,final int pos)
	{
		// Tag used to cancel the request
		String tag_json_obj = "json_obj_req";		
		 final int met = method;	
		 progressbar.show();
		JSONObject jsonBody = MyFunction.getJsonBody(keys, values);
               
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(met,
                url, jsonBody,
                new Response.Listener<JSONObject>() {
 
                    @Override
                    public void onResponse(JSONObject response) {
                    	
                    	
                    	if (met==1) {
                    		 ItemUpdate item = AppController.gson.fromJson(response.toString(),ItemUpdate.class);
                         	//updateadapter.addComment(item);
                         	listupdate.add(0,item);
                         	updateadapter.notifyDataSetChanged();
                         	txtupdate.setText("");
						}
                    	else
						{
							 ItemUpdate item = AppController.gson.fromJson(response.toString(),ItemUpdate.class);
							 listupdate.set(pos,item);
	                         updateadapter.notifyDataSetChanged();
	                         txtcomm.setText("");
	                         
						}
                    	
                     
                    	progressbar.dismiss();
                    }
                },err){
		           @Override
		        protected VolleyError parseNetworkError(
		        		VolleyError volleyError) {
		        	// TODO Auto-generated method stub
		        	   progressbar.dismiss();
		        	   
		        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
		        		   VolleyError error = null;
		                   try {
		                	   error= new VolleyError(new String(volleyError.networkResponse.data));
		                	  
		                	   JSONObject jsonObject = new JSONObject(error.getMessage());
		                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
		                   		{
		                   		AppController.permissions.setVIEWTASKS(false);
		                   		AppController.permissions.setPROXY(false);
		                   		AppController.permissions.setVIEWMEDICATIONS(false);
		                   		AppController.permissions.setVIEWUPDATES(false);
		                   		AppController.getInstance().cancelAllRequest();
		                   		
		                   	 Message message = mHandler.obtainMessage();
		                     message.sendToTarget();
		                   
		                   		}
		    			} catch (Exception e) {
		    				// TODO: handle exception
		    				e.printStackTrace();
		    			}
		                 
		               }
		        	   
		        	return MyFunction.parseNetworkErrorMy(volleyError);
		        }
		           @Override
		        	protected Response<JSONObject> parseNetworkResponse(
		        			NetworkResponse response) {
		        		// TODO Auto-generated method stub
		        		return super.parseNetworkResponse(response);
		        	}
		            /**
		             * Passing some request headers
		             * */
		           @Override
			        public Map<String, String> getHeaders() throws AuthFailureError {
			        	// TODO Auto-generated method stub
			        	return AppController.createBasicAuthHeader();
			        }
		        };
		 
		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
		
	}
	
	/**
	 * 
	 * @param keys param key
	 * @param values param value
	 * @param url url for post and edit comments
	 * @param method for GET or POST
	 * @param pos comment position
	 * @param post_pos Post position
	 */
	private void postComment(String[] keys, String[] values, String url,int method,final int pos,final int post_pos)
	{
		// Tag used to cancel the request
		String tag_json_obj = "json_obj_req";		
		 final int met = method;	
		 progressbar.show();  
		JSONObject jsonBody = MyFunction.getJsonBody(keys, values);
               
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(met,
                url, jsonBody,
                new Response.Listener<JSONObject>() {
 
                    @Override
                    public void onResponse(JSONObject response) {
                    	
                    
                    	if (met==1) {
                    		  Comments item = AppController.gson.fromJson(response.toString(),Comments.class);
                         	
                         	listcomments.add(0,item);
                         	
                         	listupdate.get(post_pos).getComments().add(item);
                         	updateadapter.notifyDataSetChanged();
                         	adaptercomments.notifyDataSetChanged();
                         	txtcomm.setText("");
						}
                    	else
						{
							 Comments item = AppController.gson.fromJson(response.toString(),Comments.class);
							 listcomments.set(pos,item);
							 listupdate.get(post_pos).getComments().set(pos,item);
							 updateadapter.notifyDataSetChanged();
							 adaptercomments.notifyDataSetChanged();	                         
						}
                     
                    	progressbar.dismiss();
                    }
                },err){
		           @Override
		        protected VolleyError parseNetworkError(
		        		VolleyError volleyError) {
		        	// TODO Auto-generated method stub
		        	   progressbar.dismiss();
		        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
		        		   VolleyError error = null;
		                   try {
		                	   error= new VolleyError(new String(volleyError.networkResponse.data));
		                	  
		                	   JSONObject jsonObject = new JSONObject(error.getMessage());
		                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
		                   		{
		                   		AppController.permissions.setVIEWTASKS(false);
		                   		AppController.permissions.setPROXY(false);
		                   		AppController.permissions.setVIEWMEDICATIONS(false);
		                   		AppController.permissions.setVIEWUPDATES(false);
		                   		AppController.getInstance().cancelAllRequest();
		                   		
		                   	 Message message = mHandler.obtainMessage();
		                     message.sendToTarget();
		                   		
		                   		
		                   		}
		    			} catch (Exception e) {
		    				// TODO: handle exception
		    				e.printStackTrace();
		    			}
		                   volleyError = error;
		               }
		        	return MyFunction.parseNetworkErrorMy(volleyError);
		        }
		           @Override
		        	protected Response<JSONObject> parseNetworkResponse(
		        			NetworkResponse response) {
		        		// TODO Auto-generated method stub
		        	 
		        		return super.parseNetworkResponse(response);
		        	}
		            /**
		             * Passing some request headers
		             * */
		           @Override
			        public Map<String, String> getHeaders() throws AuthFailureError {
			        	// TODO Auto-generated method stub
			        	return AppController.createBasicAuthHeader();
			        }		           
		        };
		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);		
	}
	
	/**
	 * 
	 * @param url delete url
	 * @param pos Post position
	 * @param flag for categarise Post array or comment array for delete
	 * @param comm_pos 0 for not comment and commposition if comment array
	 */
	private void deletereq(String url,final int pos,final String flag,final int comm_pos)
	{
		// Tag used to cancel the request
		String tag_json_obj = "json_obj_req";
		progressbar.show();  
		//JSONObject jsonBody = MyFunction.getJsonBody(keys, values);
		JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.DELETE,
                url,
                new Response.Listener<JSONObject>() {
 
                    @Override
                    public void onResponse(JSONObject response) {
                    	if (flag.equals("post")) {
                    		  ItemUpdate item = AppController.gson.fromJson(response.toString(),ItemUpdate.class);                    	
                            
                    		  listupdate.remove(pos);
                              updateadapter.notifyDataSetChanged();
						}
                    	else if (flag.equals("comment")) {
							Comments item = AppController.gson.fromJson(response.toString(),Comments.class);                    	
							//Update Main post list
							
								listcomments.remove(comm_pos);
								listupdate.get(pos).getComments().remove(comm_pos);
								
							//Update Comments list													
							adaptercomments.notifyDataSetChanged();							
							updateadapter.notifyDataSetChanged();
							setCommectItem(listupdate.get(pos));							
						}
                    	progressbar.dismiss();
                    }
                },err){
		           @Override
		        protected VolleyError parseNetworkError(
		        		VolleyError volleyError) {
		        	// TODO Auto-generated method stub
		        	   progressbar.dismiss();
		        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
		        		   VolleyError error = null;
		                   try {
		                	   error= new VolleyError(new String(volleyError.networkResponse.data));
		                	  
		                	   JSONObject jsonObject = new JSONObject(error.getMessage());
		                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
		                   		{
		                		   Log.e("onUPDATE PAGE",""+error.getMessage().toString());
		                   		AppController.permissions.setVIEWTASKS(false);
		                   		AppController.permissions.setPROXY(false);
		                   		AppController.permissions.setVIEWMEDICATIONS(false);
		                   		AppController.permissions.setVIEWUPDATES(false);
		                   		/*anu 20 aug*/
		                   		AppController.permissions.setWRITEUPDATES(false);
		                   		/*********end anu*/
		                   		AppController.getInstance().cancelAllRequest();
		                   		
		                   	 Message message = mHandler.obtainMessage();
		                     message.sendToTarget();
		                   		
		                   		}
		    			} catch (Exception e) {
		    				// TODO: handle exception
		    				e.printStackTrace();
		    			}
		                   volleyError = error;
		               }
		        	return MyFunction.parseNetworkErrorMy(volleyError);
		        	
		        }
		           @Override
		        	protected Response<JSONObject> parseNetworkResponse(
		        			NetworkResponse response) {
		        		// TODO Auto-generated method stub
		        	   try {
		        		   progressbar.dismiss();
					} catch (Exception e) {
						// TODO: handle exception
					}
		        	return super.parseNetworkResponse(response);
		        	}
		            /**
		             * Passing some request headers
		             * */
		           @Override
			        public Map<String, String> getHeaders() throws AuthFailureError {
			        	// TODO Auto-generated method stub
			        	return AppController.createBasicAuthHeader();
			        }
		           
		        };
		 
		// Adding request to request queue
		AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
	}
	
	
	 Response.ErrorListener err = new Response.ErrorListener() {

		@Override
		public void onErrorResponse(VolleyError volleyError) {
			// TODO Auto-generated method stub
			progressbar.dismiss();
			MyFunction.CheckError(getActivity(), volleyError);
			MyFunction.parseNetworkErrorMy(volleyError);
			checkIsViewPermission();
		}
	};
	
	void del_menu_create()
	{
		SwipeMenuCreator creator = new SwipeMenuCreator() {

		    @Override
		    public void create(SwipeMenu menu) {
		        // create "open" item
		    	if (menu.getViewType()==0) {
		    		 SwipeMenuItem deleteItem = new SwipeMenuItem(
				                getActivity());
				        // set item background
				        deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
				                0x3F, 0x25)));
				        // set item width
				        deleteItem.setWidth(dp2px(90));
				        // set a icon
				        deleteItem.setTitle("Delete");
				        deleteItem.setTitleColor(Color.WHITE);
				    //  deleteItem.setIcon(R.drawable.ic_delete);
				        deleteItem.setTitleSize(15);
				        // add to menu
				        menu.addMenuItem(deleteItem);
				}
		    	else
		    	{
		    		 SwipeMenuItem deleteItem = new SwipeMenuItem(
				                getActivity());
		    		 deleteItem.setBackground(new ColorDrawable(Color.WHITE));
				        // set item width
				        deleteItem.setWidth(dp2px(0));
				        // set a icon
				        deleteItem.setTitle("Back");
				        deleteItem.setTitleColor(Color.BLACK);
				    //  deleteItem.setIcon(R.drawable.ic_delete);
				        deleteItem.setTitleSize(15);				       
				        menu.addMenuItem(deleteItem);
		    	}
		        // create "delete" item
		    } 
		};

		// set creator
		listcomm.setMenuCreator(creator);
		listview.setMenuCreator(creator);
	}
	
	/**
	 *  Dp to Pixel conversion
	 * @param dp
	 * @return
	 */
	 private int dp2px(int dp) {
	        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
	                getResources().getDisplayMetrics());
	}
	 
	 public void MoreSetup()
		{
		 HideKey.hideSoftKeyboard(getActivity());
			if (relcomm.getVisibility()==View.VISIBLE) {
				
				Animation out = AnimationUtils.loadAnimation(getActivity(),R.anim.slide_out_right);
				relcomm.startAnimation(out);
				relcomm.setVisibility(View.GONE);
				btnsend.setVisibility(View.VISIBLE);				
			}
			else
			{				
				Animation in = AnimationUtils.loadAnimation(getActivity(),R.anim.slide_in_right);
				relcomm.startAnimation(in);
				btnsend.setVisibility(View.INVISIBLE);
				relcomm.setVisibility(View.VISIBLE);
				POSITION_MORE=-1;
			
			}
		}
	 
	 public void setCommectItem(ItemUpdate tempitem)
	 {
		 if (tempitem!=null) {
			 
		 ImageView img=(ImageView)relcomm.findViewById(R.id.profimg);
		 TextView txtcomm=(TextView)relcomm.findViewById(R.id.txtname);
		 txtcomm.setVisibility(View.VISIBLE);
		 TextView txtdesc=(TextView)relcomm.findViewById(R.id.txtdesc);
		 txtdesc.setVisibility(View.VISIBLE);
		 TextView txtdate=(TextView)relcomm.findViewById(R.id.txtdate);
		 txtdate.setVisibility(View.VISIBLE);
		 TextView txtcount=(TextView)relcomm.findViewById(R.id.txt_item_comm);
		 
		 try {
				if (tempitem.getUser().getProfileImage()!=null) {
					Picasso.with(getActivity())
					.load(Constants.URL_img+"/cache/w100xh100-2/images/"+tempitem.getUser().getProfileImage().getImage().getPath())
					.placeholder(R.drawable.default_profile)
					.into(img);
				}
				else
				{
					img.setImageResource(R.drawable.default_profile);
				}
			} 
		 catch (Exception e) {
				// TODO: handle exception
			}	
		
			txtdesc.setText(tempitem.getPostText());
			txtcomm.setText(tempitem.getUser().getFirstName()+" "+tempitem.getUser().getLastName());
			txtdate.setText(Constants.getDate(tempitem.getPostTimeEdited()));
			if(tempitem.getCommentsSize()==1){
				txtcount.setText(tempitem.getCommentsSize()+" comment");
			}else{
				txtcount.setText(tempitem.getCommentsSize()+" comments");
			}
			
		}
	}
	 
	 /**
	  * This is where you do your work in the UI thread. Your worker tells you in the message what to do.
	  */
	 public Handler mHandler = new Handler(Looper.getMainLooper()) {
		    @Override
		    public void handleMessage(Message message) {
		        // This is where you do your work in the UI thread.
		        // Your worker tells you in the message what to do.
		    	checkIsViewPermission();
		    }
		};
		
		private void logMobileView(){
			
			logMobUserId = AppController.permissions.loguserid;
			proxiedUser = String.valueOf(AppController.getUser().getId());
			operatingSystem = "Android";
			deviceType = MyFunction.getDeviceName();
			screenSize = MyFunction.getScreenSize(getActivity());
			
			String url = Constants.HOST_URL+"/users/"+logMobUserId+"/elasticLog/pageview/proxiedUser/"+proxiedUser+"/mobile?operatingSystem="+operatingSystem+"&deviceType="+deviceType+"&screenSize="+screenSize;
			Log.e("logMobileView", "url: "+url);
			//url = "http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com/api/users/404/elasticLog/pageview/proxiedUser/404/mobile?operatingSystem=ANDROID&deviceType=MicromaxQ395&screenSize=720x1184";
			JSONObject jsonBody=new JSONObject();
			try {
				jsonBody.put("string", "updates");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
			String urlEncoded = Uri.encode(url, ALLOWED_URI_CHARS);
			
			JsonObjectRequest user_obj=new JsonObjectRequest(Method.POST, urlEncoded, jsonBody, new Listener<JSONObject>() {

				@Override
				public void onResponse(JSONObject arg0) {
					
					Log.e("logMobileView", "onResponse: "+arg0);
				}
			}, new ErrorListener() {

				@Override
				public void onErrorResponse(VolleyError arg0) {
					// TODO Auto-generated method stub
					Log.e("logMobileView", "onErrorResponse: "+arg0);
				}
			}){
				
		           @Override
		        protected VolleyError parseNetworkError(
		        		VolleyError volleyError) 
		           {
		        	// TODO Auto-generated method stub
		        	   //Log.e("Network"," "+ volleyError.toString());	            	
		        	return MyFunction.parseNetworkErrorMy(volleyError);
		        }
		           /* (non-Javadoc)
		         * @see com.android.volley.toolbox.JsonArrayRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
		         */
		        @Override
		        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
		        	// TODO Auto-generated method stub
		        	  Map<String, String> headers = response.headers;
		        	   String cookie = headers.get("Set-Cookie");
		        	   Log.e("Set-Cookie: "," "+ cookie);
		        	   AppController.getInstance().saveCookie(cookie);
		        	   
		        	   try {
		                   String jsonString = new String(response.data,
		                           HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

		                   JSONObject result = null;

		                   if (jsonString != null && jsonString.length() > 0)
		                        result = new JSONObject(jsonString);

		                   return Response.success(result,
		                           HttpHeaderParser.parseCacheHeaders(response));
		               } catch (UnsupportedEncodingException e) {
		                   return Response.error(new ParseError(e));
		               } catch (JSONException je) {
		                   return Response.error(new ParseError(je));
		               }
		        	
		        	//return super.parseNetworkResponse(response);
		        }
		           @Override
		        public Map<String, String> getHeaders() throws AuthFailureError {
		        	// TODO Auto-generated method stub
		        	return AppController.createBasicAuthHeader();
		        }
		         
		            @Override
		            protected String getParamsEncoding() {
		            	// TODO Auto-generated method stub
		            	//Log.e("Encoding"," "+getParamsEncoding());
		            	return super.getParamsEncoding();
		            }
		        };
		        AppController.getInstance().addToRequestQueue(user_obj, "user");
		}
}
