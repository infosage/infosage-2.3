package org.infosagehealth.app;

import java.util.ArrayList;
import java.util.Map;
import org.infosagehealth.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.android.volley.AuthFailureError;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

public class Test extends Activity{

	ListView listview;
	AssignUserAdapter useradapter;
	Button btnunassign,btncancel;
	int selpos=-1;
	ArrayList<UserObject> usrobjArr = new ArrayList<UserObject>();
	String userid;
	TaskObject taskobj;
	LinearLayout lay1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test);
		lay1=(LinearLayout)findViewById(R.id.lay1);
		listview = (ListView)findViewById(R.id.listView1);
		btnunassign = (Button)findViewById(R.id.btnunassign);
		btncancel = (Button)findViewById(R.id.btncancel);
		
		if(getIntent().getExtras() != null)
		{
			if (getIntent().getExtras().getBoolean("isME")) 
			{	
				
				lay1.setVisibility(View.GONE);
				findViewById(R.id.lay_progress).setVisibility(View.VISIBLE);
				Bundle b = getIntent().getExtras();
				userid = b.getString("uid");
				ArrayList<TaskObject> arrlist = getIntent().getParcelableArrayListExtra("taskobj");
				taskobj = arrlist.get(0);
				UserObject uobj = new UserObject("", "", "");
				uobj.setUserobj(null);
				updateTask(uobj);
				
			}else{
				lay1.setVisibility(View.VISIBLE);
				findViewById(R.id.lay_progress).setVisibility(View.GONE);
			Bundle b = getIntent().getExtras();
			userid = b.getString("uid");
			ArrayList<TaskObject> arrlist = getIntent().getParcelableArrayListExtra("taskobj");
			taskobj = arrlist.get(0);
			String url = MainActivity.webserviceurl+"/users/"+userid+"/connections/";
			getResponse(url,userid);
			
			}
		}
		
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if(AppController.session.getIsProxy()){
						if(selpos >= 0){
							View selV = (View)listview.getChildAt(selpos);
							selV.setBackgroundColor(Color.WHITE);
						}
						
						selpos = arg2;
						arg1.setBackgroundColor(getResources().getColor(R.color.grayclr));
						
						UserObject uobj = usrobjArr.get(selpos);
						updateTask(uobj);
						
						
				}
				else if(AppController.getUser().getIsElder())
				{
					if(selpos >= 0){
						View selV = (View)listview.getChildAt(selpos);
						selV.setBackgroundColor(Color.WHITE);
					}
					
					selpos = arg2;
					arg1.setBackgroundColor(getResources().getColor(R.color.grayclr));
					
					UserObject uobj = usrobjArr.get(selpos);
					updateTask(uobj);
					
				}
				else
				{
					// No
				}
				
				
			}
			
		});
		
		btnunassign.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//int arg2 = listview.getSelectedItemPosition();
				/*
				Intent intent = new Intent();
				Bundle b = new Bundle();
				b.putInt("key", selpos);
				intent.putExtras(b);
				
				setResult(RESULT_OK, intent);				
				finish();
				*/
				
				UserObject uobj = new UserObject("", "", "");
				uobj.setUserobj(null);
				updateTask(uobj);
			}
		});
		btncancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				setResult(RESULT_OK, new Intent());
				finish();
			}
		});
	}

	public void getResponse(String url,String userid)
	{
		final String uid = userid;
	
		JsonArrayRequest req = new JsonArrayRequest(url, new Listener<JSONArray>() {

			@Override
			public void onResponse(JSONArray arg0) {
				// TODO Auto-generated method stub
			
				int arrcnt = arg0.length();
				
				// parse user list
				for(int i=0; i < arrcnt; i++)
				{
					try {
						JSONObject obj = arg0.getJSONObject(i);
						JSONObject targetobj = obj.getJSONObject("target");
						String imgpath=null;
						
						//add users that are not keystones of this user
						if(obj.getString("subscribed").compareTo("false")==0)
						{
						if(!targetobj.isNull("profileImage"))
						{
							 imgpath = targetobj.getJSONObject("profileImage").getJSONObject("image").getString("path");
						}else
						{
							imgpath="";
						}
						UserObject uobj = new UserObject(targetobj.getString("id"), targetobj.getString("firstName"), targetobj.getString("lastName"), imgpath, obj);
						usrobjArr.add(uobj);
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(AppController.session.getIsProxy() || AppController.getUser().getIsElder()){
				useradapter = new AssignUserAdapter(getApplicationContext(),Test.this,usrobjArr,uid,taskobj,true);
				}else
				{
					useradapter = new AssignUserAdapter(getApplicationContext(),Test.this,usrobjArr,uid,taskobj,false);
				}
				listview.setAdapter(useradapter);
				
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	            	
	        	return super.parseNetworkError(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	
	            	return super.getParamsEncoding();
	            }
	            
	        };
		
	      AppController.getInstance().addToRequestQueue(req, "fetch");

	}
	
	public void updateTask(UserObject usrobj)
	{
		try {
		UserObject obj = usrobj;
		String url;
		
		
		url = MainActivity.webserviceurl+"/users/"+userid+"/tasks/"+taskobj.getTaskId();
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("name", taskobj.getTaskName());
			jobj.put("startDate", taskobj.getTaskStartdt());
			jobj.put("endDate", taskobj.getTaskEnddt());
			jobj.put("complete", 0);
			jobj.put("assignedUser", obj.getuserobj());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JsonObjectRequest req = new JsonObjectRequest(Method.PUT, url, jobj, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				Bundle b = new Bundle();
				b.putInt("key", selpos);
				intent.putExtras(b);
				
				setResult(RESULT_OK, intent);
				finish();
			}
			
		},  new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				Bundle b = new Bundle();
				b.putInt("key", selpos);
				intent.putExtras(b);
				
				setResult(RESULT_CANCELED, intent);
				finish();
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	               	
	        	return super.parseNetworkError(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	
	            	return super.getParamsEncoding();
	            }
	            
	        };
		
	      AppController.getInstance().addToRequestQueue(req, "fetch");

			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
