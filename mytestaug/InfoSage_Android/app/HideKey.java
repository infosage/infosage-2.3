package org.infosagehealth.app;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;

/**
 * 
 * @author Govinda P.
 * @use to hide keypad on touch outside
 */
public class HideKey extends AppCompatActivity{

	/**
	 * This method used to hide keypad on view touch.
	 * @param activity current running activity
	 */
	public static void hideSoftKeyboard(Activity activity) {
		try {
			if (activity!=null) {
				 InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
				    inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
			}
			 
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	  
	}
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		hideSoftKeyboard(this);
		return super.onTouchEvent(event);
	}
	
}
