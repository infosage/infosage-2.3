package org.infosagehealth.app;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.infosagehealth.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.infosage.adapter.AdapterMeditation;
import com.infosage.adapter.OnListItemClickListner;
import com.infosage.item.ItemMeditation;
import com.infosage.util.Constants;
import com.infosage.util.MyFunction;
import com.infosage.view.CustomAnimatedProgress;
import com.infosage.view.SegmentedGroup;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 
 * @author Govinda P.
 *
 */
public class MedicationsFragment extends Fragment{

	ListView medlist;
	AdapterMeditation medadapter;
	RelativeLayout relcomm;
	LinearLayout lay2;
	Button btnedit;// btnact, btninact;
	
	float dist;
	EditMedicationCallback callback = null;
	TextView txt_medi_no;
	public final ArrayList<ItemMeditation> listMedi=new ArrayList<ItemMeditation>();
	public final ArrayList<ItemMeditation> listMedi_inactive=new ArrayList<ItemMeditation>();
	CustomAnimatedProgress progress;
	int count_active=0;
	int count_inactive=0;
	LinearLayout lay_medi_btn;
	SegmentedGroup segmentgroup;
	AlertDialog.Builder warrningBox=null;
	
	String logMobUserId,proxiedUser,operatingSystem,deviceType,screenSize; 
	
	public MedicationsFragment() {
		// TODO Auto-generated constructor stub	
		
	}
	
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onStop()
	 */
	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(progress!=null){
			progress.dismiss();
		}
		AppController.getInstance().cancelPendingRequests("medication");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		//Profile Header Visible
		
		View view = inflater.inflate(R.layout.medications, container, false);
		
		segmentgroup=(SegmentedGroup)view.findViewById(R.id.segmented);
		
		segmentgroup.check(R.id.btn_medi_1);
		try {
			lay_medi_btn=(LinearLayout)view.findViewById(R.id.linearLayout1);
			lay_medi_btn.setVisibility(View.GONE);
		} catch (Exception e) {
			// TODO: handle exception
		}
		progress=new CustomAnimatedProgress(getActivity());
		medlist = (ListView)view.findViewById(R.id.medlist);
		btnedit = (Button)view.findViewById(R.id.btnedit);
		
		txt_medi_no=(TextView)view.findViewById(R.id.txt_medi_msg);
		txt_medi_no.setTextColor(Color.BLACK);
		txt_medi_no.setText("");
		txt_medi_no.setVisibility(View.GONE);
		
		if (AppController.permissions.isVIEWMEDICATIONS()) 
		{
			btnedit.setVisibility(View.VISIBLE);
		}
		else
		{
			btnedit.setVisibility(View.GONE);
		}
		
		if (AppController.permissions.isPROXY()) 
		{
			btnedit.setVisibility(View.VISIBLE);
		}else
		{
			btnedit.setVisibility(View.GONE);
		}			
		
		// call Log Mobile Page View API
		logMobileView();
				
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.Fragment#onResume()
	 */
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			
			if (UserTasks.userTarget==null) {
				getActivity().finish();
			}
			
			if (!AppController.permissions.isVIEWMEDICATIONS()) {
				warrningBox();
				return;
			}
			
			getResponse(Constants.getURL_MedicationList(""+UserTasks.userTarget.getId()),
					AppController.session.getLogUserId());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			getResponse(Constants.getURL_MedicationList(""+AppController.getViewUser().getId()),
					AppController.session.getLogUserId());
		}
//getResponse(Constants.getURL_MedicationList(""+UserTasks.userTarget.getId()),
//AppController.session.getLogUserId());
		
		
 segmentgroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
	
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		if (!AppController.permissions.isVIEWMEDICATIONS()) {
			warrningBox();
			return;
		}
		 switch (checkedId) {
            case R.id.btn_medi_1:
            	if (listMedi.size()>0) {
            		txt_medi_no.setVisibility(View.GONE);}
            	else
            	{
            		txt_medi_no.setVisibility(View.VISIBLE);
            	}
            	medadapter = new AdapterMeditation(getActivity().getApplicationContext(), getActivity(),null,listMedi);
				medlist.setAdapter(medadapter);
				medadapter.notifyDataSetChanged();
            	
				
                return;
            case R.id.btn_medi_2:
            	if (listMedi_inactive.size()>0) {
            		txt_medi_no.setVisibility(View.GONE);		
	 }else
			{
				txt_medi_no.setVisibility(View.VISIBLE);
			}
            		medadapter = new AdapterMeditation(getActivity().getApplicationContext(), getActivity(),null,listMedi_inactive);
        			medlist.setAdapter(medadapter);
        			medadapter.notifyDataSetChanged();
                return;
           
            default:
                // Nothing to do
        }
	}
});
btnedit.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (!AppController.permissions.isVIEWMEDICATIONS() || !AppController.permissions.isPROXY()) {
			warrningBox();
			return;
		}else{
		callback.showEditFragment();
		}
	}
});
	}
	
	
	interface EditMedicationCallback {

		public void showEditFragment();
	}

	
	@Override
	public void onAttach(Activity activity){
		
		super.onAttach(activity);
		
		if(activity instanceof EditMedicationCallback){
			callback = (EditMedicationCallback)activity;
		}else{ 
			throw new ClassCastException();
		}
	}
	
	@Override
	public void onDetach(){
		
		super.onDetach();
	}
	
	/**
	 * 
	 */
	private void warrningBox() {
		// TODO Auto-generated method stub
		
			warrningBox=new AlertDialog.Builder(getActivity());
			warrningBox.setMessage(R.string.msg_pri_change);
			warrningBox.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			try {
				dialog.dismiss();
				Intent relogin=new Intent(getActivity(), MainActivity.class);
				relogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(relogin);
				getActivity().finish();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	});
			AlertDialog dialog1 = warrningBox.create();
			dialog1.setCancelable(false);
			dialog1.show();
	}
	
	/**
	 * Web services call
	 */
	public void getResponse(String url,String userid)
	{
		if(getActivity()==null){
			return;
		}
	
		progress.show();
	
		JsonArrayRequest req = new JsonArrayRequest(url, new Listener<JSONArray>() {

			@Override
			public void onResponse(JSONArray arg0) {
			
			try {
				listMedi.clear();
				listMedi_inactive.clear();
				List<ItemMeditation> templistmedi = Arrays.asList(AppController.gson.fromJson(arg0.toString(),ItemMeditation[].class));
				/**
				 * Deleted post and comments setup
				 */
				for (ItemMeditation itemmedi : templistmedi) {
					if (itemmedi.isIsActive() && itemmedi.isIsPublic()) 
					{						
						listMedi.add(itemmedi);
						count_active=count_active+1;
					}else
					{
						if (!itemmedi.isIsActive() && itemmedi.isIsPublic()) {
							listMedi_inactive.add(itemmedi);
							count_inactive=count_inactive+1;
						}
						if (itemmedi.isIsPublic()) {
							count_inactive=count_inactive+1;
						}
						
					}
				}
				
				adapterInit();
				progress.dismiss();
		//	updateadapter.notifyDataSetChanged();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				progress.dismiss();
				MyFunction.CheckError(getActivity(), arg0);
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub	        	
	        	   if (progress!=null && progress.isShowing()) {
					progress.dismiss();
	        	   }
	        	   if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
	        		   VolleyError error = null;
	                   try {
	                	   error= new VolleyError(new String(volleyError.networkResponse.data));
	                	  
	                	   JSONObject jsonObject = new JSONObject(error.getMessage());
	                	   if(jsonObject.getString("userMessage").toString().contains("You don't have permission to do that"))
	                   		{
	                		   Message message = mHandler_warnning.obtainMessage();
	                           message.sendToTarget();
	                   		}
	                   }catch(Exception e){
	                	   e.printStackTrace();
	                   }
	        	   }
	        	   
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            //	Log.e("Encoding"," "+getParamsEncoding());
	            	return super.getParamsEncoding();
	            }	            
	        };		
	      AppController.getInstance().addToRequestQueue(req, "medication");
	}
	
	public void adapterInit()
	{
	medadapter = new AdapterMeditation(getActivity().getApplicationContext(), getActivity(), new OnListItemClickListner() {
			
			@Override
			public void showlist(View v, int pos) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void showdate(View v, int pos) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void showclearbtn(View v, int flag) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void showdialog(View v, int pos) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void update(TaskObject taskobj, int flag, String taskid) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void showlist(View v, int pos, boolean isMe) {
				// TODO Auto-generated method stub
				
			}
		},listMedi);
	medlist.setAdapter(medadapter);
	
	/**
	 * For Active Enable desible
	 */
	if (count_active<1) {
		txt_medi_no.setVisibility(View.VISIBLE);
if (AppController.session.getLogUserId().equals(""+UserTasks.userTarget.getId())) {
	txt_medi_no.setText("You have no active medications listed.");
}else
{
	txt_medi_no.setText(UserTasks.userTarget.getFirstName()+" "+UserTasks.userTarget.getLastName()+" has no active medications listed.");
}
//btninact.setEnabled(false);
	}else
	{
		txt_medi_no.setVisibility(View.GONE);
		///btnact.setEnabled(true);
	}
	
	if (count_inactive<1) {
//btninact.setEnabled(false);
//txt_medi_no.setVisibility(View.VISIBLE);
		segmentgroup.setEnabled(false);
		segmentgroup.setInactiveDesaible(false);
		
	}
	else
	{
		segmentgroup.setEnabled(true);
		//btninact.setEnabled(true);
		//txt_medi_no.setVisibility(View.GONE);
	}
	
	}
	
	public Handler mHandler_warnning = new Handler(Looper.getMainLooper()) {
	    @Override
	    public void handleMessage(Message message) {
	        // This is where you do your work in the UI thread.
	        // Your worker tells you in the message what to do.
	    warrningBox();
	    
	    }
	};
	
private void logMobileView(){
		
		logMobUserId = AppController.permissions.loguserid;
		proxiedUser = String.valueOf(AppController.getUser().getId());
		operatingSystem = "Android";
		deviceType = MyFunction.getDeviceName();
		screenSize = MyFunction.getScreenSize(getActivity());
		
		String url = Constants.HOST_URL+"/users/"+logMobUserId+"/elasticLog/pageview/proxiedUser/"+proxiedUser+"/mobile?operatingSystem="+operatingSystem+"&deviceType="+deviceType+"&screenSize="+screenSize;
		Log.e("logMobileView", "url: "+url);
		//url = "http://ec2-52-36-249-33.us-west-2.compute.amazonaws.com/api/users/404/elasticLog/pageview/proxiedUser/404/mobile?operatingSystem=ANDROID&deviceType=MicromaxQ395&screenSize=720x1184";
		JSONObject jsonBody=new JSONObject();
		try {
			jsonBody.put("string", "medications");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";
		String urlEncoded = Uri.encode(url, ALLOWED_URI_CHARS);
		
		JsonObjectRequest user_obj=new JsonObjectRequest(Method.POST, urlEncoded, jsonBody, new Listener<JSONObject>() {

			@Override
			public void onResponse(JSONObject arg0) {
				
				Log.e("logMobileView", "onResponse: "+arg0);
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				Log.e("logMobileView", "onErrorResponse: "+arg0);
			}
		}){
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	   //Log.e("Network"," "+ volleyError.toString());	            	
	        	return MyFunction.parseNetworkErrorMy(volleyError);
	        }
	           /* (non-Javadoc)
	         * @see com.android.volley.toolbox.JsonArrayRequest#parseNetworkResponse(com.android.volley.NetworkResponse)
	         */
	        @Override
	        protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
	        	// TODO Auto-generated method stub
	        	  Map<String, String> headers = response.headers;
	        	   String cookie = headers.get("Set-Cookie");
	        	   Log.e("Set-Cookie: "," "+ cookie);
	        	   AppController.getInstance().saveCookie(cookie);
	        	   
	        	   try {
	                   String jsonString = new String(response.data,
	                           HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));

	                   JSONObject result = null;

	                   if (jsonString != null && jsonString.length() > 0)
	                        result = new JSONObject(jsonString);

	                   return Response.success(result,
	                           HttpHeaderParser.parseCacheHeaders(response));
	               } catch (UnsupportedEncodingException e) {
	                   return Response.error(new ParseError(e));
	               } catch (JSONException je) {
	                   return Response.error(new ParseError(je));
	               }
	        	
	        	//return super.parseNetworkResponse(response);
	        }
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	//Log.e("Encoding"," "+getParamsEncoding());
	            	return super.getParamsEncoding();
	            }
	        };
	        AppController.getInstance().addToRequestQueue(user_obj, "user");
	}
}
