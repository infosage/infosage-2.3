package org.infosagehealth.app;

import java.util.Map;

import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.StringRequest;
import com.infosage.util.Constants;
import com.infosage.util.MyFunction;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * 
 * @author Govinda P.
 *
 */
public class Logout extends Activity{

	 static JSONObject jsonBody=new JSONObject();
		
		
	
	 /**
	  * @use : currently not in used
	  */
	public void logoutAPI()
	{
		StringRequest req = new StringRequest(Method.POST,Constants.getURL_logout(String.valueOf(AppController.getMainUser().getId())), new Listener<String>() 
		{
			@Override
			public void onResponse(String arg0) {
				
				System.out.println("onResponse.........Logout Complete");
				 SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
				 SharedPreferences pref1 = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
				 
				 String helpstatus = pref1.getString("helpstatus", "");
				 Log.d("helpstatus", ""+helpstatus);
				 
				 SharedPreferences.Editor editor = pref.edit();
				 editor.clear();
				 editor.apply();
				 
				 editor = pref1.edit();
				 editor.putString("helpstatus", helpstatus);
				 Log.d("helpstatus", ""+helpstatus);
				 editor.commit();
					
				Intent intent=new Intent(AppController.getInstance(),MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
		}, new ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError arg0) {
				// TODO Auto-generated method stub
				System.out.println("onErrorResponse.........Logout Faild");
				SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
				SharedPreferences pref1 = PreferenceManager.getDefaultSharedPreferences(AppController.getInstance());
				SharedPreferences.Editor editor = pref.edit();
				editor.clear();
				editor.apply();
				
				String helpstatus = pref1.getString("helpstatus", "");
				 Log.d("helpstatus", ""+helpstatus);
				 editor = pref1.edit();
				 editor.putString("helpstatus", helpstatus);
				 Log.d("helpstatus", ""+helpstatus);
				 editor.commit();
				 
				Intent intent=new Intent(AppController.getInstance(),MainActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				finish();
			}
		}) {
			
	           @Override
	        protected VolleyError parseNetworkError(
	        		VolleyError volleyError) 
	           {
	        	// TODO Auto-generated method stub
	        	 //  Log.e("Network"," "+ volleyError.toString());	            	
	        	return super.parseNetworkError(volleyError);
	        }
	           
	           @Override
	        public Map<String, String> getHeaders() throws AuthFailureError {
	        	// TODO Auto-generated method stub
	        	return AppController.createBasicAuthHeader();
	        }
	         
	            @Override
	            protected String getParamsEncoding() {
	            	// TODO Auto-generated method stub
	            	
	            	return super.getParamsEncoding();
	            }	            
	        };		
	      AppController.getInstance().addToRequestQueue(req, "logout");
	}
	
	/**
	 * @use first unregister token then logout. and if token not-register then direct logout.
	 * @param response
	 * @param err
	 */
	public static void logoutAPI(Listener<String> response,ErrorListener err)
	{
		String userid="";
		
		jsonBody=new JSONObject();
	    jsonBody=MyFunction.getJsonBody(new String[]{"string"},new String[]{AppController.session.getPushToken()});
			// Add custom implementation, as needed.
	        
	       if (AppController.session.getPushToken().equals("")) {
	    	   
	    	   String uid=String.valueOf(AppController.getMainUser().getId());
	    	   if (uid.equals("")) {
				uid="-1";
	    	   }
	    		StringRequest req = new StringRequest(Method.POST,Constants.getURL_logout(uid),response,err) {
	    			
	    	           @Override
	    	        protected VolleyError parseNetworkError(
	    	        		VolleyError volleyError) 
	    	           {
	    	        	// TODO Auto-generated method stub
	    	        	 //  Log.e("Network"," "+ volleyError.toString());	            	
	    	        	return super.parseNetworkError(volleyError);
	    	        }
	    	           
	    	           @Override
	    	        public Map<String, String> getHeaders() throws AuthFailureError {
	    	        	// TODO Auto-generated method stub
	    	        	return AppController.createBasicAuthHeader();
	    	        }
	    	         
	    	            @Override
	    	            protected String getParamsEncoding() {
	    	            	// TODO Auto-generated method stub	            	
	    	            	return super.getParamsEncoding();
	    	            }	            
	    	        };		
	    	     AppController.getInstance().addToRequestQueue(req, "logout");
	    	      
		} else{
	    	StringRequest req_unregister = new StringRequest(Method.POST,
	    			Constants.getURL_PushUnRegistration(""+AppController.getMainUser().getId()),
	                response,err){
			           @Override
			        protected VolleyError parseNetworkError(
			        		VolleyError volleyError) {
			        	// TODO Auto-generated method stub
			        	  	
			        	
			        	   return super.parseNetworkError(volleyError);
			        	       }
			           @Override
			        	protected Response<String> parseNetworkResponse(
			        			NetworkResponse response) {
			        		// TODO Auto-generated method stub
			        	   //Log.e("headers: "," "+ response.headers.toString());
			        	 /*  Map<String, String> headers = response.headers;
			        	   String cookie = headers.get("Set-Cookie");
			        	   Log.e("Set-Cookie: "," "+ cookie);
			        	   AppController.getInstance().saveCookie(cookie);*/
			        	  // AppController.session.SetPushToken("");
							//AppController.session.SetPushRegister(false);
							//AppController.session.clearSession();
			        		return super.parseNetworkResponse(response);
			        	}
			            /**
			             * Passing some request headers
			             * */
			           @Override
				        public Map<String, String> getHeaders() throws AuthFailureError {
				        	// TODO Auto-generated method stub
			        	   
				        	return AppController.createBasicAuthHeader();
				        }	
			           
			           @Override
			           public byte[] getBody() throws AuthFailureError {
			               return jsonBody.toString().getBytes();
			           }

			           @Override
			           public String getBodyContentType() {
			               return "application/json";
			           }
			        };
		
	    	// Create retry policy for unregister 
			req_unregister.setRetryPolicy(new DefaultRetryPolicy(10000,0,1));
			int socketTimeout = 90000;//30 seconds - change to what you want
			RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
			req_unregister.setRetryPolicy(policy);
			
			AppController.getInstance().addToRequestQueue(req_unregister);
			
		if (AppController.getMainUser()==null) 
		{
			userid=AppController.session.getMyUserid();
		}else
		{
			userid=String.valueOf(AppController.getMainUser().getId());			
		}
		try {
			if (userid=="" || userid==null) {
				return;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		}
	     
	}
}
