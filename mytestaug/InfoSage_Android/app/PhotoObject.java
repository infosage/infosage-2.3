/**
*@authour Sarika
*12-May-2016
* @update date 12-May-2016
 */
package org.infosagehealth.app;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author Sarika
 *
 */
public class PhotoObject implements Parcelable{

	String id, imageid, imagepath,uploaddate;
	
	public PhotoObject(String id1, String imageid1, String imagepath1,String uploaddate1)
	{
		this.id = id1;
		this.imageid = imageid1;
		this.imagepath = imagepath1;
		this.uploaddate = uploaddate1;
	}
	
	public PhotoObject(Parcel in)
	{
		
	     this.id = in.readString();
	     this.imageid = in.readString();
	     this.imagepath = in.readString();
	     this.uploaddate = in.readString();
	     
	}
	
	@Override
	public String toString()
	{
		return "PhotoObject[id="+id+",imageid="+imageid+",imagepath="+imagepath+",uploaddate="+uploaddate+"]";
	}
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		//dest.writeStringArray(new String[] { this.taskid, this.taskname, this.taskstartdt, this.taskenddt, this.taskdur, this.taskcomplete });
		dest.writeString(this.id);
		dest.writeString(this.imageid);
		dest.writeString(this.imagepath);
		dest.writeString(this.uploaddate);
	}
	
	 public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
	        public PhotoObject createFromParcel(Parcel in) {
	            return new PhotoObject(in);
	        }

	        public PhotoObject[] newArray(int size) {
	        	
	        	
	            return new PhotoObject[size];
	        }
	    };
	    
	public String getId()
	{
		 return id;
	}
	public String getImageId()
	{
		 return imageid;
	}
	public String getImagePath()
	{
		 return imagepath;
	}
	public String getUploadDate()
	{
		 return uploaddate;
	}
}
